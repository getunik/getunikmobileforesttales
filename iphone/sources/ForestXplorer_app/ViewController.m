//
//  ViewController.m
//  ForestXplorer_app
//
//  Created by Adrian Hoitan on 10/16/13.
//  Copyright (c) 2013 Adrian Hoitan. All rights reserved.
//

#import "ViewController.h"
#import "WUINavigationController.h"
#import "WUITableView.h"
#import "WUITableViewCell.h"
#import "ULanguageHelper.h"
#import "CHTTPClient.h"
#import "AppDelegate.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize m_bStopUpdating;

/**
 * Initialize and load the displayed XML file
 *
 * @return none
 */
-(void)initializeDisplayXML
{
    // Override point for customization after application launch.
    m_guideInterfaceLoader = [[Guide_interface_loader alloc] init];
    [m_guideInterfaceLoader loadContent];
    
    m_uivAddedView = (WUITabController*)[m_guideInterfaceLoader getView];
    
    //< Load the view
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        CGRect rect = [m_uivAddedView getView].frame;
        rect.origin.y += 20;
        [[m_uivAddedView getView] setFrame:rect];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    appDelegate.m_uivViewController = self;
    
	//< Initialize MACC Display XML
    [self initializeDisplayXML];
    //< Add our display view
    [self.view addSubview:[m_uivAddedView getView]];
}

/**
 * Checking if the application data is up to date in a thread
 *
 * @return none
 */
-(void)threadIsApplcationDataUpdated
{
}

/**
 * Check if the application data is up to date and if not display a message box
 *
 * @return none
 */
-(bool)isApplicationDataUpdated
{
    bool           bIsUpToDate      = false;
    
    //< Write the time and date for the current check
    NSUserDefaults *nsudPreferred   = [NSUserDefaults standardUserDefaults];
    [nsudPreferred setObject:[NSDate date] forKey:@"CWWFFCWLastUpdateCheck"];
    [nsudPreferred synchronize];
    
    //< The date of the last update will be stored in our user defaults
    NSString       *nsstrLastUpdate = [nsudPreferred objectForKey:@"CWWFFCWLastUpdate"];
    
    //< Check if the date from our backend is updated
    CHTTPClient *httpClient = [[CHTTPClient alloc] init];
    if (true == [httpClient sendRequest:PUSH_SERVER_USER Password:PUSH_SERVER_PASS TimeOut:100 URLAddress:LAST_UPDATE_HTTP])
    {
        m_nsstrServerUpdateDate = [httpClient getReceivedStrData];
    }
    
    if ([nsstrLastUpdate length] > 0)
    {
        if (nil != m_nsstrServerUpdateDate)
        {
            bIsUpToDate = [m_nsstrServerUpdateDate isEqual:nsstrLastUpdate];
        }
        else
        {
            bIsUpToDate = true;
        }
        
        //< Inform the user that there are updates availabile if any
        if (false == bIsUpToDate)
        {
            [self displayUpdatesAvailable];
        }
        else
        {
            [self updateNoDownloadsAvailabile];
        }
    }
    else
    {
        NSUserDefaults *nsudPreferred = [NSUserDefaults standardUserDefaults];
        [nsudPreferred setObject:m_nsstrServerUpdateDate forKey:@"CWWFFCWLastUpdate"];
        [nsudPreferred synchronize];
        
        bIsUpToDate = true;
    }
    
    m_bCheckingForUpdates = false;
    
    return bIsUpToDate;
}

/**
 * Display the updates availabile screen
 *
 * @return none
 */
-(void)displayUpdatesAvailable
{
    //< The displaying screen can be already loaded
    if (nil == m_uivUADisableGlass)
    {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        
        //< Create a disabled glass view that will cover the whole application
        m_uivUADisableGlass = [[UIView alloc] initWithFrame:screenRect];
        [m_uivUADisableGlass setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.4f]];
    }
    
    NSUserDefaults *defaults  = [NSUserDefaults standardUserDefaults];
    NSArray        *languages = [defaults objectForKey:@"AppleLanguages"];
    
    NSString *nsstrSelectedLanguage   = [languages objectAtIndex:0];
    
    NSString *nsstrDetails = nil;
    NSString *nsstrYes     = nil;
    NSString *nsstrNo      = nil;
    
    if ([nsstrSelectedLanguage isEqualToString:@"cy"])
    {
        nsstrDetails = @"Mae cynnwys newydd ar gael i’w lwytho i lawr. Ydych chi am ei lwytho i lawr yn awr?";
        nsstrYes     = @"Ydw";
        nsstrNo      = @"Nac ydw";
    }
    else
    {
        if ([nsstrSelectedLanguage isEqualToString:@"de"])
        {
            nsstrDetails = @"Es sind neue Daten verfügbar. Möchten sie diese jetzt herunterladen?";
            nsstrYes     = @"Ja";
            nsstrNo      = @"Nein";
        }
        else
        {
            nsstrDetails = @"New content is available for download. Do you want to download it now ?";
            nsstrYes     = @"Yes";
            nsstrNo      = @"No";
        }
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:nsstrDetails
                                                   delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [alert show];
    [alert release];
    
    //< Add the glass view to our superview
    [self.view addSubview:m_uivUADisableGlass];
}

/**
 * Complete the updating. Reload all data.
 *
 * @return none
 */
-(void)completeUpdate:(bool)bCompleted
{
    if (true == bCompleted)
    {
        [self performSelectorOnMainThread:@selector(reloadContent) withObject:nil waitUntilDone:NO];
        
        //< Write the latest update date
        NSUserDefaults *nsudPreferred = [NSUserDefaults standardUserDefaults];
        [nsudPreferred setObject:m_nsstrServerUpdateDate forKey:@"CWWFFCWLastUpdate"];
        [nsudPreferred synchronize];
    }
    
    //< End updating
    [self endDownloading];
}

/**
 * Reload all the content
 *
 * @return none
 */
-(void)reloadContent
{
    [m_guideInterfaceLoader reloadContent];
    m_uivAddedView = (WUIView*)[m_guideInterfaceLoader getView];
    
    //< Add the view to our window
    [self.view addSubview:[m_uivAddedView getView]];
    
    //< Load the view
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        CGRect rect = [m_uivAddedView getView].frame;
        rect.origin.y += 20;
        [[m_uivAddedView getView] setFrame:rect];
    }
}

/**
 *
 */
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //< If the user wants to upated we need to select the 3rd tab screen and start the update
    if (1 == buttonIndex)
    {
        //< Go to the 3d tab screen
        [m_uivAddedView setCurrentTab:1];
        
        //< Show the updating screen and start downloading
        [self showUpdatingScreen];
        [self startDownloading];
    }
    
    //< Remove the glass view
    [m_uivUADisableGlass removeFromSuperview];
}

/**
 * Show the updating screen
 */
-(void)showUpdatingScreen
{
    [m_guideInterfaceLoader showUpdatingScreen];
}

/**
 *
 */
-(void)startDownloading
{
    [m_guideInterfaceLoader startDownloading];
}

/**
 *
 */
-(void)endDownloading
{
    [m_guideInterfaceLoader endDownloading];
}

/**
 * No new updates are available.
 *
 * @return none
 */
-(void)updateNoDownloadsAvailabile
{
    [m_guideInterfaceLoader updateNoDownloadsAvailabile];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
