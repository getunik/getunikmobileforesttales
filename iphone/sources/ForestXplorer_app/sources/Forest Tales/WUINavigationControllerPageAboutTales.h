//
//  WUINavigationControllerPageAboutTales.h
//  ForestXplorer_app
//
//  Created by Adrian Hoitan on 17/07/14.
//  Copyright (c) 2014 Adrian Hoitan. All rights reserved.
//

#import "WUINavigationControllerPage.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMessageComposeViewController.h>

@interface WUINavigationControllerPageAboutTales : WUINavigationControllerPage <MFMailComposeViewControllerDelegate>
{
    int m_iEnableDebugging;
}

@end
