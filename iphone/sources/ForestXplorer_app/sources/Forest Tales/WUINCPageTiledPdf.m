//
//  WUINCPageTiledPdf.m
//  ForestXplorer_app
//
//  Created by Adrian Hoitan on 13/05/14.
//  Copyright (c) 2014 Adrian Hoitan. All rights reserved.
//

#import "WUINCPageTiledPdf.h"

@implementation WUINCPageTiledPdf

/**
 * Called when the content is loading
 *
 * @param iIndex Widget index
 * @return none
 */
-(void)loadContent:(int)iIndex
{
    [super loadContent:iIndex];
    
    RString *nsstrTitle = (RString*)[m_rmResourcesManager getResource:@"[@IDSPDFDisplayName]"];
    [self setNavigationTitle:[nsstrTitle getValue]];
}

@end
