//
//  WUITVPDFDocumentCell.h
//  ForestXplorer_app
//
//  Created by Adrian Hoitan on 1/10/14.
//  Copyright (c) 2014 Adrian Hoitan. All rights reserved.
//

#import "WUITableViewCell.h"

@interface WUITVPDFDocumentCell : WUITableViewCell
{
    NSString *m_nsstrTitle;
    NSString *m_nsstrMapPath;
}

@end
