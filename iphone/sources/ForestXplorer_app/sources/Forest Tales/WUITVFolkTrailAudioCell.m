//
//  WUITVFolkTrailAudioCell.m
//  ForestXplorer_app
//
//  Created by Adrian Hoitan on 1/6/14.
//  Copyright (c) 2014 Adrian Hoitan. All rights reserved.
//

#import "WUITVFolkTrailAudioCell.h"
#import "WUILabel.h"
#import "CCore.h"
#import <MediaPlayer/MediaPlayer.h>

@implementation WUITVFolkTrailAudioCell

/**
 * Initialize our widget with the informations that are coming from an XML node. The Node must be a valid macc node item.
 *
 * @param cxmlNode XML node that contains all the data.
 * @param rmResourcesManager Pointer to an resource manager that contain real values. It can be null in some cases.
 * @param iIndex If this item comes from an group with multiple items, this will specify the index of the item inside the list.
 * @param ccCore Pointer to the widget and resource creator.
 * @return id of the created item.
 */
-(id)initWithXMLNode:(CXMLElement*)cxmlNode resourcesManager:(CResourceManager*)rmResourcesManager index:(int)iIndex core:(CCore*)ccCore
{
    [super initWithXMLNode:cxmlNode resourcesManager:rmResourcesManager index:iIndex core:ccCore];
    
    //< Read Tale details
    if ([[self getID] isEqualToString:@"IDTVCAudioTrails"])
    {
        m_nsstrTitle     = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/audio_trails/audio_trail[%d]/title", iIndex + 1] defaultValue:@"" index:0];
        m_nsstrAudioPath = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/audio_trails/audio_trail[%d]/audiofile", iIndex + 1] defaultValue:@"" index:0];
    }
    
    if ([[self getID] isEqualToString:@"IDTVCFolkTrails"])
    {
        m_nsstrTitle     = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/folk_tales/folk_tale[%d]/title", iIndex + 1] defaultValue:@"" index:0];
        m_nsstrAudioPath = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/folk_tales/folk_tale[%d]/audiofile", iIndex + 1] defaultValue:@"" index:0];
    }
    
    if ([[self getID] isEqualToString:@"IDTVCHeritageTrails"])
    {
        m_nsstrTitle     = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/heritage_scrolls/heritage_scroll[%d]/title", iIndex + 1] defaultValue:@"" index:0];
        m_nsstrAudioPath = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/heritage_scrolls/heritage_scroll[%d]/audiofile", iIndex + 1] defaultValue:@"" index:0];
    }
    
    if ([m_nsstrAudioPath length] == 0)
    {
        [self setCellHeight:0];
        [[self getUITableCell] setHidden:YES];
    }
    
    return self;
}

/**
 * Load the contend displayed inside this table view cell
 *
 * @return none
 */
-(void)loadContent
{
    RStringLocalized *rsText = nil;
    
    if (false == m_bContentLoaded)
    {
        [super loadContent];
        
        if ([m_nsstrAudioPath length] > 0)
        {
            WUILabel *uilTitle = (WUILabel*)[self getChildWidget:@"IDLTitle"];
            if ([[self getID] isEqualToString:@"IDTVCAudioTrails"])
            {
                rsText = (RStringLocalized*)[m_rmResourcesManager getResource:@"[@IDRSLPlayAudioTrail]"];
            }
            if ([[self getID] isEqualToString:@"IDTVCFolkTrails"])
            {
                rsText = (RStringLocalized*)[m_rmResourcesManager getResource:@"[@IDRSLPlayFolkTale]"];
            }
            if ([[self getID] isEqualToString:@"IDTVCHeritageTrails"])
            {
                rsText = (RStringLocalized*)[m_rmResourcesManager getResource:@"[@IDRSLPlayAudioTale]"];
            }
            [uilTitle setText:[rsText getValue]];
        }
    }
}

///
///
///
-(NSString*)getActionID
{
    NSString *documentsDirectory = nil;
    WUINavigationControllerPage *ncpPage = nil;
    NSString *nsstrExtension = [m_nsstrAudioPath pathExtension];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    documentsDirectory = [paths objectAtIndex:0];
    documentsDirectory = [documentsDirectory stringByAppendingPathComponent:[CResourceManager uniqueIDFromString:m_nsstrAudioPath]];
    documentsDirectory = [documentsDirectory stringByAppendingPathExtension:nsstrExtension];
    
    NSURL *fileURL = [NSURL fileURLWithPath:documentsDirectory];

    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:documentsDirectory];
    if (false == fileExists)
    {
        fileURL = [NSURL URLWithString:m_nsstrAudioPath];
    }
    
    //< Create a media player that will play the audio
    MPMoviePlayerController *moviePlayerController = [[MPMoviePlayerController alloc] initWithContentURL:fileURL];
    [moviePlayerController.view setFrame:[UIScreen mainScreen].bounds];
    
    if ([[self getID] isEqualToString:@"IDTVCAudioTrails"])
    {
        ncpPage = (WUINavigationControllerPage*)[m_cCore getWidget:@"IDNCPAudioTrails"];
    }
    
    if ([[self getID] isEqualToString:@"IDTVCFolkTrails"])
    {
        ncpPage = (WUINavigationControllerPage*)[m_cCore getWidget:@"IDNCPFolkTrails"];
    }
    
    if ([[self getID] isEqualToString:@"IDTVCHeritageTrails"])
    {
        ncpPage = (WUINavigationControllerPage*)[m_cCore getWidget:@"IDNCPHeritageTrails"];
    }
    
    //< Add player to our view
    [ncpPage.m_uivcController.view addSubview:moviePlayerController.view];
    
    //< Go full screen
    moviePlayerController.fullscreen = YES;
    
    // Register for the playback finished notification
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(playerExitedFullScreen:) name: MPMoviePlayerDidExitFullscreenNotification object: moviePlayerController];
    
    //< Start playing
    [moviePlayerController play];
    
    return m_nsstrActionID;
}

/**
 * Notification called when the media player exits full screen
 *
 * @param aNotification Player object
 * @return none
 */
-(void)playerExitedFullScreen:(NSNotification*) aNotification
{
    //< Convert to the player object
    MPMoviePlayerController* theMovie = [aNotification object];
    
    //< Remove player from view
    [theMovie.view removeFromSuperview];
    
    //< Remove notification
    [[NSNotificationCenter defaultCenter] removeObserver: self name: MPMoviePlayerPlaybackDidFinishNotification object: theMovie];
    
    // Release the movie instance created in playMovieAtURL:
    [theMovie release];
}
@end
