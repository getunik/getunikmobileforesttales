//
//  WUITVFolkTrailMapCell.m
//  ForestXplorer_app
//
//  Created by Adrian Hoitan on 1/6/14.
//  Copyright (c) 2014 Adrian Hoitan. All rights reserved.
//

#import "WUITVFolkTrailMapCell.h"
#import "WUILabel.h"

@implementation WUITVFolkTrailMapCell

/**
 * Initialize our widget with the informations that are coming from an XML node. The Node must be a valid macc node item.
 *
 * @param cxmlNode XML node that contains all the data.
 * @param rmResourcesManager Pointer to an resource manager that contain real values. It can be null in some cases.
 * @param iIndex If this item comes from an group with multiple items, this will specify the index of the item inside the list.
 * @param ccCore Pointer to the widget and resource creator.
 * @return id of the created item.
 */
-(id)initWithXMLNode:(CXMLElement*)cxmlNode resourcesManager:(CResourceManager*)rmResourcesManager index:(int)iIndex core:(CCore*)ccCore
{
    [super initWithXMLNode:cxmlNode resourcesManager:rmResourcesManager index:iIndex core:ccCore];
    
    if ([[self getID] isEqualToString:@"IDTVCAudioTrails"])
    {
        m_nsstrTitle   = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/audio_trails/audio_trail[%d]/title", iIndex + 1] defaultValue:@"" index:0];
        m_nsstrMapPath = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/audio_trails/audio_trail[%d]/map", iIndex + 1] defaultValue:@"" index:0];
    }
    
    if ([[self getID] isEqualToString:@"IDTVCFolkTrails"])
    {
        m_nsstrTitle   = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/folk_tales/folk_tale[%d]/title", iIndex + 1] defaultValue:@"" index:0];
        m_nsstrMapPath = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/folk_tales/folk_tale[%d]/map", iIndex + 1] defaultValue:@"" index:0];
    }
    
    if ([[self getID] isEqualToString:@"IDTVCHeritageTrails"])
    {
        m_nsstrTitle   = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/heritage_scrolls/heritage_scroll[%d]/title", iIndex + 1] defaultValue:@"" index:0];
        m_nsstrMapPath = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/heritage_scrolls/heritage_scroll[%d]/map", iIndex + 1] defaultValue:@"" index:0];
    }
    
    if ([m_nsstrMapPath length] == 0)
    {
        [self setCellHeight:0];
    }
    
    return self;
}

/**
 * Load the contend displayed inside this table view cell
 *
 * @return none
 */
-(void)loadContent
{
    if (false == m_bContentLoaded)
    {
        [super loadContent];
        
        if ([m_nsstrMapPath length] > 0)
        {
            WUILabel *uilTitle = (WUILabel*)[self getChildWidget:@"IDLTitle"];
            [uilTitle setText:[NSString stringWithFormat:@"%@ Map (pdf)", m_nsstrTitle]];
        }
    }
}

///
///
///
-(NSString*)getActionID
{
    RString  *rstrTrailPath = (RString*)[m_rmResourcesManager getResource:@"[@IDSTrailMapPath]"];
    [rstrTrailPath setValue:m_nsstrMapPath];
    
    return m_nsstrActionID;
}

@end
