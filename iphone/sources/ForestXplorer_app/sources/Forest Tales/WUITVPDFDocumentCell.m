//
//  WUITVPDFDocumentCell.m
//  ForestXplorer_app
//
//  Created by Adrian Hoitan on 1/10/14.
//  Copyright (c) 2014 Adrian Hoitan. All rights reserved.
//

#import "WUITVPDFDocumentCell.h"
#import "WUILabel.h"

@implementation WUITVPDFDocumentCell

/**
 * Initialize our widget with the informations that are coming from an XML node. The Node must be a valid macc node item.
 *
 * @param cxmlNode XML node that contains all the data.
 * @param rmResourcesManager Pointer to an resource manager that contain real values. It can be null in some cases.
 * @param iIndex If this item comes from an group with multiple items, this will specify the index of the item inside the list.
 * @param ccCore Pointer to the widget and resource creator.
 * @return id of the created item.
 */
-(id)initWithXMLNode:(CXMLElement*)cxmlNode resourcesManager:(CResourceManager*)rmResourcesManager index:(int)iIndex core:(CCore*)ccCore
{
    [super initWithXMLNode:cxmlNode resourcesManager:rmResourcesManager index:iIndex core:ccCore];
    
    if ([[self getID] isEqualToString:@"IDTVCAudioMap"])
    {
        m_nsstrTitle   = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/audio_trails/audio_trail[%d]/title", iIndex + 1] defaultValue:@"" index:0];
        m_nsstrMapPath = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/audio_trails/audio_trail[%d]/map", iIndex + 1] defaultValue:@"" index:0];
    }
    if ([[self getID] isEqualToString:@"IDTVCAudioInfo"])
    {
        m_nsstrTitle   = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/audio_trails/audio_trail[%d]/title", iIndex + 1] defaultValue:@"" index:0];
        m_nsstrMapPath = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/audio_trails/audio_trail[%d]/info", iIndex + 1] defaultValue:@"" index:0];
    }
    if ([[self getID] isEqualToString:@"IDTVCAudioText"])
    {
        m_nsstrTitle   = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/audio_trails/audio_trail[%d]/title", iIndex + 1] defaultValue:@"" index:0];
        m_nsstrMapPath = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/audio_trails/audio_trail[%d]/text", iIndex + 1] defaultValue:@"" index:0];
    }
    
    if ([[self getID] isEqualToString:@"IDTVCFolkMap"])
    {
        m_nsstrTitle   = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/folk_tales/folk_tale[%d]/title", iIndex + 1] defaultValue:@"" index:0];
        m_nsstrMapPath = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/folk_tales/folk_tale[%d]/map", iIndex + 1] defaultValue:@"" index:0];
    }
    if ([[self getID] isEqualToString:@"IDTVCFolkInfo"])
    {
        m_nsstrTitle   = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/folk_tales/folk_tale[%d]/title", iIndex + 1] defaultValue:@"" index:0];
        m_nsstrMapPath = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/folk_tales/folk_tale[%d]/info", iIndex + 1] defaultValue:@"" index:0];
    }
    if ([[self getID] isEqualToString:@"IDTVCFolkText"])
    {
        m_nsstrTitle   = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/folk_tales/folk_tale[%d]/title", iIndex + 1] defaultValue:@"" index:0];
        m_nsstrMapPath = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/folk_tales/folk_tale[%d]/text", iIndex + 1] defaultValue:@"" index:0];
    }
    
    if ([[self getID] isEqualToString:@"IDTVCHeritageMap"])
    {
        m_nsstrTitle   = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/heritage_scrolls/heritage_scroll[%d]/title", iIndex + 1] defaultValue:@"" index:0];
        m_nsstrMapPath = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/heritage_scrolls/heritage_scroll[%d]/map", iIndex + 1] defaultValue:@"" index:0];
    }
    if ([[self getID] isEqualToString:@"IDTVCHeritageScroll"])
    {
        m_nsstrTitle   = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/heritage_scrolls/heritage_scroll[%d]/title", iIndex + 1] defaultValue:@"" index:0];
        m_nsstrMapPath = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/heritage_scrolls/heritage_scroll[%d]/scroll", iIndex + 1] defaultValue:@"" index:0];
    }
    
    if ([m_nsstrMapPath length] == 0)
    {
        [self setCellHeight:0];
        [[self getUITableCell] setHidden:YES];
    }
    
    return self;
}

/**
 * Load the contend displayed inside this table view cell
 *
 * @return none
 */
-(void)loadContent
{
    if (false == m_bContentLoaded)
    {
        [super loadContent];
        
        if ([m_nsstrMapPath length] > 0)
        {
            WUILabel *uilTitle = (WUILabel*)[self getChildWidget:@"IDLTitle"];
            if (([[self getID] isEqualToString:@"IDTVCAudioMap"]) || ([[self getID] isEqualToString:@"IDTVCFolkMap"]) || ([[self getID] isEqualToString:@"IDTVCHeritageMap"]))
            {
                RStringLocalized *rsText = (RStringLocalized*)[m_rmResourcesManager getResource:@"[@IDRSLDisplayMap]"];
                [uilTitle setText:[rsText getValue]];
            }
            if (([[self getID] isEqualToString:@"IDTVCAudioInfo"]) || ([[self getID] isEqualToString:@"IDTVCFolkInfo"]))
            {
                RStringLocalized *rsText = (RStringLocalized*)[m_rmResourcesManager getResource:@"[@IDRSLDisplayInfo]"];
                [uilTitle setText:[rsText getValue]];
            }
            if (([[self getID] isEqualToString:@"IDTVCAudioText"]) || ([[self getID] isEqualToString:@"IDTVCFolkText"]))
            {
                RStringLocalized *rsText = (RStringLocalized*)[m_rmResourcesManager getResource:@"[@IDRSLDisplayText]"];
                [uilTitle setText:[rsText getValue]];
            }
            if ([[self getID] isEqualToString:@"IDTVCHeritageScroll"])
            {
                RStringLocalized *rsText = (RStringLocalized*)[m_rmResourcesManager getResource:@"[@IDRSLDisplayScroll]"];
                [uilTitle setText:[rsText getValue]];
            }
        }
    }
}

///
///
///
-(NSString*)getActionID
{
    RString  *rstrTrailPath = (RString*)[m_rmResourcesManager getResource:@"[@IDSTrailMapPath]"];
    [rstrTrailPath setValue:m_nsstrMapPath];
    
    RString *rsNavigationPageTitle = (RString*)[m_rmResourcesManager getResource:@"[@IDSPDFDisplayName]"];
    if (([[self getID] isEqualToString:@"IDTVCAudioMap"]) || ([[self getID] isEqualToString:@"IDTVCFolkMap"]) || ([[self getID] isEqualToString:@"IDTVCHeritageMap"]))
    {
        [rsNavigationPageTitle setValue:@"[@IDRSLPDFViewerMap]"];
    }
    if (([[self getID] isEqualToString:@"IDTVCAudioInfo"]) || ([[self getID] isEqualToString:@"IDTVCFolkInfo"]))
    {
        [rsNavigationPageTitle setValue:@"[@IDRSLPDFViewerInfo]"];
    }
    if (([[self getID] isEqualToString:@"IDTVCAudioText"]) || ([[self getID] isEqualToString:@"IDTVCFolkText"]))
    {
        [rsNavigationPageTitle setValue:@"[@IDRSLPDFViewerScript]"];
    }
    if ([[self getID] isEqualToString:@"IDTVCHeritageScroll"])
    {
        [rsNavigationPageTitle setValue:@"[@IDRSLPDFViewerScroll]"];
    }
    
    return m_nsstrActionID;
}

@end
