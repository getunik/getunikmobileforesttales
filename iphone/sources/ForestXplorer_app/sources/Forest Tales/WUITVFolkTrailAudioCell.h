//
//  WUITVFolkTrailAudioCell.h
//  ForestXplorer_app
//
//  Created by Adrian Hoitan on 1/6/14.
//  Copyright (c) 2014 Adrian Hoitan. All rights reserved.
//

#import "WUITableViewCell.h"

@interface WUITVFolkTrailAudioCell : WUITableViewCell
{
    NSString *m_nsstrTitle;
    NSString *m_nsstrAudioPath;
}
@end
