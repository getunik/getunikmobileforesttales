//
//  WUINCPageTalesDetails.m
//  ForestXplorer_app
//
//  Created by Adrian Hoitan on 1/10/14.
//  Copyright (c) 2014 Adrian Hoitan. All rights reserved.
//

#import "WUINCPageTalesDetails.h"
#import "CCore.h"
#import "WUIButton.h"
#import "WUITableViewCell.h"

@implementation WUINCPageTalesDetails

/**
 * Function called before the pop animation to happen.
 *
 * @return none
 */
-(void)willUnloadContent
{
    [m_cdContentDownload abortDownload];
}

/**
 * Called when the content is loading
 *
 * @param iIndex Widget index
 * @return none
 */
-(void)loadContent:(int)iIndex
{
    //< Retreive the number object associated with the selected POI and set the selected number
    RNumber *rnSelectedPOI = (RNumber*)[m_rmResourcesManager getResource:@"[@RNSelectedPOI]"];
    [rnSelectedPOI setValue:iIndex];
    
    RNumber *rnSelectedIndex = (RNumber*)[m_rmResourcesManager getResource:@"[@RNPOIIndex]"];
    [rnSelectedIndex setValue:(iIndex + 1)];
    
    //< Superclass call
    [super loadContent:iIndex];
    
    //< Hide the download button if content files are available
    RXPathNodes *rsContentData = (RXPathNodes*)[m_rmResourcesManager getResource:@"[@DataXMLList]"];
    CXMLElement *cxmlElement = [rsContentData getCXMLElementAtPath:@"[@DataXMLList]" index:[rnSelectedIndex getValue] - 1];
    
    m_cdContentDownload = [[CNSDataXMLNodeContentDownload alloc] init];
    [m_cdContentDownload setDelegate:self];
    [m_cdContentDownload addDownloadFileExtension:@"jpg"];
    [m_cdContentDownload addDownloadFileExtension:@"pdf"];
    [m_cdContentDownload addDownloadFileExtension:@"mp3"];
    
    long lNewContentSize = [m_cdContentDownload getSizeOfNewContentWithXMLNode:cxmlElement];
    if (0 == lNewContentSize)
    {
        WUITableViewCell *wuitvcDownloadButton = (WUITableViewCell*)[self getChildWidget:@"IDTVCDownloadTales"];
        [[wuitvcDownloadButton getUITableCell] setHidden:YES];
        [wuitvcDownloadButton setCellHeight:0];
    }
    else
    {
        WUITableViewCell *wuitvcDownloadButton = (WUITableViewCell*)[self getChildWidget:@"IDTVCDownloadTales"];
        WUIButton *uibDownloadButton = (WUIButton*)[wuitvcDownloadButton getChildWidget:@"IDBDownloadButton"];
        RStringLocalized *rsText = (RStringLocalized*)[m_rmResourcesManager getResource:@"[@IDSDownloadSiteTales]"];
        
        //< Add 1 MB always since the size can be very close to 0
        [uibDownloadButton setTitle:[NSString stringWithFormat:@"%@ (%dMB)", [rsText getValue], lNewContentSize / 1048576 + 1]];
    }
}

/**
 * Send an callback event to our parent widgets informing about the new action.
 *
 * @param nsstrSenderID Sender ID
 * @param nsnIndex Index number of the widget that is calling the callback
 * @param nsnActionType Specific action that was triggered
 * @return None
 **/
-(void)sendCallbackEvent:(NSString *)nsstrSenderID index:(NSNumber*)nsnIndex actionType:(NSNumber*)nsnActionType
{
    bool bHandled = false;
    
    if ([nsstrSenderID isEqualToString:@"IDBDownloadContent"])
    {
        [self startDownload];
        
        bHandled = true;
    }
    
    if (false == bHandled)
    {
        [super sendCallbackEvent:nsstrSenderID index:nsnIndex actionType:nsnActionType];
    }
}

-(void)startDownload
{
    RNumber *rnSelectedIndex = (RNumber*)[m_rmResourcesManager getResource:@"[@RNPOIIndex]"];
    RXPathNodes *rsContentData = (RXPathNodes*)[m_rmResourcesManager getResource:@"[@DataXMLList]"];
    CXMLElement *cxmlElement = [rsContentData getCXMLElementAtPath:@"[@DataXMLList]" index:[rnSelectedIndex getValue] - 1];
    
    [m_cdContentDownload downloadAllContentFromXMLNode:cxmlElement withUser:@"admin" withPassword:@"g3tun1k"];
    
    WUIButton *uibDownloadButton = (WUIButton*)[self getChildWidget:@"IDBDownloadButton"];
    [[uibDownloadButton getView] setHidden:YES];
    
    WUIView *uivDownloadView = (WUIView*)[self getChildWidget:@"IDVDownloadView"];
    [[uivDownloadView getView] setHidden:NO];
    m_uipProgress = [[UIProgressView alloc] initWithFrame:CGRectMake(20, 30, 280, 20)];
    [[uivDownloadView getView] addSubview:m_uipProgress];
}

/**
 * Delegate function called when the NSData finished loading.
 *
 * @param fFileProgress File progress. Value between 0-1.
 * @param fTotalProgress Todal file progress. Value between 0-1.
 * @return none
 */
-(void)NSDataManagerDidReceiveDataWithFileProgress:(float)fFileProgress totalProgress:(float)fTotalProgress
{
    [m_uipProgress setProgress:fTotalProgress];
    
    //< The download is completed
    if (1 == fTotalProgress)
    {
        //< Hide the download button
        WUITableViewCell *wuitvcDownloadButton = (WUITableViewCell*)[self getChildWidget:@"IDTVCDownloadTales"];
        [[wuitvcDownloadButton getUITableCell] setHidden:YES];
        [wuitvcDownloadButton setCellHeight:0];
        
        //< Reload table view
        WUITableView *wuitvTableView = (WUITableView*)[self getChildWidget:@"IDTVDetailPOI"];
        [wuitvTableView reloadData];
    }
    
    return;
}

/**
 * Delegate function called when the NSData finished loading.
 *
 * @param error An error object containing details of why the connection failed to load the request successfully.
 * @return none
 */
-(void)NSDataManagerContentDidFailWithError:(NSError*)error
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No network connection"
                                                    message:@"Data download not possible. Please check your internet connection."
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];
    
    return;
}

@end
