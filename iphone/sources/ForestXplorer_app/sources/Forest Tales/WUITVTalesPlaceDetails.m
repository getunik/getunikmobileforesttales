//
//  WUITVTalesPlaceDetails.m
//  ForestXplorer_app
//
//  Created by Adrian Hoitan on 1/6/14.
//  Copyright (c) 2014 Adrian Hoitan. All rights reserved.
//

#import "WUITVTalesPlaceDetails.h"
#import "WUITableViewCell.h"

@implementation WUITVTalesPlaceDetails

/**
 * Initialize our widget with the informations that are coming from an XML node. The Node must be a valid macc node item.
 *
 * @param cxmlNode XML node that contains all the data.
 * @param rmResourcesManager Pointer to an resource manager that contain real values. It can be null in some cases.
 * @param iIndex If this item comes from an group with multiple items, this will specify the index of the item inside the list.
 * @param ccCore Pointer to the widget and resource creator.
 * @return id of the created item.
 */
-(id)initWithXMLNode:(CXMLElement*)cxmlNode resourcesManager:(CResourceManager*)rmResourcesManager index:(int)iIndex core:(CCore*)ccCore parentWidget:(IWidget *)iwParentWidget
{
    [super initWithXMLNode:cxmlNode resourcesManager:rmResourcesManager index:iIndex core:ccCore parentWidget:iwParentWidget];
    
    //< Check if we have any audioguides
    int iAudioTrails = [m_rmResourcesManager getIntAttributeValue:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/audio_trails/audio_trail" defaultValue:0 index:0];
    if (0 == iAudioTrails)
    {
        WUITableViewCell *uitvcCell = (WUITableViewCell*)[self getChildWidget:@"IDTVCDetailsAudioTrails"];
        [uitvcCell setCellHeight:0];
        [[uitvcCell getUITableCell] setHidden:YES];
    }
    
    //< Check if we have any folk tales
    int iFolkTales = [m_rmResourcesManager getIntAttributeValue:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/folk_tales/folk_tale" defaultValue:0 index:0];
    if (0 == iFolkTales)
    {
        WUITableViewCell *uitvcCell = (WUITableViewCell*)[self getChildWidget:@"IDTVCDetailsFolkTales"];
        [uitvcCell setCellHeight:0];
        [[uitvcCell getUITableCell] setHidden:YES];
    }
    
    //< Check if we have any audioguides
    int iHeritageScrolls = [m_rmResourcesManager getIntAttributeValue:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/heritage_scrolls/heritage_scroll" defaultValue:0 index:0];
    if (0 == iHeritageScrolls)
    {
        WUITableViewCell *uitvcCell = (WUITableViewCell*)[self getChildWidget:@"IDTVCDetailsHeritageScrolls"];
        [uitvcCell setCellHeight:0];
        [[uitvcCell getUITableCell] setHidden:YES];
    }
    
    [self reloadData];
    
    return self;
}

@end
