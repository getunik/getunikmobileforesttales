//
//  WUINavigationControllerPageAboutTales.m
//  ForestXplorer_app
//
//  Created by Adrian Hoitan on 17/07/14.
//  Copyright (c) 2014 Adrian Hoitan. All rights reserved.
//

#import "WUINavigationControllerPageAboutTales.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import "UAnalyticsTracking.h"
#import "USharingActivityProvider.h"
#import "AppDelegate.h"

@implementation WUINavigationControllerPageAboutTales

/**
 * Send an callback event to our parent widgets informing about the new action.
 *
 * @param nsstrSenderID Sender ID
 * @param nsnIndex Index number of the widget that is calling the callback
 * @param nsnActionType Specific action that was triggered
 * @return None
 **/
-(void)sendCallbackEvent:(NSString *)nsstrSenderID index:(NSNumber*)nsnIndex actionType:(NSNumber*)nsnActionType
{
    BOOL bHandled = NO;
    
    if ([nsstrSenderID isEqualToString:@"IDCPShare"])
    {
        USharingActivityProvider *sharingActivityProvider = [[USharingActivityProvider alloc] initWithPlaceholderItem:@{@"body":@"", @"url":@""}];
        [sharingActivityProvider setResourceManager:m_rmResourcesManager];
        
        NSString *nsstrMessage = [m_rmResourcesManager getStrAttributeValue:@"[@SettingsXMLList]./setting/share_ios" defaultValue:@"" index:0];
        NSArray *items = @[nsstrMessage];
        
        //< Display sharing options
        UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:items applicationActivities:nil];
        activityController.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll];
        //[m_uincNavigationController presentViewController:activityController animated:YES completion:nil];
        
        AppDelegate *m_appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [m_appDelegate.m_uivViewController presentViewController:activityController animated:YES completion:nil];
        
#ifdef INCLUDE_GOOGLE_ANALYTICS
        [[UAnalyticsTracking sharedUAnalyticsHelper] trackGoogleAnalyticsPage:[NSString stringWithFormat:@"/iPhone/%@/about/share", [m_rmResourcesManager getSelectedLanguage]]];
#endif
        //< Cleanup
        SAFE_RELEASE(nsstrMessage);
        
        bHandled = YES;
    }
    
    if ([nsstrSenderID isEqualToString:@"IDCPRate"])
    {
        NSString *nsstrURL = [m_rmResourcesManager getStrAttributeValue:@"[@SettingsXMLList]./setting/itunes_url" defaultValue:@"" index:0];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:nsstrURL]];
#ifdef INCLUDE_GOOGLE_ANALYTICS
        [[UAnalyticsTracking sharedUAnalyticsHelper] trackGoogleAnalyticsPage:[NSString stringWithFormat:@"/iPhone/%@/about/rate", [m_rmResourcesManager getSelectedLanguage]]];
#endif
        //< Cleanup
        SAFE_RELEASE(nsstrURL);
        
        bHandled = YES;
    }
    
    if ([nsstrSenderID isEqualToString:@"IDCPContact"])
    {
        NSUserDefaults *defaults  = [NSUserDefaults standardUserDefaults];
        NSArray        *languages = [defaults objectForKey:@"AppleLanguages"];
        
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        NSString *nsstrSelectedLanguage   = [languages objectAtIndex:0];
        if ([nsstrSelectedLanguage isEqualToString:@"cy"])
        {
            [controller setToRecipients:[NSArray arrayWithObject:@"chwedl.app@cyfoethnaturiolcymru.gov.uk"]];
        }
        else
        {
            [controller setToRecipients:[NSArray arrayWithObject:@"tales.app@naturalresourceswales.gov.uk"]];
        }
        [controller setSubject:@"Feedback on iPhone App"];
        [controller setMessageBody:@"" isHTML:NO];
        if (controller) [self.m_uivcController presentModalViewController:controller animated:YES];
        [controller release];
#ifdef INCLUDE_GOOGLE_ANALYTICS
        [[UAnalyticsTracking sharedUAnalyticsHelper] trackGoogleAnalyticsPage:[NSString stringWithFormat:@"/iPhone/%@/about/contactdeveloper", [m_rmResourcesManager getSelectedLanguage]]];
#endif
        bHandled = YES;
    }
    
    if ([nsstrSenderID isEqualToString:@"IDCPOtherApps"])
    {
        NSString *nsstrLink = [m_rmResourcesManager getStrAttributeValue:@"[@SettingsXMLList]./setting/itunes_publisher_url" defaultValue:@"" index:0];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:nsstrLink]];
        
        bHandled = true;
    }
    
    if ([nsstrSenderID isEqualToString:@"IDBAppIconDebug"])
    {
        m_iEnableDebugging++;
        if (10 == m_iEnableDebugging)
        {
            m_iEnableDebugging = 0;
            
            //< Read the current debugging mode
            NSUserDefaults *defaults  = [NSUserDefaults standardUserDefaults];
            NSString *nsstrDebugging = [defaults objectForKey:@"IDDebuggingEnabled"];
            
            if ((nil != nsstrDebugging) && ([nsstrDebugging isEqualToString:@"false"]))
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information"
                                                                message:@"Preview mode enabled. Please restart the app."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                [alert release];
                
                [defaults setObject:@"true" forKey:@"IDDebuggingEnabled"];
                [defaults synchronize];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information"
                                                                message:@"Preview mode disabled. Please restart the app."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                [alert release];
                
                [defaults setObject:@"false" forKey:@"IDDebuggingEnabled"];
                [defaults synchronize];
            }
        }
    }
    
    if (NO == bHandled)
    {
        [super sendCallbackEvent:nsstrSenderID index:nsnIndex actionType:nsnActionType];
    }
}

/**
 * Tells the delegate that the user wants to dismiss the mail composition view.
 *
 * @param controller The view controller object managing the mail composition view.
 * @param result The result of the user’s action.
 * @param error If an error occurred, this parameter contains an error object with information about the type of failure.
 * @return none
 */
-(void)mailComposeController: (MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultSent:
            break;
        case MFMailComposeResultFailed:
            break;
        default:
            break;
    }
    
    [self.m_uivcController dismissModalViewControllerAnimated:YES];
}

@end
