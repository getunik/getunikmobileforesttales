//
//  WUINCPageTalesDetails.h
//  ForestXplorer_app
//
//  Created by Adrian Hoitan on 1/10/14.
//  Copyright (c) 2014 Adrian Hoitan. All rights reserved.
//

#import "WUINavigationControllerPage.h"
#import "CNSDataXMLNodeContentDownload.h"
#import "CNSDataDownloadManager.h"

@interface WUINCPageTalesDetails : WUINavigationControllerPage <CNSDataDownloadManagerDelegate>
{
    CNSDataXMLNodeContentDownload *m_cdContentDownload;
    UIProgressView *m_uipProgress;
}

@end
