//
//  WUITableViewPOIList.h
//  General_app
//
//  Created by ahn on 11.10.11.
//  Copyright 2011 getunik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WUITableView.h"
#import <MapKit/MapKit.h>

@interface WUITableViewPOIList : WUITableView <CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
    CLLocation        *currentLocation;
}

@end
