//
//  WUITMetricsView.m
//  General_app
//
//  Created by Adrian Hoitan on 21.08.12.
//  Copyright (c) 2012 getunik. All rights reserved.
//

#import "WUITMetricsView.h"
#import "WUITableViewPOIList.h"
#import "WUITableViewCell.h"
#import "CCore.h"

@implementation WUITMetricsView

///
///
///
-(id)initWithXMLNode:(CXMLElement*)cxmlNode resourcesManager:(CResourceManager*)rmResourcesManager index:(int)iIndex core:(CCore*)ccCore parentWidget:(IWidget *)iwParentWidget
{
    [super initWithXMLNode:cxmlNode resourcesManager:rmResourcesManager index:iIndex core:ccCore parentWidget:iwParentWidget];
    
    [self updateDisplay];
    return self;
}

///
/// Event recived when a specific element was selected
/// \param tableView Table view object
/// \param indexPath Element index
/// \return none
///
- (void)tableView: (UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    WUITableViewSection *uitvsSection = [m_nsmaTableSections objectAtIndex:indexPath.section];
    
    switch (indexPath.row)
    {
        case 0:
            [self writeMetrics:@"km"];
        break;
        case 1:
            [self writeMetrics:@"mi"];
        break;
        default:
        break;
    }

    [super tableView:tableView didSelectRowAtIndexPath:indexPath];

    [self reloadData];
    [self updateDisplay];
}

///
///
///
-(void)writeMetrics:(NSString*)nsstrMetrics
{
    //< Write the latest update date
    NSUserDefaults *nsudPreferred = [NSUserDefaults standardUserDefaults];
    [nsudPreferred setObject:nsstrMetrics forKey:@"CWWFFCWUnits"];
    [nsudPreferred synchronize];
}

/**
 *
 */
-(void)updateDisplay
{
    WUITableViewCell *cell          = nil;
    NSUserDefaults   *nsudPreferred = [NSUserDefaults standardUserDefaults];
    NSString         *nsstrMetrics  = [nsudPreferred objectForKey:@"CWWFFCWUnits"];
    
    if ([nsstrMetrics isEqualToString:@"mi"])
    {
        cell = (WUITableViewCell*)[m_cCore getWidget:@"IDCMetricsKM"];
        [cell setAccessory:0];

        cell = (WUITableViewCell*)[m_cCore getWidget:@"IDCMetricsMI"];
        [cell setAccessory:3];
    }
    else
    {
        cell = (WUITableViewCell*)[m_cCore getWidget:@"IDCMetricsKM"];
        [cell setAccessory:3];

        cell = (WUITableViewCell*)[m_cCore getWidget:@"IDCMetricsMI"];
        [cell setAccessory:0];
    }

    WUITableViewPOIList *uitvPOITable = (WUITableViewPOIList*)[m_cCore getWidget:@"IDTVNearbyList"];
    [uitvPOITable updateMetrics];
}

@end
