//
//  WUILabelAccessPointList.m
//  General_app
//
//  Created by Adrian Hoitan on 26.01.12.
//  Copyright (c) 2012 getunik. All rights reserved.
//

#import "WUILabelAccessPointList.h"

@implementation WUILabelAccessPointList

/**
 * Load the property called text.
 *
 * @param cxmlAttributeNode XML node that contains the data for this attribute.
 * @return none
 * @note This function can be overwritten if we want to load a different data
 */
-(void)loadAttributeText:(CXMLNode*)cxmlAttributeNode
{
    NSString *nsstrFormat = nil;
    NSString *nsstrText   = nil;
    
    //< Set the displayed text
    if (nil != cxmlAttributeNode)
    {
        nsstrText = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/access_points/access_point[%d]/title_of_the_ap", m_iWidgetIndex + 1] defaultValue:[NSString stringWithString:@""] index:m_iWidgetIndex];
        
        //< Check if the text has an formatting attribute and set the UILabel property
        cxmlAttributeNode = [m_rmResourcesManager getXMLNodeForAttribute:@"formatting" fromElementNode:m_cxmlNode];
        if (nil != cxmlAttributeNode)
        {
            nsstrFormat = [m_rmResourcesManager getStrAttributeValue:[cxmlAttributeNode stringValue] defaultValue:[NSString stringWithString:@""] index:m_iWidgetIndex];
        }
        
        if (nil != nsstrFormat)
        {
            [m_uilLabel setText:[NSString stringWithFormat:nsstrFormat, nsstrText]];
        }
        else
        {
            [m_uilLabel setText:nsstrText];
        }
        
        [nsstrFormat release];
        [nsstrText release];
    }
}

@end
