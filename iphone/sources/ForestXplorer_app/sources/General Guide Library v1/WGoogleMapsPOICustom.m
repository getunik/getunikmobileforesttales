//
//  WGoogleMapsPOICustom.m
//  General_app
//
//  Created by ahn on 10.10.11.
//  Copyright 2011 getunik. All rights reserved.
//

#import "WGoogleMapsPOICustom.h"


@implementation WGoogleMapsPOICustom

/**
 * Initialize our widget with the informations that are coming from an XML node. The Node must be a valid macc node item.
 * 
 * @param cxmlNode XML node that contains all the data.
 * @param rmResourcesManager Pointer to an resource manager that contain real values. It can be null in some cases.
 * @param iIndex If this item comes from an group with multiple items, this will specify the index of the item inside the list.
 * @param ccCore Pointer to the widget and resource creator.
 * @return id of the created item.
 */
-(id)initWithXMLNode:(CXMLElement*)cxmlNode resourcesManager:(CResourceManager*)rmResourcesManager index:(int)iIndex core:(CCore*)ccCore
{
    [super initWithXMLNode:cxmlNode resourcesManager:rmResourcesManager index:iIndex core:ccCore];
    
    m_nsmaActivities = [[NSMutableArray alloc] init];
    m_iFarmColor     = 0;
    
    //< Read and store all the activities that this POI can have
    int iActivities = [m_rmResourcesManager getIntAttributeValue:@"[@DataXMLList]./activity" defaultValue:0 index:iIndex];
    for (int iStep = 0; iStep < iActivities; iStep++)
    {
        NSString *nsstrActivityID = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLList]./activity[%d]", iStep + 1] defaultValue:@"" index:iIndex];
        [m_nsmaActivities addObject:nsstrActivityID];
    }
    
    //< Check if the POI is under windfarm construction
    NSString *nsstrFarmColor = [m_rmResourcesManager getStrAttributeValue:@"[@DataXMLList]./map_dot_color" defaultValue:@"green" index:iIndex];
    
    if ([nsstrFarmColor isEqualToString:@"green"])
    {
        m_iFarmColor = 0;
    }
    if ([nsstrFarmColor isEqualToString:@"red"])
    {
        m_iFarmColor = 1;
    }
    if ([nsstrFarmColor isEqualToString:@"blue"])
    {
        m_iFarmColor = 2;
    }
    
    return self;
}

/**
 * Check if a specific activity is listed by this POI
 *
 * @param iActivityID Id of the activity that the user has selected
 * @return int category value
 */
-(BOOL)isActivityListed:(int)iActivityID
{
    BOOL bReturnVal = NO;
    
    for (int iStep = 0; iStep < [m_nsmaActivities count]; iStep++)
    {
        if ([[m_nsmaActivities objectAtIndex:iStep] intValue] == iActivityID)
        {
            bReturnVal = YES;
        }
    }
    
    //< The -1 value represents the all activities
    if (-1 == iActivityID)
    {
        bReturnVal = YES;
    }
    
    return bReturnVal;
}

/**
 * Is the windfar under construction or not.
 *
 * @return true if windfarm is underconstruction
 */
//-(int)isWindfarmConstruction
-(int)getFarmColor
{
    return m_iFarmColor;
}
@end
