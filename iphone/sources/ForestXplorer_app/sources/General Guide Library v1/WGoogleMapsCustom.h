//
//  WGoogleMapsCustom.h
//  General_app
//
//  Created by ahn on 07.10.11.
//  Copyright 2011 getunik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WGoogleMaps.h"

@interface WGoogleMapsCustom : WGoogleMaps
{
}

/**
 * Update the displayed points on the map based on the selected activity
 *
 * @return none
 */
-(void)updatePOIDisplay;

@end
