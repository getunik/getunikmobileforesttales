//
//  WUINCPageDetails.h
//  General_app
//
//  Created by Adrian Hoitan on 23.01.12.
//  Copyright (c) 2012 getunik. All rights reserved.
//

#import "WUINavigationControllerPage.h"
#import "CNSDataXMLNodeContentDownload.h"
#import "CNSDataDownloadManager.h"

@interface WUINCPageDetails : WUINavigationControllerPage <CNSDataDownloadManagerDelegate>
{
    CNSDataXMLNodeContentDownload *m_cdContentDownload;
    UIProgressView *m_uipProgress;
}

@end
