//
//  WUINCPMapWithFilter.m
//  General_app
//
//  Created by ahn on 06.10.11.
//  Copyright 2011 getunik. All rights reserved.
//

#import "WUINCPMapWithFilter.h"
#import "WUIView.h"
#import "CCore.h"
#import "WUIButton.h"
#import "WGoogleMapsCustom.h"
#import "WUITableViewPOIList.h"
#import <QuartzCore/CAAnimation.h>

@implementation WUINCPMapWithFilter

/**
 * Initialize our widget with the informations that are coming from an XML node. The Node must be a valid macc node item.
 * 
 * @param cxmlNode XML node that contains all the data.
 * @param rmResourcesManager Pointer to an resource manager that contain real values. It can be null in some cases.
 * @param iIndex If this item comes from an group with multiple items, this will specify the index of the item inside the list.
 * @param ccCore Pointer to the widget and resource creator.
 * @return id of the created item.
 */
-(id)initWithXMLNode:(CXMLElement*)cxmlNode resourcesManager:(CResourceManager*)rmResourcesManager index:(int)iIndex core:(CCore*)ccCore
{
    [super initWithXMLNode:cxmlNode resourcesManager:rmResourcesManager index:iIndex core:ccCore];
    
    return self;
}

/**
 * Send an callback event to our parent widgets informing about the new action.
 *
 * @param nsstrSenderID Sender ID
 * @param nsnIndex Index number of the widget that is calling the callback
 * @param nsnActionType Specific action that was triggered
 * @return None
 **/
-(void)sendCallbackEvent:(NSString *)nsstrSenderID index:(NSNumber*)nsnIndex actionType:(NSNumber*)nsnActionType
{
    BOOL bHandled = NO;
    
    WUIView           *uivNearbyList = (WUIView*)[self getChildWidget:@"IDVNearbyList"];
    WGoogleMapsCustom *wgmMapView    = (WGoogleMapsCustom*)[self getChildWidget:@"IDMGoogleMainMap"];
    
    if ([nsstrSenderID isEqualToString:@"IDNCPNearbyList"])
    {
        if (NO == curled)
        {
            if (YES == [[uivNearbyList getView] isHidden])
            {
                //< Flip the nearby list
                [UIView beginAnimations:nil context:nil];
                [UIView setAnimationDuration:1.0];
                [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:[uivNearbyList getView] cache:YES];
                [[uivNearbyList getView] setHidden:NO];
                [UIView commitAnimations];

                //< Flip the map view
                [UIView beginAnimations:nil context:nil];
                [UIView setAnimationDuration:1.0];
                [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:[wgmMapView getMKMapView] cache:YES];
                [[wgmMapView getMKMapView] setHidden:YES];
                [UIView commitAnimations];
                
                //< Update button text
                WUIButton *uibNearby = [m_cCore getWidget:@"IDBNearby"];
                
                NSUserDefaults *defaults  = [NSUserDefaults standardUserDefaults];
                NSArray        *languages = [defaults objectForKey:@"AppleLanguages"];
                
                NSString *nsstrSelectedLanguage   = [languages objectAtIndex:0];
                if ([nsstrSelectedLanguage isEqualToString:@"de"])
                {
                    [uibNearby setTitle:@"Karte"];
                }
                else
                {
                    [uibNearby setTitle:@"Map"];
                }
            }
            else
            {
                //< Flip the nearby list
                [UIView beginAnimations:nil context:nil];
                [UIView setAnimationDuration:1.0];
                [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:[uivNearbyList getView] cache:YES];
                [[uivNearbyList getView] setHidden:YES];
                [UIView commitAnimations];
                
                //< Flip the map view
                [UIView beginAnimations:nil context:nil];
                [UIView setAnimationDuration:1.0];
                [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:[wgmMapView getMKMapView] cache:YES];
                [[wgmMapView getMKMapView] setHidden:NO];
                [UIView commitAnimations];
                
                //< Update button text
                WUIButton *uibNearby = [m_cCore getWidget:@"IDBNearby"];
                
                NSUserDefaults *defaults  = [NSUserDefaults standardUserDefaults];
                NSArray        *languages = [defaults objectForKey:@"AppleLanguages"];
                
                NSString *nsstrSelectedLanguage   = [languages objectAtIndex:0];
                
                if ([nsstrSelectedLanguage isEqualToString:@"cy"])
                {
                    [uibNearby setTitle:@"Gerllaw"];
                }
                else
                {
                    if ([nsstrSelectedLanguage isEqualToString:@"de"])
                    {
                        [uibNearby setTitle:@"Liste"];
                    }
                    else
                    {
                        [uibNearby setTitle:@"Nearby"];
                    }
                }
            }
        }
        
        bHandled = YES;
    }
    
    if ([nsstrSenderID isEqualToString:@"IDFilterPOI"])
    {
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        {
            [self curlMapviewPage];
        }
        else
        {
            [self curlMapviewPageiOS6];
        }
        
        bHandled = YES;
    }
    
    if (NO == bHandled)
    {
        [super sendCallbackEvent:nsstrSenderID index:nsnIndex actionType:nsnActionType];
    }
}

/**
 * Curl the map page up or down
 *
 * @return none
 */
-(void)curlMapviewPage
{
    WUIView *uivFilterListView = (WUIView*)[m_cCore getWidget:@"IDVFilterList"];
    WUIView *uivNearbyList     = (WUIView*)[self getChildWidget:@"IDVNearbyList"];
    
    CATransition *animation = [CATransition animation];
    [animation setDelegate:self]; [animation setDuration:0.70];
    [animation setTimingFunction:UIViewAnimationCurveEaseInOut];
    if (NO == curled)
    {
        animation.type = @"pageCurl";
        animation.fillMode = kCAFillModeForwards;
        animation.endProgress = 0.80;
        
        [[uivFilterListView getView] setHidden:NO];
    }
    else
    {
        animation.type = @"pageUnCurl";
        animation.fillMode = kCAFillModeBackwards;
        animation.startProgress = 0.20;
        
        [[uivFilterListView getView] setHidden:YES];
        
        WGoogleMapsCustom *wgmcGoogleMap = (WGoogleMapsCustom*)[m_cCore getWidget:@"IDMGoogleMainMap"];
        [wgmcGoogleMap updatePOIDisplay]; 
        
        WUITableViewPOIList *wuitvNearbyList = (WUITableViewPOIList*)[m_cCore getWidget:@"IDTVNearbyList"];
        [wuitvNearbyList fillTableView];
    }
    [animation setRemovedOnCompletion:NO];
    if (YES == [[uivNearbyList getView] isHidden])
    {
        [[m_uivcController view] exchangeSubviewAtIndex:0 withSubviewAtIndex:2];
    }
    else
    {
#ifdef FOREST_TALES
        [[m_uivcController view] exchangeSubviewAtIndex:0 withSubviewAtIndex:4];
#else
        [[m_uivcController view] exchangeSubviewAtIndex:0 withSubviewAtIndex:5];
#endif
        //[[m_uivcController view] exchangeSubviewAtIndex:1 withSubviewAtIndex:2];
    }
    [[[m_uivcController view] layer] addAnimation:animation forKey:@"pageCurlAnimation"];
    curled = !curled;
}


/**
 * Curl the map page up or down
 *
 * @return none
 */
-(void)curlMapviewPageiOS6
{
    WUIView *uivFilterListView = (WUIView*)[m_cCore getWidget:@"IDVFilterList"];
    WUIView *uivNearbyList     = (WUIView*)[self getChildWidget:@"IDVNearbyList"];
    
    CATransition *animation = [CATransition animation];
    [animation setDelegate:self]; [animation setDuration:0.70];
    [animation setTimingFunction:UIViewAnimationCurveEaseInOut];
    if (NO == curled)
    {
        animation.type = @"pageCurl";
        animation.fillMode = kCAFillModeForwards;
        animation.endProgress = 0.80;
        
        [[uivFilterListView getView] setHidden:NO];
    }
    else
    {
        animation.type = @"pageUnCurl";
        animation.fillMode = kCAFillModeBackwards;
        animation.startProgress = 0.20;
        
        [[uivFilterListView getView] setHidden:YES];
        
        WGoogleMapsCustom *wgmcGoogleMap = (WGoogleMapsCustom*)[m_cCore getWidget:@"IDMGoogleMainMap"];
        [wgmcGoogleMap updatePOIDisplay];
        
        WUITableViewPOIList *wuitvNearbyList = (WUITableViewPOIList*)[m_cCore getWidget:@"IDTVNearbyList"];
        [wuitvNearbyList fillTableView];
    }
    [animation setRemovedOnCompletion:NO];
    if (YES == [[uivNearbyList getView] isHidden])
    {
        [[m_uivcController view] exchangeSubviewAtIndex:0 withSubviewAtIndex:1];
    }
    else
    {
        [[m_uivcController view] exchangeSubviewAtIndex:0 withSubviewAtIndex:2];
    }
    [[[m_uivcController view] layer] addAnimation:animation forKey:@"pageCurlAnimation"];
    curled = !curled;
}
@end
