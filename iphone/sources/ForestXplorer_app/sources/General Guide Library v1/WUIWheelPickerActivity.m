//
//  WUIWheelPickerActivity.m
//  General_app
//
//  Created by Adrian Hoitan on 24.01.12.
//  Copyright (c) 2012 getunik. All rights reserved.
//

#import "WUIWheelPickerActivity.h"
#import "WGoogleMapsCustom.h"

@implementation WUIWheelPickerActivity

/**
 * Initialize our widget with the informations that are coming from an XML node. The Node must be a valid macc node item.
 * 
 * @param cxmlNode XML node that contains all the data.
 * @param rmResourcesManager Pointer to an resource manager that contain real values. It can be null in some cases.
 * @param iIndex If this item comes from an group with multiple items, this will specify the index of the item inside the list.
 * @param ccCore Pointer to the widget and resource creator.
 * @return id of the created item.
 */
-(id)initWithXMLNode:(CXMLElement*)cxmlNode resourcesManager:(CResourceManager*)rmResourcesManager index:(int)iIndex core:(CCore*)ccCore
{
    //< Call the super class for all the initializations
    [super initWithXMLNode:cxmlNode resourcesManager:rmResourcesManager index:iIndex core:ccCore];
    
    return self;
}

/**
 * Parse the value list and store all the values
 *
 * @return none
 */
-(void)parsePickerValues
{
    //< Alloc and initialize the lists that will hold our values
    m_nsmaStringValues = [[NSMutableArray alloc] init];
    m_nsmaIntValues    = [[NSMutableArray alloc] init];
    
    //< Add all items picker selection
    NSUserDefaults *defaults  = [NSUserDefaults standardUserDefaults];
    NSArray        *languages = [defaults objectForKey:@"AppleLanguages"];
    
    NSString *nsstrSelectedLanguage   = [languages objectAtIndex:0];
    
    if ([nsstrSelectedLanguage isEqualToString:@"cy"])
    {
        [m_nsmaStringValues addObject:@"Holl weithgareddau"];
    }
    else
    {
        if ([nsstrSelectedLanguage isEqualToString:@"de"])
        {
            [m_nsmaStringValues addObject:@"Alle"];
        }
        else
        {
            [m_nsmaStringValues addObject:@"All Activities"];
        }
    }
    [m_nsmaIntValues addObject:[[NSString alloc] initWithString:@"-1"]];
    
    //< Get a list with all values
    int iActivities = [m_rmResourcesManager getIntAttributeValue:@"[@ActivitiesXMLList]" defaultValue:0 index:0];
    
    //< Loop all elements and store the values
    for (int iStep = 0; iStep < iActivities; iStep++)
    {
        NSString *nsaStringList = [m_rmResourcesManager getStrAttributeValue:@"[@ActivitiesXMLList]./title_of_the_activity" defaultValue:@"" index:iStep];
        NSString *nsaIntList    = [m_rmResourcesManager getStrAttributeValue:@"[@ActivitiesXMLList]./@id" defaultValue:@"" index:iStep];
        
        //< We should have only one value inside our list
        [m_nsmaStringValues addObject:nsaStringList];
        [m_nsmaIntValues addObject:nsaIntList];
    }
}

/**
 * Called by the picker view when the user selects a row in a component.
 *
 * @param thePickerView An object representing the picker view requesting the data.
 * @param row A zero-indexed number identifying a row of component. Rows are numbered top-to-bottom.
 * @param component A zero-indexed number identifying a component of pickerView. Components are numbered left-to-right.
 * @return none
 */
-(void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [super pickerView:thePickerView didSelectRow:row inComponent:component];
}

@end
