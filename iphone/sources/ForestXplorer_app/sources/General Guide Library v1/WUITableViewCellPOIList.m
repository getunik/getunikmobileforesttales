//
//  WUITableViewCellPOIList.m
//  General_app
//
//  Created by ahn on 11.10.11.
//  Copyright 2011 getunik. All rights reserved.
//

#import "WUITableViewCellPOIList.h"
#import "WUILabel.h"
#import "CCore.h"

@implementation WUITableViewCellPOIList

/**
 * Initialize our widget with the informations that are coming from an XML node. The Node must be a valid macc node item.
 * 
 * @param cxmlNode XML node that contains all the data.
 * @param rmResourcesManager Pointer to an resource manager that contain real values. It can be null in some cases.
 * @param iIndex If this item comes from an group with multiple items, this will specify the index of the item inside the list.
 * @param ccCore Pointer to the widget and resource creator.
 * @return id of the created item.
 */
-(id)initWithXMLNode:(CXMLElement*)cxmlNode resourcesManager:(CResourceManager*)rmResourcesManager index:(int)iIndex core:(CCore*)ccCore
{
    [super initWithXMLNode:cxmlNode resourcesManager:rmResourcesManager index:iIndex core:ccCore];
    
    //< Retreive the location of the POI
    float    fLatitude       = 0;
    float    fLongitude      = 0;
    NSString *nsstrLatitude  = [rmResourcesManager getStrAttributeValue:@"[@DataXMLList]./geolocation_lat" defaultValue:[NSString stringWithString:@"0"] index:iIndex];
    NSString *nsstrLongitude = [rmResourcesManager getStrAttributeValue:@"[@DataXMLList]./geolocation_long" defaultValue:[NSString stringWithString:@"0"] index:iIndex];
    
    fLatitude  = [nsstrLatitude floatValue];
    fLongitude = [nsstrLongitude floatValue];

    if (iIndex % 2 == 1)
    {
        m_uicBackgroundColor = [[UIColor alloc] initWithRed:0.94 green:0.93 blue:0.92 alpha:1.0];
    }
    //< Cleanup
    [nsstrLatitude release];
    [nsstrLongitude release];
    
    //< Store the location
    m_cclPOILocation = [[CLLocation alloc] initWithLatitude:fLatitude longitude:fLongitude];
    
    m_nsmaActivities = [[NSMutableArray alloc] init];
    
    //< Read and store all the activities that this POI can have
    int iActivities = [m_rmResourcesManager getIntAttributeValue:@"[@DataXMLList]./activity" defaultValue:0 index:iIndex];
    for (int iStep = 0; iStep < iActivities; iStep++)
    {
        NSString *nsstrActivityID = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLList]./activity[%d]", iStep + 1] defaultValue:@"" index:iIndex];
        [m_nsmaActivities addObject:nsstrActivityID];
    }
    
    return self;
}

/**
 * Check if a specific activity is listed by this POI
 *
 * @param iActivityID Id of the activity that the user has selected
 * @return int category value
 */
-(BOOL)isActivityListed:(int)iActivityID
{
    BOOL bReturnVal = NO;
    
    for (int iStep = 0; iStep < [m_nsmaActivities count]; iStep++)
    {
        if ([[m_nsmaActivities objectAtIndex:iStep] intValue] == iActivityID)
        {
            bReturnVal = YES;
        }
    }
    
    //< The -1 value represents the all activities
    if (-1 == iActivityID)
    {
        bReturnVal = YES;
    }
    
    return bReturnVal;
}

/**
 *
 */
-(void)refreshViewWithNewLocation:(CLLocation*)currentLocation
{
    NSString *distanceString = nil;
    
    [self loadContent];
    
    if (nil != currentLocation)
    {
        CLLocationDistance ccDistance;
        ccDistance = [m_cclPOILocation getDistanceFrom:currentLocation];
            
        m_fDistance = ccDistance;
        
        NSUserDefaults   *nsudPreferred = [NSUserDefaults standardUserDefaults];
        NSString         *nsstrMetrics  = [nsudPreferred objectForKey:@"CWWFFCWUnits"];
        
        if ([nsstrMetrics isEqualToString:@"mi"])
        {
            distanceString = [NSString stringWithFormat:@"%.2f mi", m_fDistance / 1000 / 1.61];
        }
        else
        {
            distanceString = [NSString stringWithFormat:@"%.2f km", m_fDistance / 1000];
        }
    }
    else
    {
        distanceString = [NSString stringWithString:@" "];
    }
    
    WUILabel *uilDistanceText = [self getChildWidget:@"IDLDistance"];
    [uilDistanceText setText:distanceString];
}

/**
 *
 */
-(NSNumber *)getCompareNumber
{
    return [NSNumber numberWithFloat: m_fDistance];
}

/**
 * Dealloc and release all allocated memory
 * 
 * @return none
 **/
-(void)dealloc
{
    SAFE_RELEASE(m_cclPOILocation);
    
    //< Call the superclass for deallocations
    [super dealloc];
}
@end
