//
//  WUITableViewAccessPoints.m
//  General_app
//
//  Created by ahn on 14.10.11.
//  Copyright 2011 getunik. All rights reserved.
//

#import "WUITableViewAccessPoints.h"
#import "UAnalyticsTracking.h"

@implementation WUITableViewAccessPoints

/**
 * Send an callback event to our parent widgets informing about the new action.
 *
 * @param nsstrSenderID Sender ID
 * @param nsnIndex Index number of the widget that is calling the callback
 * @param nsnActionType Specific action that was triggered
 * @return None
 **/
-(void)sendCallbackEvent:(NSString *)nsstrSenderID index:(NSNumber*)nsnIndex actionType:(NSNumber*)nsnActionType
{
    if ([nsstrSenderID isEqualToString:@"IDNCPAccessPoint"])
    {
        RNumber *rnSelectedAccessPoint = (RNumber*)[m_rmResourcesManager getResource:@"[@RNAccessPoint]"];
        [rnSelectedAccessPoint setValue:(int)[nsnIndex intValue] + 1];
        
        //< Check if we have to send a tracking message
        NSString *strTitle = [m_rmResourcesManager getStrAttributeValue:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/title_of_the_poi" defaultValue:@"" index:0];
        NSString *strTitleAP = [m_rmResourcesManager getStrAttributeValue:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/access_points/access_point[[@RNAccessPoint]]/title_of_the_ap" defaultValue:@"" index:0];
        
        NSString *strName = [NSString stringWithFormat:@"/iPhone/%@/%@/%@", [m_rmResourcesManager getSelectedLanguage], strTitle, strTitleAP];
#ifdef INCLUDE_GOOGLE_ANALYTICS
        [[UAnalyticsTracking sharedUAnalyticsHelper] trackGoogleAnalyticsPage:strName];
#endif
    }
    
    [super sendCallbackEvent:nsstrSenderID index:nsnIndex actionType:nsnActionType];
}

@end