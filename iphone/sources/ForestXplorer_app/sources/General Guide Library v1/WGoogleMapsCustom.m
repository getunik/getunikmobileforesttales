//
//  WGoogleMapsCustom.m
//  General_app
//
//  Created by ahn on 07.10.11.
//  Copyright 2011 getunik. All rights reserved.
//

#import "WGoogleMapsCustom.h"
#import "CCore.h"
#import "WGoogleMapsPOICustom.h"
#import "WUIWheelPickerActivity.h"
#import <QuartzCore/CAAnimation.h>
#import "UAnalyticsTracking.h"

@implementation WGoogleMapsCustom

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views { 
    MKAnnotationView *aV; 
    float fDelay = 0;
    for (aV in views) {
        CGRect endFrame = aV.frame;

        aV.frame = CGRectMake(aV.frame.origin.x, aV.frame.origin.y - 300.0, aV.frame.size.width, aV.frame.size.height);

        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.45];
        [UIView setAnimationDelay:fDelay];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [aV setFrame:endFrame];
        [UIView commitAnimations];
        fDelay+=0.05;

    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{    
    MKPinAnnotationView  *mkaPOIView  = nil;

    if ([annotation isKindOfClass:[WGoogleMapsPOICustom class]])
    {
        WGoogleMapsPOICustom *wgmPOIAnnotation = annotation;
        mkaPOIView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"parkingloc"];
        [wgmPOIAnnotation setAnnotationView:mkaPOIView];

        UIButton *disclosureButton = [UIButton buttonWithType: UIButtonTypeDetailDisclosure];
        [disclosureButton addTarget:self action:@selector(POIButtonCallback:) forControlEvents:UIControlEventTouchUpInside];

        disclosureButton.tag = wgmPOIAnnotation.m_iButtonTag;
        mkaPOIView.rightCalloutAccessoryView = disclosureButton;
        mkaPOIView.canShowCallout = YES;

        if (1 == [wgmPOIAnnotation getFarmColor])
        {
            UIImage * image = [UIImage imageNamed:@"red_map_pin.png"];
            UIImageView *imageView = [[[UIImageView alloc] initWithImage:image] autorelease];
            [mkaPOIView addSubview:imageView];
        }
        else
        {
            if (2 == [wgmPOIAnnotation getFarmColor])
            {
                UIImage * image = [UIImage imageNamed:@"blue_map_pin.png"];
                UIImageView *imageView = [[[UIImageView alloc] initWithImage:image] autorelease];
                [mkaPOIView addSubview:imageView];
            }
            else
            {
                UIImage * image = [UIImage imageNamed:@"green_map_pin.png"];
                UIImageView *imageView = [[[UIImageView alloc] initWithImage:image] autorelease];
                [mkaPOIView addSubview:imageView];
            }
        }
    }

	return mkaPOIView;
}

/**
 * Update the displayed points on the map based on the selected activity
 *
 * @return none
 */
-(void)updatePOIDisplay
{
    WGoogleMapsPOICustom *wgmCutomPOI = nil;

    //< Get the selected activity ID
    WUIWheelPickerActivity *uiwpSelectedActivity = (WUIWheelPickerActivity*)[m_cCore getWidget:@"IDWPActivities"];
    int iSelectedActivity = [uiwpSelectedActivity getSelectedInt];

    int iPOICount = [m_nsmaPOIList count];
    for (int iStep = 0; iStep < iPOICount; iStep++)
    {
        wgmCutomPOI = [m_nsmaPOIList objectAtIndex:iStep];

        MKPinAnnotationView *mkPOIAnnotation = [wgmCutomPOI getAnnotationView];
        if (1 == [wgmCutomPOI getFarmColor])
        {
            UIImage * image = [UIImage imageNamed:@"red_map_pin.png"];
            UIImageView *imageView = [[[UIImageView alloc] initWithImage:image] autorelease];
            [mkPOIAnnotation addSubview:imageView];
        }
        else
        {
            if (2 == [wgmCutomPOI getFarmColor])
            {
                UIImage * image = [UIImage imageNamed:@"blue_map_pin.png"];
                UIImageView *imageView = [[[UIImageView alloc] initWithImage:image] autorelease];
                [mkPOIAnnotation addSubview:imageView];
            }
            else
            {
                UIImage * image = [UIImage imageNamed:@"green_map_pin.png"];
                UIImageView *imageView = [[[UIImageView alloc] initWithImage:image] autorelease];
                [mkPOIAnnotation addSubview:imageView];
            }
        }
        [mkPOIAnnotation setHidden:![wgmCutomPOI isActivityListed:iSelectedActivity]];
    }
}

/**
 * Send an callback event to our parent widgets informing about the new action.
 *
 * @param nsstrSenderID Sender ID
 * @param nsnIndex Index number of the widget that is calling the callback
 * @param nsnActionType Specific action that was triggered
 * @return None
 **/
-(void)sendCallbackEvent:(NSString *)nsstrSenderID index:(NSNumber*)nsnIndex actionType:(NSNumber*)nsnActionType
{
    //< Check if we have to send a tracking message
    NSString *strName = [NSString stringWithFormat:@"/iPhone/%@/%@", [m_rmResourcesManager getSelectedLanguage], [m_rmResourcesManager getStrAttributeValue:@"[@DataXMLList]./title_of_the_poi" defaultValue:@"" index:[nsnIndex intValue]]];
#ifdef INCLUDE_GOOGLE_ANALYTICS
    [[UAnalyticsTracking sharedUAnalyticsHelper] trackGoogleAnalyticsPage:strName];
#endif
    [super sendCallbackEvent:nsstrSenderID index:nsnIndex actionType:nsnActionType];
}

@end
