//
//  WGoogleMapsPOICustom.h
//  General_app
//
//  Created by ahn on 10.10.11.
//  Copyright 2011 getunik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WGoogleMapsPOI.h"

@interface WGoogleMapsPOICustom : WGoogleMapsPOI
{
    NSMutableArray *m_nsmaActivities;   //< Keep track of the activities of this POI
    int m_iFarmColor;
}

/**
 * Check if a specific activity is listed by this POI
 *
 * @param iActivityID Id of the activity that the user has selected
 * @return int category value
 */
-(BOOL)isActivityListed:(int)iActivityID;

/**
 * Is the windfar under construction or not.
 *
 * @return true if windfarm is underconstruction
 */
-(BOOL)isWindfarmConstruction;

@end