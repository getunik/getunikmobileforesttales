//
//  WUIImageAppIcon.h
//  ForestXplorer_app
//
//  Created by Adrian Hoitan on 22/09/14.
//  Copyright (c) 2014 Adrian Hoitan. All rights reserved.
//

#import "WUIButton.h"

@interface WUIImageAppIcon : WUIButton

@end
