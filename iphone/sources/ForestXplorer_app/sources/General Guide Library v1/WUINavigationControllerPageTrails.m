//
//  WUINavigationControllerPageTrails.m
//  General_app
//
//  Created by Adrian Hoitan on 30.01.12.
//  Copyright (c) 2012 getunik. All rights reserved.
//

#import "WUINavigationControllerPageTrails.h"

@implementation WUINavigationControllerPageTrails

/**
 * Send an callback event to our parent widgets informing about the new action.
 *
 * @param nsstrSenderID Sender ID
 * @param nsnIndex Index number of the widget that is calling the callback
 * @param nsnActionType Specific action that was triggered
 * @return None
 **/
-(void)sendCallbackEvent:(NSString *)nsstrSenderID index:(NSNumber*)nsnIndex actionType:(NSNumber*)nsnActionType
{
    BOOL bHandled = NO;
    
    if ([nsstrSenderID isEqualToString:@"IDNCPTrailMapPDF"])
    {        
        NSString *nsstrTrailMapPath = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/access_points/access_point[[@RNAccessPoint]]/trails/trail[%d]/map_of_trail", [nsnIndex intValue] + 1] defaultValue:@"" index:0];
        RString  *rstrTrailPath     = (RString*)[m_rmResourcesManager getResource:@"[@IDSTrailMapPath]"];
        [rstrTrailPath setValue:nsstrTrailMapPath];
        
        //< Cleanup 
        [nsstrTrailMapPath release];
        
        //< Send the callback action
        [super sendCallbackEvent:@"IDNCPTrailMapPDF" index:nsnIndex actionType:nsnActionType];
        
        bHandled = YES;
    }
    
    if ([nsstrSenderID isEqualToString:@"IDNCPTrailAerial"])
    {        
        NSString *nsstrTrailMapPath = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/access_points/access_point[[@RNAccessPoint]]/trails/trail[%d]/ap_of_trail", [nsnIndex intValue] + 1] defaultValue:@"" index:0];
        RString  *rstrTrailPath     = (RString*)[m_rmResourcesManager getResource:@"[@IDSTrailMapPath]"];
        [rstrTrailPath setValue:nsstrTrailMapPath];
        
        //< Cleanup 
        [nsstrTrailMapPath release];
        
        //< Send the callback action
        [super sendCallbackEvent:@"IDNCPTrailAerial" index:nsnIndex actionType:nsnActionType];
        
        bHandled = YES;
    }

    if (NO == bHandled)
    {
        [super sendCallbackEvent:nsstrSenderID index:nsnIndex actionType:nsnActionType];
    }
}

@end
