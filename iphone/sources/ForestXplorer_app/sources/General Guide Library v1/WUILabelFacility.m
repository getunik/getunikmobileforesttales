//
//  WUILabelFacility.m
//  General_app
//
//  Created by Adrian Hoitan on 27.01.12.
//  Copyright (c) 2012 getunik. All rights reserved.
//

#import "WUILabelFacility.h"
#import "CCore.h"

@implementation WUILabelFacility

/**
 * Load the property called text.
 *
 * @param cxmlAttributeNode XML node that contains the data for this attribute.
 * @return none
 * @note This function can be overwritten if we want to load a different data
 */
-(void)loadAttributeText:(CXMLNode*)cxmlAttributeNode
{
    NSString *nsstrReturn     = @"";
    NSString *nsstrFacilityID = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/facility[%d]", m_iWidgetIndex + 1] defaultValue:@"" index:0];
    
    int iFacilities = [m_rmResourcesManager getIntAttributeValue:@"[@FacilitiesXMLList]./facility" defaultValue:0 index:0];
    
    for (int iStep = 0; iStep < iFacilities; iStep++)
    {
        NSString *nsstrXMLFacilityID = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@FacilitiesXMLList]./facility[%d]/@id", iStep + 1] defaultValue:@"" index:0];
        if ([nsstrXMLFacilityID isEqualToString:nsstrFacilityID])
        {
            nsstrReturn = [NSString stringWithFormat:@"[@FacilitiesXMLList]./facility[%d]/title_of_the_facility", iStep + 1];
        }
        
        //< Cleanup 
        SAFE_RELEASE(nsstrXMLFacilityID);
    }
    
    //< Cleanup 
    SAFE_RELEASE(nsstrFacilityID);
    
    NSString *nsstrFormat = nil;
    NSString *nsstrText   = nil;
    
    //< Set the displayed text
    if (nil != cxmlAttributeNode)
    {
        nsstrText = [m_rmResourcesManager getStrAttributeValue:nsstrReturn defaultValue:[NSString stringWithString:@""] index:m_iWidgetIndex];
        
        //< Check if the text has an formatting attribute and set the UILabel property
        cxmlAttributeNode = [m_rmResourcesManager getXMLNodeForAttribute:@"formatting" fromElementNode:m_cxmlNode];
        if (nil != cxmlAttributeNode)
        {
            nsstrFormat = [m_rmResourcesManager getStrAttributeValue:[cxmlAttributeNode stringValue] defaultValue:[NSString stringWithString:@""] index:m_iWidgetIndex];
        }
        
        if (nil != nsstrFormat)
        {
            [m_uilLabel setText:[NSString stringWithFormat:nsstrFormat, nsstrText]];
        }
        else
        {
            [m_uilLabel setText:nsstrText];
        }
        
        [nsstrFormat release];
        [nsstrText release];
    }
}

@end
