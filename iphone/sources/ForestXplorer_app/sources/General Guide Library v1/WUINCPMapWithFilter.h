//
//  WUINCPMapWithFilter.h
//  General_app
//
//  Created by ahn on 06.10.11.
//  Copyright 2011 getunik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WUINavigationControllerPage.h"

@interface WUINCPMapWithFilter : WUINavigationControllerPage
{
    BOOL curled;
}

/**
 * Curl the map page up or down
 *
 * @return none
 */
-(void)curlMapviewPage;

@end
