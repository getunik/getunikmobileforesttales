//
//  WUINavigationControllerPageAbout.h
//  General_app
//
//  Created by Adrian Hoitan on 16.02.12.
//  Copyright (c) 2012 getunik. All rights reserved.
//

#import "WUINavigationControllerPage.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMessageComposeViewController.h>

@interface WUINavigationControllerPageAbout : WUINavigationControllerPage <MFMailComposeViewControllerDelegate>
{
    int m_iEnableDebugging;
}


@end
