//
//  WUIImageFacility.m
//  General_app
//
//  Created by Adrian Hoitan on 27.01.12.
//  Copyright (c) 2012 getunik. All rights reserved.
//

#import "WUIImageFacility.h"
#import "CCore.h"

@implementation WUIImageFacility

/**
 * Load the property called text.
 *
 * @param cxmlAttributeNode XML node that contains the data for this attribute.
 * @return none
 * @note This function can be overwritten if we want to load a different data
 */
-(NSString*)loadAttributeResource:(CXMLNode*)cxmlAttributeNode
{
    NSString *nsstrReturn     = @"";
    NSString *nsstrFacilityID = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/facility[%d]", m_iWidgetIndex + 1] defaultValue:@"" index:0];
    
    int iFacilities = [m_rmResourcesManager getIntAttributeValue:@"[@FacilitiesXMLList]./facility" defaultValue:0 index:0];
    
    for (int iStep = 0; iStep < iFacilities; iStep++)
    {
        NSString *nsstrXMLFacilityID = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@FacilitiesXMLList]./facility[%d]/@id", iStep + 1] defaultValue:@"" index:0];
        if ([nsstrXMLFacilityID isEqualToString:nsstrFacilityID])
        {
            nsstrReturn = [NSString stringWithFormat:@"[@FacilitiesXMLList]./facility[%d]/picture_of_facility", iStep + 1];
        }
        
        //< Cleanup 
        SAFE_RELEASE(nsstrXMLFacilityID);
    }
    
    //< Cleanup 
    SAFE_RELEASE(nsstrFacilityID);
    
    return nsstrReturn;
}

@end
