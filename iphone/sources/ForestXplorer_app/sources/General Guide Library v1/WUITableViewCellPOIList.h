//
//  WUITableViewCellPOIList.h
//  General_app
//
//  Created by ahn on 11.10.11.
//  Copyright 2011 getunik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WUITableViewCell.h"
#import <MapKit/MapKit.h>

@interface WUITableViewCellPOIList : WUITableViewCell
{
    CLLocation *m_cclPOILocation;
    float      m_fDistance;
    
    NSMutableArray *m_nsmaActivities;   //< Keep track of the activities of this POI
}

@end
