//
//  WUIWebViewPOIDetails.m
//  General_app
//
//  Created by Adrian Hoitan on 17.01.12.
//  Copyright (c) 2012 getunik. All rights reserved.
//

#import "WUIWebViewPOIDetails.h"
#import "WUITableViewCell.h"
#import "CCore.h"

@implementation WUIWebViewPOIDetails

/**
 * Load the property called text.
 *
 * @param cxmlAttributeNode XML node that contains the data for this attribute.
 * @return none
 * @note This function can be overwritten if we want to load a different data
 */
-(void)loadAttributeText:(CXMLNode*)cxmlAttributeNode
{
    if (nil != cxmlAttributeNode)
    {
        NSString *nsstrText = [m_rmResourcesManager getStrAttributeValue:[cxmlAttributeNode stringValue] defaultValue:@"" index:m_iWidgetIndex];
        NSString *path      = [[NSBundle mainBundle] bundlePath];
        NSURL    *baseURL   = [NSURL fileURLWithPath:path];
        
        NSString *nsstrHTML = [[NSString alloc] initWithFormat:@"<html><head><style type=\"text/css\"> body {font-family:Helvetica; font-size:14;}</style></head><body>%@</body>", nsstrText];
        
        [m_uiwvWebView loadHTMLString:nsstrHTML baseURL:baseURL];
        
        m_bLoadLinksInSafari = true;
        
        //< Cleanup
        [nsstrHTML release];
        [nsstrText release];
    }
}

/**
 * Sent after a web view finishes loading a frame.
 *
 * @webView The web view has finished loading.
 * @return none
 */
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    //< Recalculate the size of the webview based on the content
	CGFloat contentHeight = [[webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.scrollHeight"] floatValue];
	webView.frame = CGRectMake(webView.frame.origin.x, webView.frame.origin.y, webView.frame.size.width, contentHeight);
    
    //< Resize the cell height
    WUITableViewCell *wuitvcWebDetails = (WUITableViewCell*)[m_cCore getWidget:@"IDTVCWebDetails"];
    [wuitvcWebDetails setCellHeight:(int)contentHeight];

    //< Reload our table
    WUITableView *wuitvDetailsPOI = (WUITableView*)[m_cCore getWidget:@"IDTVDetailPOI"];
    [wuitvDetailsPOI reloadData];
}

@end
