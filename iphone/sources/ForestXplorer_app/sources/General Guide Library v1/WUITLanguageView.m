//
//  WUITLanguageView.m
//  General_app
//
//  Created by Adrian Hoitan on 31.01.12.
//  Copyright (c) 2012 getunik. All rights reserved.
//

#import "WUITLanguageView.h"
#import "ULanguageHelper.h"
@implementation WUITLanguageView

///
///
///
-(id)initWithXMLNode:(CXMLElement*)cxmlNode resourcesManager:(CResourceManager*)rmResourcesManager index:(int)iIndex core:(CCore*)ccCore parentWidget:(IWidget *)iwParentWidget
{
    [super initWithXMLNode:cxmlNode resourcesManager:rmResourcesManager index:iIndex core:ccCore parentWidget:iwParentWidget];
    
    [self setLanguage];
    return self;
}

///
/// Event recived when a specific element was selected
/// \param tableView Table view object
/// \param indexPath Element index
/// \return none
///
- (void)tableView: (UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    WUITableViewCell    *cell         = nil;
    WUITableViewSection *uitvsSection = [m_nsmaTableSections objectAtIndex:indexPath.section];
    
    if (nil != uitvsSection)
    {
        for (int iStep = 0; iStep < 2; iStep++)
        {
            cell = [uitvsSection getCellAtIndex:iStep];
            if (iStep == indexPath.row)
            {
                [cell setAccessory:3];
            }
            else
            {
                [cell setAccessory:0];
            }
        }
    }
    
    switch (indexPath.row)
    {
        case 0:
            [[ULanguageHelper sharedULanguageHelper] setApplicationLanguage:@"en" infoTitle:@"Information" infoText:@"Please restart the application in order for the changes to take place." okButton:@"OK"];
        break;
        case 1:
#ifdef APPMAKER_DEMO
            [[ULanguageHelper sharedULanguageHelper] setApplicationLanguage:@"de" infoTitle:@"Information" infoText:@"Please restart the application in order for the changes to take place." okButton:@"OK"];
#else
            [[ULanguageHelper sharedULanguageHelper] setApplicationLanguage:@"cy" infoTitle:@"Information" infoText:@"Please restart the application in order for the changes to take place." okButton:@"OK"];
#endif
        break;
            
        default:
            break;
    }

    [super tableView:tableView didSelectRowAtIndexPath:indexPath];

    [self reloadData];
}

///
///
///
-(void)setLanguage
{
    WUITableViewCell    *cell         = nil;
    NSString *documentsDirectory = nil;
    NSError  *error              = nil;
    WUITableViewSection *uitvsSection = [m_nsmaTableSections objectAtIndex:0];
    
    //< Check if we have precached data
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    documentsDirectory = [paths objectAtIndex:0];
    documentsDirectory = [NSString stringWithFormat:@"%@/language.dat", documentsDirectory];
    
    NSString *strLanguage = [[ULanguageHelper sharedULanguageHelper] getApplicationLanguage];
    
    if (nil != uitvsSection)
    {
        cell = [uitvsSection getCellAtIndex:0];
        [cell setAccessory: ([strLanguage isEqual:@"en"] ? 3 : 0)];
        
#ifdef APPMAKER_DEMO
        cell = [uitvsSection getCellAtIndex:1];
        [cell setAccessory: ([strLanguage isEqual:@"de"] ? 3 : 0)];
#else
        cell = [uitvsSection getCellAtIndex:1];
        [cell setAccessory: ([strLanguage isEqual:@"cy"] ? 3 : 0)];
#endif
    }
}

@end
