//
//  Guide_interface_loader.m
//  General_app
//
//  Created by ahn on 06.10.11.
//  Copyright 2011 getunik. All rights reserved.
//

#import "Guide_interface_loader.h"
#import "WUINavigationController.h"
#import "WUIGuideDataDownloadingView.h"
#import "WUITableView.h"
#import "WUILabel.h"
#import "WUILastUpdateLabel.h"
#import "WUIButtonUpdating.h"
#import "UAnalyticsTracking.h"

@implementation Guide_interface_loader

/**
 * Load the content of this app
 *
 * @return none
 */
-(void)loadContent
{
    //< Set Google Analytics Tracking Shared Global Resource
#ifdef APPMAKER_DEMO
    [[UAnalyticsTracking sharedUAnalyticsHelper] initializeGoogleAnalyticsTrackingWithUACode:MACC_GOOGLE_UA];
#endif
    
    //< Load the default content, or the cached one
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        m_cmContentManager = [[CContentManager alloc] init:@"main.xml"];
    }
    else
    {
        m_cmContentManager = [[CContentManager alloc] init:@"main_ios6.xml"];
    }
    m_cCoreEngine      = [m_cmContentManager loadContent];
    
    //< Load the view
    m_uivMainView      = [m_cCoreEngine getComponent:@"//*[@id='IDTCMainTabController']"];
}

/**
 * Reload all the content
 *
 * @return none
 */
-(void)reloadContent
{
    [[m_uivMainView getView] removeFromSuperview];
    [m_cCoreEngine releaseWidget:m_uivMainView];

    m_cCoreEngine = [m_cmContentManager loadContent];
    m_uivMainView = [m_cCoreEngine getComponent:@"//*[@id='IDTCMainTabController']"];
}

/**
 * Show the updating screen
 */
-(void)showUpdatingScreen
{
    //< Get the main navigation controller
    WUINavigationController *uincNavicationController = (WUINavigationController*)[m_cCoreEngine getWidget:@"IDNCSettingsNavigation"];
    
    //< Jump to root controller to handle any exceptions
    [uincNavicationController popToRootController];
    [uincNavicationController subControllerCallback:@"IDCPUpdates" index:nil actionType:nil];
}

/**
 *
 */
-(void)startDownloading
{
    //< First display all the downloading cells
    WUITableView     *uitvUpdatesList = (WUITableView*)[m_cCoreEngine getWidget:@"IDTVUpdates"];
    WUITableViewCell *uitvcGuideData  = (WUITableViewCell*)[uitvUpdatesList getChildWidget:@"IDTVCGuideData"];
    
    //< Make sure that the content is loaded
    [uitvcGuideData loadContent];
    
    //< Display the cell view
    [[uitvcGuideData getUITableCell] setHidden:NO];

    //< Set the downloading button text. But make sure the content is loaded
    WUIButtonUpdating *uibUpdating = (WUIButtonUpdating*)[m_cCoreEngine getWidget:@"IDBStartUpdating"];
    [uibUpdating toogleVerifyState:false];

    //< Reset the progress bars
    WUIGuideDataDownloadingView *uivGuideData = (WUIGuideDataDownloadingView*)[uitvcGuideData getChildWidget:@"IDVGuideData"];
    [uivGuideData resetProgressBar];
    
    //< Start the first downloading set of data
    [uivGuideData startUpdating];
}

/**
 *
 */
-(void)endDownloading
{
    //< First display all the downloading cells
    WUITableView     *uitvUpdatesList = (WUITableView*)[m_cCoreEngine getWidget:@"IDTVUpdates"];
    WUITableViewCell *uitvcGuideData  = (WUITableViewCell*)[uitvUpdatesList getChildWidget:@"IDTVCGuideData"];

    [[uitvcGuideData getUITableCell] setHidden:YES];
    
    //< Set the downloading button text. But make sure the content is loaded
    WUIButtonUpdating *uibUpdating = (WUIButtonUpdating*)[m_cCoreEngine getWidget:@"IDBStartUpdating"];
    [uibUpdating toogleVerifyState:true];
    
    //< Update the last update displayed text
    WUILastUpdateLabel *uilLabel = (WUILastUpdateLabel*)[m_cCoreEngine getWidget:@"IDLLastUpdate"];
    [uilLabel updateDisplayedText];
}

/**
 * No new updates are available.
 *
 * @return none
 */
-(void)updateNoDownloadsAvailabile
{
    WUILabel *uilText = (WUILabel*)[m_cCoreEngine getWidget:@"IDLNoMoreUpdates"];
    [[uilText getLabel] setHidden:NO];
}

/**
 *
 */
-(WUITabController*)getView
{
    return m_uivMainView;
}

@end
