//
//  WUIWheelPickerActivity.h
//  General_app
//
//  Created by Adrian Hoitan on 24.01.12.
//  Copyright (c) 2012 getunik. All rights reserved.
//

#import "WUINavigationControllerPage.h"
#import "WUIWheelPicker.h"

@interface WUIWheelPickerActivity : WUIWheelPicker

@end
