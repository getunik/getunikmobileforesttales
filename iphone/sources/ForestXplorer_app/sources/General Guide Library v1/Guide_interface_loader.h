//
//  Guide_interface_loader.h
//  General_app
//
//  Created by ahn on 06.10.11.
//  Copyright 2011 getunik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CContentManager.h"
#import "CCore.h"
#import "CHTTPRequestSettings.h"
#import "WUIView.h"
#import "WUITabController.h"

@interface Guide_interface_loader : NSObject
{
#ifndef APPMAKER_DEMO
    #define MACC_GOOGLE_UA     @"UA-15164922-3"
#else
    #define MACC_GOOGLE_UA     @""
#endif
    
    //< coreLibrary Variables
    CHTTPRequestSettings       *m_tsSettings;
    CContentManager            *m_cmContentManager;
    CCore                      *m_cCoreEngine;
    
    WUITabController           *m_uivMainView;
}

@end
