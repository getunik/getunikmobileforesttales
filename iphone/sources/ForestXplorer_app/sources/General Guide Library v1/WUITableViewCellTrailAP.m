//
//  WUITableViewCellTrailAP.m
//  General_app
//
//  Created by Adrian Hoitan on 07.02.12.
//  Copyright (c) 2012 getunik. All rights reserved.
//

#import "WUITableViewCellTrailAP.h"

@implementation WUITableViewCellTrailAP

/**
 * Initialize our widget with the informations that are coming from an XML node. The Node must be a valid macc node item.
 * 
 * @param cxmlNode XML node that contains all the data.
 * @param rmResourcesManager Pointer to an resource manager that contain real values. It can be null in some cases.
 * @param iIndex If this item comes from an group with multiple items, this will specify the index of the item inside the list.
 * @param ccCore Pointer to the widget and resource creator.
 * @return id of the created item.
 */
-(id)initWithXMLNode:(CXMLElement*)cxmlNode resourcesManager:(CResourceManager*)rmResourcesManager index:(int)iIndex core:(CCore*)ccCore
{
    [super initWithXMLNode:cxmlNode resourcesManager:rmResourcesManager index:iIndex core:ccCore];
    
    NSString *nsstrTrailMapPath = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/access_points/access_point[[@RNAccessPoint]]/trails/trail[%d]/ap_of_trail", iIndex + 1] defaultValue:@"" index:0];
    
    if ([nsstrTrailMapPath length] == 0)
    {
        m_iCellHeight = 0;
        [m_uitvcTableCell setHidden:YES];
    }
    
    [nsstrTrailMapPath release];
    
    return self;
}

@end
