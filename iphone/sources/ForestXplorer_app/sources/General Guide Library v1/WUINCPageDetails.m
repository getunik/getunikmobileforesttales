//
//  WUINCPageDetails.m
//  General_app
//
//  Created by Adrian Hoitan on 23.01.12.
//  Copyright (c) 2012 getunik. All rights reserved.
//

#import "WUINCPageDetails.h"
#import "CCore.h"
#import "WUIButton.h"
#import "WUITableViewCell.h"

@implementation WUINCPageDetails

/**
 * Called when the content is loading
 *
 * @param iIndex Widget index
 * @return none
 */
-(void)loadContent:(int)iIndex
{
    //< Retreive the number object associated with the selected POI and set the selected number
    RNumber *rnSelectedPOI = (RNumber*)[m_rmResourcesManager getResource:@"[@RNSelectedPOI]"];
    [rnSelectedPOI setValue:iIndex];
    
    RNumber *rnSelectedIndex = (RNumber*)[m_rmResourcesManager getResource:@"[@RNPOIIndex]"];
    [rnSelectedIndex setValue:(iIndex + 1)];
    
    //< Superclass call
    [super loadContent:iIndex];
}

/**
 * Send an callback event to our parent widgets informing about the new action.
 *
 * @param nsstrSenderID Sender ID
 * @param nsnIndex Index number of the widget that is calling the callback
 * @param nsnActionType Specific action that was triggered
 * @return None
 **/
-(void)sendCallbackEvent:(NSString *)nsstrSenderID index:(NSNumber*)nsnIndex actionType:(NSNumber*)nsnActionType
{
    bool bHandled = false;
    
    if (false == bHandled)
    {
        [super sendCallbackEvent:nsstrSenderID index:nsnIndex actionType:nsnActionType];
    }
}

@end
