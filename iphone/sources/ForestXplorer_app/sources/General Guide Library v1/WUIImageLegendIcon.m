//
//  WUIImageLegendIcon.m
//  ForestXplorer_app
//
//  Created by Adrian Hoitan on 22/09/14.
//  Copyright (c) 2014 Adrian Hoitan. All rights reserved.
//

#import "WUIImageLegendIcon.h"

@implementation WUIImageLegendIcon

/**
 * Load the property called text.
 *
 * @param cxmlAttributeNode XML node that contains the data for this attribute.
 * @return none
 * @note This function can be overwritten if we want to load a different data
 */
-(NSString*)loadAttributeResource:(CXMLNode*)cxmlAttributeNode
{
    //< Check if the POI is under windfarm construction
    NSString *nsstrItem = @"[@IDILegendGreen]";
    NSString *nsstrFarmColor = [m_rmResourcesManager getStrAttributeValue:@"[@DataXMLList]./map_dot_color" defaultValue:@"" index:m_iWidgetIndex];
    
    if ((nil != nsstrFarmColor) && ([nsstrFarmColor length] > 0))
    {
        if ([nsstrFarmColor isEqualToString:@"red"])
        {
            nsstrItem = @"[@IDILegendRed]";
        }
        if ([nsstrFarmColor isEqualToString:@"blue"])
        {
            nsstrItem = @"[@IDILegendBlue]";
        }
    }
    
    return nsstrItem;
}

@end
