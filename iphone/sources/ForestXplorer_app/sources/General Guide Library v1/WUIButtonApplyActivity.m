//
//  WUIButtonApplyActivity.m
//  General_app
//
//  Created by Adrian Hoitan on 25.01.12.
//  Copyright (c) 2012 getunik. All rights reserved.
//

#import "WUIButtonApplyActivity.h"
#import "CCore.h"
#import "WGoogleMapsCustom.h"
#import "WUINCPMapWithFilter.h"
#import "WUITableViewPOIList.h"

@implementation WUIButtonApplyActivity

/**
 * Send an callback event to our parent widgets informing about the new action.
 *
 * @param nsstrSenderID Sender ID
 * @param nsnIndex Index number of the widget that is calling the callback
 * @param nsnActionType Specific action that was triggered
 * @return None
 **/
-(void)sendCallbackEvent:(NSString *)nsstrSenderID index:(NSNumber*)nsnIndex actionType:(NSNumber*)nsnActionType
{
    if ([nsstrSenderID isEqualToString:@"IDApplyActivity"])
    {
        WGoogleMapsCustom *wgmcGoogleMap = (WGoogleMapsCustom*)[m_cCore getWidget:@"IDMGoogleMainMap"];
        [wgmcGoogleMap updatePOIDisplay];
        
        WUITableViewPOIList *wuitvNearbyList = (WUITableViewPOIList*)[m_cCore getWidget:@"IDTVNearbyList"];
        [wuitvNearbyList fillTableView];
        
        WUINCPMapWithFilter *wmfFilterMap = (WUINCPMapWithFilter*)[m_cCore getWidget:@"IDNCPMapView"];
        [wmfFilterMap curlMapviewPage];
    }
}

@end
