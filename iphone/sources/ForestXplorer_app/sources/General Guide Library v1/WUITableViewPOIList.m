//
//  WUITableViewPOIList.m
//  General_app
//
//  Created by ahn on 11.10.11.
//  Copyright 2011 getunik. All rights reserved.
//

#import "WUITableViewPOIList.h"
#import "WUITableViewCellPOIList.h"
#import "WUIWheelPickerActivity.h"
#import "UAnalyticsTracking.h"

@implementation WUITableViewPOIList

/**
 * Initialize our widget with the informations that are coming from an XML node. The Node must be a valid macc node item.
 * 
 * @param cxmlNode XML node that contains all the data.
 * @param rmResourcesManager Pointer to an resource manager that contain real values. It can be null in some cases.
 * @param iIndex If this item comes from an group with multiple items, this will specify the index of the item inside the list.
 * @param ccCore Pointer to the widget and resource creator.
 * @return id of the created item.
 */
-(id)initWithXMLNode:(CXMLElement*)cxmlNode resourcesManager:(CResourceManager*)rmResourcesManager index:(int)iIndex core:(CCore*)ccCore parentWidget:(IWidget *)iwParentWidget
{
    [super initWithXMLNode:cxmlNode resourcesManager:rmResourcesManager index:iIndex core:ccCore parentWidget:iwParentWidget];
    
    //< Initialize all location manager details
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    [locationManager startUpdatingLocation];
    
    return self;
}

/**
 * Tells the delegate that a new location value is available.
 *
 * @param manager The location manager object that generated the update event.
 * @param newLocation The new location data.
 * @param oldLocation The location data from the previous update. If this is the first update event delivered by this location manager, this parameter is nil.
 * @return none
 */
-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSDate* newLocationeventDate = newLocation.timestamp;
	NSTimeInterval howRecentNewLocation = [newLocationeventDate timeIntervalSinceNow];
    
    // Needed to filter cached and too old locations
	if ((!currentLocation || currentLocation.horizontalAccuracy > newLocation.horizontalAccuracy) &&
		(howRecentNewLocation < -0.0 && howRecentNewLocation > -10.0))
    {
		if (currentLocation)
			[currentLocation release];
		
		currentLocation = [newLocation retain];
        [self fillTableView];
	}
    
/*    if (nil != currentLocation)
    {
        [currentLocation release];
    }
    currentLocation = [[CLLocation alloc] initWithLatitude:newLocation.coordinate.latitude longitude:newLocation.coordinate.longitude];*/
    
    
}

/**
 * Fill the table view
 *
 * @return none
 */
-(void)fillTableView
{
    WUITableViewCellPOIList *cell = nil;
    WUITableViewSection *uitvsSection = [m_nsmaTableSections objectAtIndex:0];
    
    WUIWheelPickerActivity *uiwpSelectedActivity = (WUIWheelPickerActivity*)[m_cCore getWidget:@"IDWPActivities"];
    int iSelectedActivity = [uiwpSelectedActivity getSelectedInt];
    
    int iSize = [uitvsSection getCellCount];
    int iBackgroundCount = 0;
    for (int iStep = 0; iStep < iSize; iStep++)
    {
        cell = (WUITableViewCellPOIList*)[uitvsSection getCellAtIndex:iStep];
        if (NO == [cell isActivityListed:iSelectedActivity])
        {
            [[cell getUITableCell] setHidden:YES];
            [cell setCellHeight:0];
        }
        else
        {
            if (iBackgroundCount % 2 == 0)
            {
                cell.m_uicBackgroundColor = [UIColor whiteColor];
            }
            else
            {
                cell.m_uicBackgroundColor = [[UIColor alloc] initWithRed:0.94 green:0.93 blue:0.92 alpha:1.0];
            }
            iBackgroundCount++;
            [[cell getUITableCell] setHidden:NO];
            [cell setCellHeight:122];
        }
        
        [cell refreshViewWithNewLocation:currentLocation];
    }
    
    [uitvsSection sort];
    
    [m_uitvTableView reloadData];
}

/**
 * 
 *
 * @return none
 */
-(void)updateMetrics
{
    [self fillTableView];
}

/**
 * Send an callback event to our parent widgets informing about the new action.
 *
 * @param nsstrSenderID Sender ID
 * @param nsnIndex Index number of the widget that is calling the callback
 * @param nsnActionType Specific action that was triggered
 * @return None
 **/
-(void)sendCallbackEvent:(NSString *)nsstrSenderID index:(NSNumber*)nsnIndex actionType:(NSNumber*)nsnActionType
{
    if ([nsstrSenderID isEqualToString:@"IDNCPDetailsPOI"])
    {
        RNumber *rnSelectedPOIIndex = (RNumber*)[m_rmResourcesManager getResource:@"[@RNPOIIndex]"];
        [rnSelectedPOIIndex setValue:(int)[nsnIndex intValue]];
        
        NSString *strName = [NSString stringWithFormat:@"/iPhone/%@/%@", [m_rmResourcesManager getSelectedLanguage], [m_rmResourcesManager getStrAttributeValue:@"[@DataXMLList]./title_of_the_poi" defaultValue:@"" index:[nsnIndex intValue]]];
#ifdef INCLUDE_GOOGLE_ANALYTICS
        [[UAnalyticsTracking sharedUAnalyticsHelper] trackGoogleAnalyticsPage:strName];
#endif
    }
    
    [super sendCallbackEvent:nsstrSenderID index:nsnIndex actionType:nsnActionType];
}

@end
