//
//  WUITableViewSectionAP.m
//  General_app
//
//  Created by Adrian Hoitan on 30.01.12.
//  Copyright (c) 2012 getunik. All rights reserved.
//

#import "WUITableViewSectionAP.h"

@implementation WUITableViewSectionAP

/**
 * Load the property called title.
 *
 * @param cxmlAttributeNode XML node that contains the data for this attribute.
 * @return none
 * @note This function can be overwritten if we want to load a different data
 */
-(void)loadAttributeTitle:(CXMLNode*)cxmlAttributeNode
{
    //< Set the displayed text
    if (nil != cxmlAttributeNode)
    {
        m_nsstrSectionTitle = [m_rmResourcesManager getStrAttributeValue:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/title_of_the_poi" defaultValue:@"" index:m_iWidgetIndex];
    }
}

@end
