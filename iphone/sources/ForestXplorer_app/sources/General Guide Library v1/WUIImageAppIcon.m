//
//  WUIImageAppIcon.m
//  ForestXplorer_app
//
//  Created by Adrian Hoitan on 22/09/14.
//  Copyright (c) 2014 Adrian Hoitan. All rights reserved.
//

#import "WUIImageAppIcon.h"

@implementation WUIImageAppIcon

/**
 * Initialize our widget with the informations that are coming from an XML node. The Node must be a valid macc node item.
 *
 * @param cxmlNode XML node that contains all the data.
 * @param rmResourcesManager Pointer to an resource manager that contain real values. It can be null in some cases.
 * @param iIndex If this item comes from an group with multiple items, this will specify the index of the item inside the list.
 * @param ccCore Pointer to the widget and resource creator.
 * @return id of the created item.
 */
-(id)initWithXMLNode:(CXMLElement*)cxmlNode resourcesManager:(CResourceManager*)rmResourcesManager index:(int)iIndex core:(CCore*)ccCore
{
    [super initWithXMLNode:cxmlNode resourcesManager:rmResourcesManager index:iIndex core:ccCore];
    
    //< Check if the POI is under windfarm construction
    NSUserDefaults *defaults  = [NSUserDefaults standardUserDefaults];
    NSString *nsstrDebugging = [defaults objectForKey:@"IDDebuggingEnabled"];
    
    if ((nil != nsstrDebugging) && ([nsstrDebugging length] > 0))
    {
        if ([nsstrDebugging isEqualToString:@"true"])
        {
            [self setDisplayResource:@"[@IDIAppIconDebug]"];
        }
    }
    
    return self;
}

@end
