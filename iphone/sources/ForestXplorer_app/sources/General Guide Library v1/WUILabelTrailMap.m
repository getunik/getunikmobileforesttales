//
//  WUILabelTrailMap.m
//  General_app
//
//  Created by Adrian Hoitan on 26.01.12.
//  Copyright (c) 2012 getunik. All rights reserved.
//

#import "WUILabelTrailMap.h"
#import "WUITableViewCell.h"

@implementation WUILabelTrailMap

/**
 * Load the property called text.
 *
 * @param cxmlAttributeNode XML node that contains the data for this attribute.
 * @return none
 * @note This function can be overwritten if we want to load a different data
 */
-(void)loadAttributeText:(CXMLNode*)cxmlAttributeNode
{
    NSString *nsstrFormat = nil;
    NSString *nsstrText   = nil;
    
    //< Set the displayed text
    if (nil != cxmlAttributeNode)
    {
        nsstrText = [m_rmResourcesManager getStrAttributeValue:[NSString stringWithFormat:@"[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/access_points/access_point[[@RNAccessPoint]]/trails/trail[%d]/title_of_the_trail", m_iWidgetIndex + 1] defaultValue:[NSString stringWithString:@""] index:m_iWidgetIndex];
        nsstrFormat = [[NSString alloc] initWithFormat:@"%@ - Map", nsstrText];
        [m_uilLabel setText:nsstrFormat];
        
        [nsstrFormat release];
        [nsstrText release];
    }
}

@end
