//
//  WUILightDownloadingView.h
//  Ratgeber AUT
//
//  Created by Adrian Hoitan on 21.12.11.
//  Copyright (c) 2011 getunik. All rights reserved.
//

#import "WUIView.h"

@interface WUIGuideDataDownloadingView : WUIView
{
    UIProgressView *m_uipvProgress;
}

@end
