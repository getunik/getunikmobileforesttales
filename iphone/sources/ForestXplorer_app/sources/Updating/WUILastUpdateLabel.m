//
//  WUILastUpdateLabel.m
//  Fishguide
//
//  Created by ahn on 01.06.11.
//  Copyright 2011 getunik. All rights reserved.
//

#import "WUILastUpdateLabel.h"


@implementation WUILastUpdateLabel

/**
 * Initialize our widget with the informations that are coming from an XML node. The Node must be a valid macc node item.
 * 
 * @param cxmlNode XML node that contains all the data.
 * @param rmResourcesManager Pointer to an resource manager that contain real values. It can be null in some cases.
 * @param iIndex If this item comes from an group with multiple items, this will specify the index of the item inside the list.
 * @param ccCore Pointer to the widget and resource creator.
 * @return id of the created item.
 */
-(id)initWithXMLNode:(CXMLElement*)cxmlNode resourcesManager:(CResourceManager*)rmResourcesManager index:(int)iIndex core:(CCore*)ccCore
{
    [super initWithXMLNode:cxmlNode resourcesManager:rmResourcesManager index:iIndex core:ccCore];

    [self updateDisplayedText];
    
    return self;
}

/**
 *
 */
-(void)updateDisplayedText
{
    //< The date of the last update will be stored in our user defaults
    NSUserDefaults *nsudPreferred      = [NSUserDefaults standardUserDefaults];

    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyyMMddhhmmss"];
    NSDate *nsDate = [nsudPreferred objectForKey:@"CWWFFCWLastUpdateCheck"];
    NSString *nsstrLastUpdate = nil;
    if (nil != nsDate)
    {
        nsstrLastUpdate = [dateFormatter stringFromDate:[NSDate date]];
    }
    
    NSUserDefaults *defaults  = [NSUserDefaults standardUserDefaults];
    NSArray        *languages = [defaults objectForKey:@"AppleLanguages"];
    
    NSString *nsstrSelectedLanguage   = [languages objectAtIndex:0];
    
    if (nil == nsstrLastUpdate)
    {
        if ([nsstrSelectedLanguage isEqualToString:@"cy"])
        {
            [self setText:[NSString stringWithFormat:@"Dim ar gael diweddariadau"]];
        }
        else
        {
            if ([nsstrSelectedLanguage isEqualToString:@"de"])
            {
                [self setText:[NSString stringWithFormat:@"Keine Letzte Aktualisierung"]];
            }
            else
            {
                [self setText:[NSString stringWithFormat:@"No updates available"]];
            }
        }
    }
    else
    {
        if ([nsstrSelectedLanguage isEqualToString:@"cy"])
        {
            [self setText:[NSString stringWithFormat:@"Diweddariad diwethaf: %@.%@.%@ %@:%@",
                           [nsstrLastUpdate substringWithRange:NSMakeRange(6, 2)], 
                           [nsstrLastUpdate substringWithRange:NSMakeRange(4, 2)],
                           [nsstrLastUpdate substringWithRange:NSMakeRange(0, 4)],
                           [nsstrLastUpdate substringWithRange:NSMakeRange(8, 2)],
                           [nsstrLastUpdate substringWithRange:NSMakeRange(10, 2)]]];
        }
        else
        {
            if ([nsstrSelectedLanguage isEqualToString:@"de"])
            {
                [self setText:[NSString stringWithFormat:@"Letzte Aktualisierung: %@.%@.%@ %@:%@",
                               [nsstrLastUpdate substringWithRange:NSMakeRange(6, 2)], 
                               [nsstrLastUpdate substringWithRange:NSMakeRange(4, 2)],
                               [nsstrLastUpdate substringWithRange:NSMakeRange(0, 4)],
                               [nsstrLastUpdate substringWithRange:NSMakeRange(8, 2)],
                               [nsstrLastUpdate substringWithRange:NSMakeRange(10, 2)]]];
            }
            else
            {
            [self setText:[NSString stringWithFormat:@"Last update: %@.%@.%@ %@:%@",
                           [nsstrLastUpdate substringWithRange:NSMakeRange(6, 2)], 
                           [nsstrLastUpdate substringWithRange:NSMakeRange(4, 2)],
                           [nsstrLastUpdate substringWithRange:NSMakeRange(0, 4)],
                           [nsstrLastUpdate substringWithRange:NSMakeRange(8, 2)],
                           [nsstrLastUpdate substringWithRange:NSMakeRange(10, 2)]]];
            }
        }
    }
}

@end
