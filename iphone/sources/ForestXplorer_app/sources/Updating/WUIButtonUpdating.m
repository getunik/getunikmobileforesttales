//
//  WUIButtonUpdating.m
//  Fishguide
//
//  Created by ahn on 20.05.11.
//  Copyright 2011 getunik. All rights reserved.
//

#import "WUIButtonUpdating.h"
#import "AppDelegate.h"
#import "WUILabel.h"

@implementation WUIButtonUpdating

/**
 * Initialize our widget with the informations that are coming from an XML node. The Node must be a valid macc node item.
 * 
 * @param cxmlNode XML node that contains all the data.
 * @param rmResourcesManager Pointer to an resource manager that contain real values. It can be null in some cases.
 * @param iIndex If this item comes from an group with multiple items, this will specify the index of the item inside the list.
 * @param ccCore Pointer to the widget and resource creator.
 * @return id of the created item.
 */
-(id)initWithXMLNode:(CXMLElement*)cxmlNode resourcesManager:(CResourceManager*)rmResourcesManager index:(int)iIndex core:(CCore*)ccCore
{
    [super initWithXMLNode:cxmlNode resourcesManager:rmResourcesManager index:iIndex core:ccCore];
    
    m_bVerifyState = true;
    
    return self;
}

/**
 *
 */
-(void)controlTouchUpInside:(id)sender
{
    //< Get the app delegate instance
    AppDelegate *m_appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    if (m_bVerifyState == true)
    {        
        [m_appDelegate.m_uivViewController threadIsApplcationDataUpdated];
    }
    else
    {
        //< Cancel the download
        m_appDelegate.m_uivViewController.m_bStopUpdating = true;
    }
}

/**
 * This button can have 2 states. Enable the verify of updates state and cancel downloading state
 */
-(void)toogleVerifyState:(bool)bVerifyState
{
    m_bVerifyState = bVerifyState;
    
    if (false == m_bVerifyState)
    {
        [self setTitle:@"Abort"];
    }
    else
    {
        [self setTitle:@"Check for new updates"];
    }

}

@end