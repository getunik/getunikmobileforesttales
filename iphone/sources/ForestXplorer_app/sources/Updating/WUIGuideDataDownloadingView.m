//
//  WUILightDownloadingView.m
//  Ratgeber AUT
//
//  Created by Adrian Hoitan on 21.12.11.
//  Copyright (c) 2011 getunik. All rights reserved.
//

#import "WUIGuideDataDownloadingView.h"
#import "AppDelegate.h"

@implementation WUIGuideDataDownloadingView

/**
 * Initialize our widget with the informations that are coming from an XML node. The Node must be a valid macc node item.
 * 
 * @param cxmlNode XML node that contains all the data.
 * @param rmResourcesManager Pointer to an resource manager that contain real values. It can be null in some cases.
 * @param iIndex If this item comes from an group with multiple items, this will specify the index of the item inside the list.
 * @param ccCore Pointer to the widget and resource creator.
 * @return id of the created item.
 */
-(id)initWithXMLNode:(CXMLElement*)cxmlNode resourcesManager:(CResourceManager*)rmResourcesManager index:(int)iIndex core:(CCore*)ccCore
{
    [super initWithXMLNode:cxmlNode resourcesManager:rmResourcesManager index:iIndex core:ccCore];
    
    m_uipvProgress = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];
    [m_uipvProgress setFrame:CGRectMake(10, 0, 300, 20)];
    
    [m_uivView addSubview:m_uipvProgress];
    
    return self;
}

/**
 * Reset the progress bar to 0%
 *
 * @return none
 */
-(void)resetProgressBar
{
    [m_uipvProgress setProgress:0.0];
}


/**
 * Start the updating process
 *
 * @return none
 */
-(void)startUpdating
{
    [self performSelectorInBackground:@selector(updateDatabase) withObject:nil];
}

/**
 * Update the completed progress
 *
 * @param fValue New progress value 0.0-1.0 range
 * @return none
 */
-(void)updateProgressView:(NSNumber*)fValue
{
    [m_uipvProgress setProgress:[fValue floatValue]];
}

/**
 *
 */
-(void)updateDatabase
{
    bool bDatabasesUpdated = true;
    
    //< Get the app delegate instance
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    //< Move the progress bar
    [self performSelectorOnMainThread:@selector(updateProgressView:) withObject:[NSNumber numberWithFloat:(float) 0.1] waitUntilDone:NO];
    
    //< Cancel the download if requested
    if (true == appDelegate.m_uivViewController.m_bStopUpdating){bDatabasesUpdated = false;}
    
    //< Update our RSS
    RRSSData *data = (RRSSData*)[[m_cCore getResourcesManager] getResource:@"[@DataXML]"];
    if (false != bDatabasesUpdated)
    {
        bDatabasesUpdated = [data redownloadAndLoadContent];
    }
   
    //< Fake the rest of the progress
    for (int iStep = 2; (iStep <= 10)&&(bDatabasesUpdated == true); iStep++)
    {
        //< Cancel the download if requested
        if (true == appDelegate.m_uivViewController.m_bStopUpdating){bDatabasesUpdated = false;}
        
        [NSThread sleepForTimeInterval:0.1];
        [self performSelectorOnMainThread:@selector(updateProgressView:) withObject:[NSNumber numberWithFloat:(float) iStep / 10.0f] waitUntilDone:NO];
    }
    
    //< Call the updating mechanism for the next item
    if (true == bDatabasesUpdated)
    {
        [appDelegate.m_uivViewController completeUpdate:true];
    }
    else
    {
        //< The update is not completed, cancel the rest of the downloading
        [appDelegate.m_uivViewController completeUpdate:false];
    }
    
}

@end
