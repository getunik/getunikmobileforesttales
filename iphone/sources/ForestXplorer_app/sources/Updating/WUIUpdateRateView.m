//
//  WUIUpdateRateView.m
//  Fishguide
//
//  Created by ahn on 31.05.11.
//  Copyright 2011 getunik. All rights reserved.
//

#import "WUIUpdateRateView.h"


@implementation WUIUpdateRateView

/**
 * Initialize our widget with the informations that are coming from an XML node. The Node must be a valid macc node item.
 * 
 * @param cxmlNode XML node that contains all the data.
 * @param rmResourcesManager Pointer to an resource manager that contain real values. It can be null in some cases.
 * @param iIndex If this item comes from an group with multiple items, this will specify the index of the item inside the list.
 * @param ccCore Pointer to the widget and resource creator.
 * @return id of the created item.
 */
-(id)initWithXMLNode:(CXMLElement*)cxmlNode resourcesManager:(CResourceManager*)rmResourcesManager index:(int)iIndex core:(CCore*)ccCore
{
    [super initWithXMLNode:cxmlNode resourcesManager:rmResourcesManager index:iIndex core:ccCore];
    
    NSArray *itemArray = [NSArray arrayWithObjects: @"Immer", @"Täglich", @"Wöchentlich", nil];
    UISegmentedControl *m_scSegmentedControl = [[UISegmentedControl alloc] initWithItems:itemArray];
    m_scSegmentedControl.frame = CGRectMake(10, 0, 300, 35);
    m_scSegmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
    
    UIColor *uiColor = [rmResourcesManager getColorAttributeValue:@"0xa8d474FF" index:iIndex];
    [m_scSegmentedControl setTintColor:[UIColor lightGrayColor]];
    [uiColor release];
    
    //< Get and set the selected segment index
    NSUserDefaults *nsudPreferred = [NSUserDefaults standardUserDefaults];
    NSNumber *nsnSelectedIndex    = [nsudPreferred objectForKey:@"CWWFFCWRatgeberUpdateRate"];
    m_scSegmentedControl.selectedSegmentIndex = [nsnSelectedIndex intValue];
    
    [m_scSegmentedControl addTarget:self action:@selector(rateChanged:) forControlEvents:UIControlEventValueChanged];
    
    [m_uivView addSubview:m_scSegmentedControl];
    
    return self;
}

/**
 * Called when the update range has changed
 */
- (void)rateChanged:(id)sender
{
    UISegmentedControl *segmentedControl = (UISegmentedControl *)sender;
    
    //< Write the preffered update rate
    NSUserDefaults *nsudPreferred = [NSUserDefaults standardUserDefaults];
    [nsudPreferred setObject:[NSNumber numberWithInt:[segmentedControl selectedSegmentIndex]] forKey:@"CWWFFCWRatgeberUpdateRate"];
    [nsudPreferred synchronize];
}

@end
