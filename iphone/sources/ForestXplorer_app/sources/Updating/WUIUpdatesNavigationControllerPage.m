//
//  WUIUpdatesNavigationControllerPage.m
//  Fishguide
//
//  Created by ahn on 06.06.11.
//  Copyright 2011 getunik. All rights reserved.
//

#import "WUIUpdatesNavigationControllerPage.h"
#import "WUILabel.h"

@implementation WUIUpdatesNavigationControllerPage

/**
 * Function called after the pop animation to happen.
 *
 * @return none
 */
-(void)unloadContent
{
    WUILabel *uilText = [m_cCore getWidget:@"IDLNoMoreUpdates"];
    [[uilText getLabel] setHidden:YES];
    
    [super unloadContent];
}

@end