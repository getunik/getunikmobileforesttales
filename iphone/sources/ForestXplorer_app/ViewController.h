//
//  ViewController.h
//  ForestXplorer_app
//
//  Created by Adrian Hoitan on 10/16/13.
//  Copyright (c) 2013 Adrian Hoitan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Guide_interface_loader.h"

@interface ViewController : UIViewController
{
#ifdef APPMAKER_DEMO
    #define LAST_UPDATE_HTTP @"http://admin.guideappmaker.com/demo/lastmodification"
    #define PUSH_SERVER_HTTP @"http://admin.guideappmaker.com/demo/token"
    #define PUSH_SERVER_USER @"demo"
    #define PUSH_SERVER_PASS @"demo"
#else
#ifdef FOREST_TALES
    #define LAST_UPDATE_HTTP @"http://admin.guideappmaker.com/foresttale/lastmodification"
    #define PUSH_SERVER_HTTP @"http://admin.guideappmaker.com/foresttale/token"
    #define PUSH_SERVER_USER @"admin"
    #define PUSH_SERVER_PASS @"g3tun1k"
#else
    #define LAST_UPDATE_HTTP @"http://admin.guideappmaker.com/fcw/lastmodification"
    #define PUSH_SERVER_HTTP @"http://admin.guideappmaker.com/fcw/token"
    #define PUSH_SERVER_USER @"fcw"
    #define PUSH_SERVER_PASS @"gimidi66"
#endif
#endif
    
    
    
    WUITabController       *m_uivAddedView;
    Guide_interface_loader *m_guideInterfaceLoader;
    
    //< Updates availabile
    UIView                 *m_uivUADisableGlass;
    NSString               *m_nsstrServerUpdateDate;
    bool                   m_bStopUpdating;
    bool                   m_bCheckingForUpdates;
}

@property (nonatomic) bool m_bStopUpdating;
@end
