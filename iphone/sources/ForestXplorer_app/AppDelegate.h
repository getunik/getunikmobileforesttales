//
//  AppDelegate.h
//  ForestXplorer_app
//
//  Created by Adrian Hoitan on 10/16/13.
//  Copyright (c) 2013 Adrian Hoitan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    //< Updates availabile
    UIView               *m_uivUADisableGlass;
    NSString             *m_nsstrServerUpdateDate;
    bool                 m_bStopUpdating;
    bool                 m_bCheckingForUpdates;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) ViewController *m_uivViewController;

@end
