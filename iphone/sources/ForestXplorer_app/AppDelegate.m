//
//  AppDelegate.m
//  ForestXplorer_app
//
//  Created by Adrian Hoitan on 10/16/13.
//  Copyright (c) 2013 Adrian Hoitan. All rights reserved.
//

#import "AppDelegate.h"
#import "WUINavigationController.h"
#import "WUITableView.h"
#import "WUITableViewCell.h"
#import "ULanguageHelper.h"
#import "CHTTPClient.h"
#import "CHTTPPostRequestWithConnection.h"

@implementation AppDelegate

@synthesize m_uivViewController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //< Register for Puhs notifications
    if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
    {
        // iOS 8 Notifications
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [application registerForRemoteNotifications];
    }
    else
    {
        // iOS < 8 Notifications
        [application registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
    }
    
    // Override point for customization after application launch.
    return YES; 
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

//< Register for push notifications
-(void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    [NSThread detachNewThreadSelector:@selector(sendPushNotificationToken:) toTarget:self withObject:deviceToken];
}

-(void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err
{
    NSString *str = [NSString stringWithFormat: @"Error: %@", err];
    NSLog(@"Error Registering to push Notification: %@", str);
}

-(void)sendPushNotificationToken:(NSData *)deviceToken
{
    CHTTPPostRequestWithConnection *httpPostConnection = [[CHTTPPostRequestWithConnection alloc] init];
    [httpPostConnection sendPOSTRequest:[NSString stringWithFormat:@"name=iphone&token=%@", deviceToken] postURL:PUSH_SERVER_HTTP user:PUSH_SERVER_USER password:PUSH_SERVER_PASS];
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    for (id key in userInfo)
    {
        NSLog(@"key: %@, value: %@", key, [userInfo objectForKey:key]);
    }
}

@end
