package com.getunik.audiotours.forest.tales;

import org.dom4j.Element;

import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.graphics.drawable.GradientDrawable;
import net.getunik.android.core.InterfaceMaker;
import net.getunik.android.resources.CResourceManager;
import net.getunik.android.widgets.WUITableView;
import net.getunik.android.widgets.WUITableViewCell;

public class WUITLanguageView extends WUITableView
{
	/**
     * {@inheritDoc}
     */
    public Object initWithXMLNode(Element cxmlNode, CResourceManager rmResourcesManager, int iIndex, InterfaceMaker ccCore)
    {        
        super.initWithXMLNode(cxmlNode, rmResourcesManager, iIndex, ccCore);
        
        updateDisplay();
        
        return this;
    }
    
	/**
     * Send an callback event to our parent widgets informing about the new action.
     * 
     * @param nsstrSenderID Sender ID
     * @param nsnIndex Index number of the widget that is calling the callback
     * @return none
     */
    public void sendCallbackEvent(String nsstrSenderID, int nsnIndex)
    {
    	if (nsstrSenderID.equals("IDEnglish"))
    	{
    		writeLanguage("en");
    		
    		updateDisplay();
    	}
    	if (nsstrSenderID.equals("IDWelsh"))
    	{
    		writeLanguage("cy");
    		
    		updateDisplay();
    	}

    	AlertDialog deleteAlert = new AlertDialog.Builder(m_cCore.m_Activity).create();
		deleteAlert.setTitle("");
		
		if (m_cCore.getResourceManager().getSelectedLanguage().equals("cy"))
		{
			deleteAlert.setMessage("Ail-cychwynnwch yr ap i weld yr iaith wedi ei ddiweddaru.");
		}
		else
		{
			deleteAlert.setMessage("Please restart the application in order for the changes to take place.");
		}
		deleteAlert.show();

    	return;
    }
    
    protected void writeLanguage(String nsstrLanguage)
    {
    	// We need an Editor object to make preference changes.
        // All objects are from android.context.Context
        SharedPreferences settings = m_cCore.getMainContext().getSharedPreferences(this.getClass().getPackage().getName(), 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("Language", nsstrLanguage);
        
        // Commit the edits!
        editor.commit();
    }
    
    protected void updateDisplay()
    {
    	if (m_cCore.getResourceManager().getSelectedLanguage().equals("cy"))
        {
        	WUITableViewCell uivCellEnglish = (WUITableViewCell) m_cCore.getWidget("IDCEnglishLanguage");
    		uivCellEnglish.setAccessory(0);
    		WUITableViewCell uivCellWelsh = (WUITableViewCell) m_cCore.getWidget("IDCWelshLanguage");
    		uivCellWelsh.setAccessory(1);
        }
        else
        {
        	WUITableViewCell uivCellEnglish = (WUITableViewCell) m_cCore.getWidget("IDCEnglishLanguage");
    		uivCellEnglish.setAccessory(1);
    		WUITableViewCell uivCellWelsh = (WUITableViewCell) m_cCore.getWidget("IDCWelshLanguage");
    		uivCellWelsh.setAccessory(0);
        }
    }
}