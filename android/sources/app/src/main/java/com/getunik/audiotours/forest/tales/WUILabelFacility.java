package com.getunik.audiotours.forest.tales;

import org.dom4j.Element;

import net.getunik.android.core.InterfaceMaker;
import net.getunik.android.resources.CResourceManager;
import net.getunik.android.widgets.WUILabel;

public class WUILabelFacility extends WUILabel
{
	int m_iObjectIndex = 0;
	public Object initWithXMLNode(Element cxmlNode, CResourceManager rmResourcesManager, int iIndex, InterfaceMaker ccCore)
    {        
		m_iObjectIndex = iIndex;
        super.initWithXMLNode(cxmlNode, rmResourcesManager, 0, ccCore);
        
        return this;
    }
	
	/**
	 * Load the property called text.
	 *
	 * @param cxmlAttributeNode XML node that contains the data for this attribute.
	 * @return none
	 * @note This function can be overwritten if we want to load a different data
	 */
	public String loadAttributeText(Element cxmlAttributeNode)
	{
	    String nsstrReturn     = "";
	    String nsstrFacilityID = m_rmResourcesManager.getStrAttributeValue(String.format("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/facility[%d]", m_iObjectIndex + 1), "", 0);
	    
	    int iFacilities = m_rmResourcesManager.getIntAttributeValue("[@FacilitiesXMLList]./facility", 0, 0);
	    
	    for (int iStep = 0; iStep < iFacilities; iStep++)
	    {
	        String nsstrXMLFacilityID = m_rmResourcesManager.getStrAttributeValue(String.format("[@FacilitiesXMLList]./facility[%d]/@id", iStep + 1), "", 0);
	        if (nsstrXMLFacilityID.equals(nsstrFacilityID))
	        {
	            nsstrReturn = String.format("[@FacilitiesXMLList]./facility[%d]/title_of_the_facility", iStep + 1);
	        }
	    }
	    
	    String nsstrText = m_rmResourcesManager.getStrAttributeValue(nsstrReturn, "", m_iWidgetIndex);
	    
	    return nsstrText;
	}
}