package com.getunik.audiotours.forest.tales;

import org.dom4j.Element;

import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TableLayout.LayoutParams;

import net.getunik.android.widgets.WUITableViewSection;

public class WUITableViewSectionAP extends WUITableViewSection
{
	/**
     * Set the title for this section
     *  
     * @param strTitle String containing the title
     * @return none
     */
    protected void setSectionTitle(String strTitle)
    {
        m_nsstrSectionTitle = strTitle;
        if (null == m_tvHeader)
        {            
            m_tvHeader = new TextView(m_cCore.getMainContext(), null, 0);
            m_tvHeader.setTextColor(Color.BLACK);
            m_tvHeader.setTextSize(18);
            m_tvHeader.setTypeface(Typeface.DEFAULT, 1);
            m_tvHeader.setPadding(0, 40, 0, 40);
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
            lp.leftMargin = 10;
            lp.topMargin  = 8;
            lp.bottomMargin  = 8;
            
            //< Set the layout
            m_tvHeader.setLayoutParams(lp);
            m_rlHeader = new RelativeLayout(m_cCore.getMainContext());
            m_rlHeader.addView(m_tvHeader);
        }
        
        m_tvHeader.setText(m_nsstrSectionTitle);
    }
    
	/**
	 * Load the property called title.
	 *
	 * @param cxmlAttributeNode XML node that contains the data for this attribute.
	 * @return none
	 * @note This function can be overwritten if we want to load a different data
	 */
	public void loadAttributeTitle(Element cxmlAttributeNode)
	{
	    //< Set the displayed text
	    if (null != cxmlAttributeNode)
	    {
	    	m_nsstrSectionTitle = m_rmResourcesManager.getStrAttributeValue("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/title_of_the_poi", "", 0);
	    }
	}
}