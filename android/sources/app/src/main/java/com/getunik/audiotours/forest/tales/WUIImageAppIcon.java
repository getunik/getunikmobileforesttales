package com.getunik.audiotours.forest.tales;

import org.dom4j.Element;

import android.content.SharedPreferences;
import net.getunik.android.widgets.WUIImage;

public class WUIImageAppIcon extends WUIImage
{

	/**
	 * Load the property called text.
	 *
	 * @param cxmlAttributeNode XML node that contains the data for this attribute.
	 * @return none
	 * @note This function can be overwritten if we want to load a different data
	 */
	 public String loadAttributeResource(Element cxmlNode)
	{
		 String nsstrReturn = "[@IDIAppIcon]";
		 SharedPreferences settings = m_cCore.m_Activity.getSharedPreferences(m_cCore.m_Activity.getClass().getPackage().getName(), 0);
	     String strDebugging = settings.getString("IDDebuggingEnabled", "false");
	     
	     if ("false".equals(strDebugging) || null == strDebugging)
	     {
	    	 nsstrReturn = "[@IDIAppIcon]";
	     }
	     else
	     {
	    	 nsstrReturn = "[@IDIAppIconDebug]";
	     }
	     
	    return nsstrReturn;
	}
}