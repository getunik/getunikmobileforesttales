package com.getunik.audiotours.forest.tales;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.text.Html;
import net.getunik.android.resources.RString;
import net.getunik.android.widgets.WUINavigationControllerPage;

public class WUINavigationControllerPageAbout extends WUINavigationControllerPage
{
	int m_iEnableDebugging = 0;
	
	/**
     * Send an callback event to our parent widgets informing about the new action.
     * 
     * @param nsstrSenderID Sender ID
     * @param nsnIndex Index number of the widget that is calling the callback
     * @return none
     */
    public void sendCallbackEvent(String nsstrSenderID, int nsnIndex)
    {
	    boolean bHandled = false;
	    
	    if (nsstrSenderID.equals("IDCPShare"))
	    { 	
	    	Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
	    	sharingIntent.setType("text/plain");

	    	String nsstrMessage = m_rmResourcesManager.getStrAttributeValue("[@SettingsXMLList]./setting/share_android", "", 0);
	    	sharingIntent.putExtra(Intent.EXTRA_TEXT, nsstrMessage);

	    	try
	    	{
	    	    m_cCore.m_Activity.startActivity(Intent.createChooser(sharingIntent, "Share"));
	    	}
	    	catch (android.content.ActivityNotFoundException ex)
	    	{
	    	}
	    	
	    	String strName = String.format("/android/%s/about/share", m_rmResourcesManager.getSelectedLanguage());
	        m_cCore.getTrackObject().sendTrackDetails(strName);
	    	
	    	bHandled = true;
	    }
	    
	    if (nsstrSenderID.equals("IDCPRate"))
	    {        
	    	String nsstrURL = m_rmResourcesManager.getStrAttributeValue("[@SettingsXMLList]./setting/play_store_url", "", 0);
	    	Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(nsstrURL));
	    	m_cCore.m_Activity.startActivity(browserIntent);

	    	String strName = String.format("/android/%s/about/rate", m_rmResourcesManager.getSelectedLanguage());
	        m_cCore.getTrackObject().sendTrackDetails(strName);
	        
	        bHandled = true;
	    }
	    
	    if (nsstrSenderID.equals("IDCPOtherApps"))
	    {        
	    	String nsstrLink = m_rmResourcesManager.getStrAttributeValue("[@SettingsXMLList]./setting/play_store_publisher_url", "", 0);
	    	
	    	Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(nsstrLink));
	    	m_cCore.m_Activity.startActivity(browserIntent);

	    	String strName = String.format("/android/%s/about/otherapps", m_rmResourcesManager.getSelectedLanguage());
	        m_cCore.getTrackObject().sendTrackDetails(strName);
	        
	        bHandled = true;
	    }
	    
	    if (nsstrSenderID.equals("IDCPContact"))
	    { 
	    	Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

	    	String[] recipients = null;
	    	
	    	if (m_rmResourcesManager.getSelectedLanguage().equals("cy"))
	    	{
	    		recipients = new String[]{"chwedl.app@cyfoethnaturiolcymru.gov.uk", "",};
	    	}
	    	else
	    	{
	    		recipients = new String[]{"tales.app@naturalresourceswales.gov.uk", "",};
	    	}
	    	
	    	emailIntent.putExtra(Intent.EXTRA_EMAIL, recipients);
	    	emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback on Android App");
	    	emailIntent.putExtra(Intent.EXTRA_TEXT   , "");
			
	    	emailIntent.setType("text/plain");
	    	try {
	    	    m_cCore.m_Activity.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
	    	} catch (android.content.ActivityNotFoundException ex)
	    	{
	    	}
	    	
	    	String strName = String.format("/android/%s/about/contactdeveloper", m_rmResourcesManager.getSelectedLanguage());
	        m_cCore.getTrackObject().sendTrackDetails(strName);
	    	
	    	bHandled = true;
	    }
	    
	    if (nsstrSenderID.equals("IDBAppIconDebug"))
	    { 
	    	m_iEnableDebugging++;
	    	if (10 == m_iEnableDebugging)
	    	{
	    		m_iEnableDebugging = 0;
	    	
	    		//< Read the current debugging mode
	    		SharedPreferences settings = m_cCore.m_Activity.getSharedPreferences(m_cCore.m_Activity.getClass().getPackage().getName(), 0);
	            String strDebugging = settings.getString("IDDebuggingEnabled", "false");
	            
	            if ("false".equals(strDebugging) || null == strDebugging)
	            {
	            	AlertDialog alertDialog;
	            	alertDialog = new AlertDialog.Builder(m_cCore.getMainContext()).create();
	            	alertDialog.setTitle("Information");
	            	alertDialog.setMessage("Preview mode enabled. Please restart the app.");
	            	alertDialog.show();
	            	
	            	SharedPreferences.Editor editor = settings.edit();
	            	editor.putString("IDDebuggingEnabled", "true");
	                editor.commit();
	            }
	            else
	            {
	            	AlertDialog alertDialog;
	            	alertDialog = new AlertDialog.Builder(m_cCore.getMainContext()).create();
	            	alertDialog.setTitle("Information");
	            	alertDialog.setMessage("Preview mode disabled. Please restart the app.");
	            	alertDialog.show();
	            	
	            	SharedPreferences.Editor editor = settings.edit();
	            	editor.putString("IDDebuggingEnabled", "false");
	                editor.commit();
	            }
	    	}
	    }

	    if (false == bHandled)
	    {
	        super.sendCallbackEvent(nsstrSenderID, nsnIndex);
	    }
	}
}