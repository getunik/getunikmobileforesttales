package com.getunik.audiotours.forest.tales;

import android.view.View;
import net.getunik.android.widgets.WUINCPRightButton;
import net.getunik.android.widgets.WUINavigationControllerPage;
import net.getunik.android.widgets.WUIView;

public class WUINCPMapWithFilter extends WUINavigationControllerPage
{
	public void removeMap()
	{
		WGoogleMapsCustom maps = (WGoogleMapsCustom)m_cCore.getWidget("IDMGoogleMainMap");
		getView().removeView(maps.getMapView());
	}
	/**
	 * Function called by the callback function.
	 * Note: This function should be overwritten in the derived classes if they want to handle the events recived
	 *
	 * @param nsstrSenderID Sender ID
	 * @param nsnIndex Index number of the widget that is calling the callback
	 * @param nsnActionType Specific action that was triggered
	 * @return None
	 *
	 **/
	public void subControllerCallback(String nsstrSenderID, int nsnIndex)
	{
	    boolean bHandled = false;
	    
	    if (true == m_cCore.isUserInteractionEnabled())
    	{
	    	if (nsstrSenderID.equals("IDNCPNearbyList"))
	    	{
	    		WUIView uivPOIList = (WUIView)m_cCore.getWidget("IDVNearbyList");
	    		WGoogleMapsCustom wgmMapView = (WGoogleMapsCustom)m_cCore.getWidget("IDMGoogleMainMap");
	    		WUINCPRightButton wuirbButton = (WUINCPRightButton)m_cCore.getWidget("IDBNearby");
	    		
	    		if (uivPOIList.getView().getVisibility() == View.VISIBLE)
	    		{
	    			uivPOIList.setVisibility(View.INVISIBLE);
	    			wgmMapView.setHidden(false);
	    			
	    			if (m_rmResourcesManager.getSelectedLanguage().equals("cy"))
	    			{
	    				wuirbButton.getButton().setText("Gerllaw");
	    			}
	    			else
	    			{
	    				wuirbButton.getButton().setText("Nearby");
	    			}
	    		}
	    		else
	    		{
	    			uivPOIList.setVisibility(View.VISIBLE);
	    			wgmMapView.setHidden(true);
	    			wuirbButton.getButton().setText("Map");
	    		}
	    		
	    		bHandled = true;
	    	}
	    	
	    	if (nsstrSenderID.equals("IDBFilter"))
	    	{
	    		WUIView uivFilter = (WUIView)m_cCore.getWidget("IDWPActivities");
				if (true == (uivFilter.getView().getVisibility() == View.VISIBLE))
				{
	    			uivFilter.setVisibility(View.INVISIBLE);
				}
				else
				{
					uivFilter.setVisibility(View.VISIBLE);
				}
	    		
	    		bHandled = true;
	    	}
	    
	    
	    	if (false == bHandled)
	    	{
	    		super.subControllerCallback(nsstrSenderID, nsnIndex);
	    	}
    	}
	}
}