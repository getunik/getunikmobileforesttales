package com.getunik.audiotours.forest.tales;

import java.io.File;

import org.dom4j.Element;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import net.getunik.android.core.InterfaceMaker;
import net.getunik.android.resources.CResourceManager;
import net.getunik.android.resources.RString;
import net.getunik.android.utils.CToolbox;
import net.getunik.android.widgets.WUILabel;
import net.getunik.android.widgets.WUITableViewCell;

public class WUITVPDFTrailCell extends WUITableViewCell
{
	String m_nsstrTitle = null;
	String m_nsstrMapPath = null;
	
	/**
	 * Initialize our widget with the informations that are coming from an XML node. The Node must be a valid macc node item.
	 * 
	 * @param cxmlNode XML node that contains all the data.
	 * @param rmResourcesManager Pointer to an resource manager that contain real values. It can be null in some cases.
	 * @param iIndex If this item comes from an group with multiple items, this will specify the index of the item inside the list.
	 * @param ccCore Pointer to the widget and resource creator.
	 * @return id of the created item.
	 */
	public Object initWithXMLNode(Element cxmlNode, CResourceManager rmResourcesManager, int iIndex, InterfaceMaker ccCore)
    {
        super.initWithXMLNode(cxmlNode, rmResourcesManager, iIndex, ccCore);
	    
        if (getID().equals("IDTVCPDFAudioTrails"))
        {
            m_nsstrTitle   = m_rmResourcesManager.getStrAttributeValue(String.format("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/audio_trails/audio_trail[%d]/title", iIndex + 1), "", 0);
            m_nsstrMapPath = m_rmResourcesManager.getStrAttributeValue(String.format("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/audio_trails/audio_trail[%d]/pdf", iIndex + 1), "", 0);
        }
        
        if (getID().equals("IDTVCPDFFolkTrails"))
        {
            m_nsstrTitle   = m_rmResourcesManager.getStrAttributeValue(String.format("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/folk_tales/folk_tale[%d]/title", iIndex + 1), "", 0);
            m_nsstrMapPath = m_rmResourcesManager.getStrAttributeValue(String.format("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/folk_tales/folk_tale[%d]/pdf", iIndex + 1), "", 0);
        }
        
        if (getID().equals("IDTVCPDFHeritageTrails"))
        {
            m_nsstrTitle   = m_rmResourcesManager.getStrAttributeValue(String.format("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/heritage_scrolls/heritage_scroll[%d]/title", iIndex + 1), "", 0);
            m_nsstrMapPath = m_rmResourcesManager.getStrAttributeValue(String.format("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/heritage_scrolls/heritage_scroll[%d]/pdf", iIndex + 1), "", 0);
        }
        
        if ((null == m_nsstrMapPath) || (m_nsstrMapPath.length() == 0))
        {
            this.getCellView().setVisibility(View.INVISIBLE);
        }
        
	    return this;
	}
	
	/**
     * Load the content presented by this cell
     * 
     * @return none
     */
    public void loadContent()
    {
    	if (false == m_bContentLoaded)
        {
            super.loadContent();
            
            if ((null != m_nsstrMapPath) && (m_nsstrMapPath.length() > 0))
            {
                WUILabel uilTitle = (WUILabel)getChildWidget("IDLTitle");
                uilTitle.setText(String.format("%s PDF (pdf)", m_nsstrTitle));
            }
        }
    }
    
  ///
    ///
    ///
    public String getActionID()
    {
        RString rstrTrailPath = (RString)m_rmResourcesManager.getResource("[@IDSTrailMapPath]");
        rstrTrailPath.setValue(m_nsstrMapPath);
        
        showPDFDocument("[@IDSTrailMapPath]");
        
        return null;
    }
    
    /**
     * Download and display a pdf document in an external pdf viewer
     * @param strParsePath
     */
    public void showPDFDocument(String strParsePath)
    {
    	final String strPDF = m_rmResourcesManager.getStrAttributeValue(strParsePath, "", 0);
  		
  		final String strCachedImageName = Integer.toString(strPDF.hashCode()) + ".pdf";
  		
  		if (false == CToolbox.fileExistsOnSDCard("/forestTales", strCachedImageName))
  		{
  			final AlertDialog alertDialog;
  			alertDialog = new AlertDialog.Builder(m_cCore.getMainContext()).create();
  			alertDialog.setTitle("Downloading");
  			if (m_rmResourcesManager.getSelectedLanguage().equals("cy"))
  			{
  				alertDialog.setMessage("Lawrlwytho Map Lleoliad. Disgwyliwch.");
  			}
  			else
  			{
  				alertDialog.setMessage("Downloading Location Map. Please wait.");
  			}
  			alertDialog.show();

  			final Handler handler = new Handler();
  			handler.postDelayed(new Runnable() {
  			  @Override
  			  public void run() {
  				  CToolbox.downloadHTTPFileToSDCard(strPDF, "/forestTales", strCachedImageName);
  				  
  				  alertDialog.cancel();
  				  
  				  File pdfFile = new File(String.format("/sdcard/forestTales/%s", strCachedImageName)); 
    				if(pdfFile.exists()) 
    				{
    					Uri path = Uri.fromFile(pdfFile);
    					Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
    					pdfIntent.setDataAndType(path, "application/pdf");
    					pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

    					try
    					{
    						m_cCore.m_Activity.startActivity(pdfIntent);
    					}
    					catch(ActivityNotFoundException e)
    					{
    						Toast.makeText(m_cCore.getMainContext(), "No Application available to view pdf", Toast.LENGTH_LONG).show(); 
    					}
    				}
  			  }
  			}, 1000);
  		}
  		else
  		{
  			File pdfFile = new File(String.format("/sdcard/forestTales/%s", strCachedImageName)); 
  			if(pdfFile.exists()) 
  			{
  				Uri path = Uri.fromFile(pdfFile);
  				Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
  				pdfIntent.setDataAndType(path, "application/pdf");
  				pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

  				try
  				{
  					m_cCore.m_Activity.startActivity(pdfIntent);
  				}
  				catch(ActivityNotFoundException e)
  				{
  					Toast.makeText(m_cCore.getMainContext(), "No Application available to view pdf", Toast.LENGTH_LONG).show(); 
  				}
  			}
  		}
    }
}