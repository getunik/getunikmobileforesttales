package com.getunik.audiotours.forest.tales;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import net.getunik.android.core.CContentManager;
import net.getunik.android.core.InterfaceMaker;
import net.getunik.android.eventracker.CTrackStatistics;
import net.getunik.android.httphandling.CHTTPAsyncClient;
import net.getunik.android.httphandling.CHTTPAsyncClient.IPublicAsyncClientCallback;
import net.getunik.android.httphandling.CHTTPClient;
import net.getunik.android.httphandling.CHTTPRequestSettings;
import net.getunik.android.notifications.CNotifications;
import net.getunik.android.utils.CUserLocation;
import net.getunik.android.utils.CUserLocation.IUserLocationCallback;
import net.getunik.android.widgets.WUILabel;
import net.getunik.android.widgets.WUINavigationController;
import net.getunik.android.widgets.WUITabController;
import net.getunik.android.widgets.WUITableView;
import net.getunik.android.widgets.WUITableViewCell;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;

public class MainActivity extends Activity
{
	//< MACC Library Variables
	CHTTPRequestSettings m_tsSettings; 
	
    CTrackStatistics     m_ctsStatistics;
    CContentManager      m_cmContentManager;
    InterfaceMaker       m_imInterfaceMaker;
    boolean              m_bPressedBack = false;
    
    WUITabController     m_uivAddedView  = null;
    RelativeLayout       m_lrMainView    = null;
    
    boolean              m_isUpdating    = false;
    
    //< Application Variables
    public List<String>  m_nsmaToptenPaths = null;
    int                  m_iTopprodukteCategory;
    int                  m_iProductIndex;
    int                  m_iLichtIndex;
    String               m_strSelectedStandby;
    
    CUserLocation m_ulUserLocation = null;
    CHTTPAsyncClient        httpClient = null;
    
    String m_nsstrServerUpdateDate = "";
    String               m_nsstrGetunikUpdateDate = "";
    public static final String PREFS_NAME = "ForestXplorerPrefs";
    
    void setTopprodukteCategory(int iValue) { m_iTopprodukteCategory = iValue; }
    int getTopprodukteCategory() { return m_iTopprodukteCategory;}
    
    void setLightIndex(int iValue) { m_iLichtIndex = iValue; }
    public int  getLightIndex() { return m_iLichtIndex;}
    
    void setSelectedStandby(String strValue) { m_strSelectedStandby = strValue; }
    public String getSelectedStandby() { return m_strSelectedStandby;}
    
    public void addToptenPath(String strPath) { m_nsmaToptenPaths.add(strPath); }
    String getToptenPath(String nsstrAddString)
	{
		String nsstrFullPath = "";
		
	    //< Retrive the app delegate
	    
	    for (int iStep = 0; iStep < m_nsmaToptenPaths.size(); iStep++)
	    {
	        nsstrFullPath += m_nsmaToptenPaths.get(iStep);
	    }
	    
	    nsstrFullPath += nsstrAddString;
	    
	    return nsstrFullPath;
	}
    
    void removeToptenLastObject()
    {
    	if (m_nsmaToptenPaths.size() > 0)
    	{
    		m_nsmaToptenPaths.remove(m_nsmaToptenPaths.size() - 1);
    	}
    }

    int getToptenPathCount()
    {
    	return m_nsmaToptenPaths.size();
    }
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        Intent intent = new Intent("com.google.android.c2dm.intent.REGISTER");
        intent.setPackage("com.getunik.audiotours.forest.tales");
        intent.putExtra("app", PendingIntent.getBroadcast(this, 0, new Intent(), 0));
    	intent.putExtra("sender", "steven.richards-price@forestry.gsi.gov.uk");
    	startService(intent);

    	//< Set the default language
        SharedPreferences settings = this.getSharedPreferences(this.getClass().getPackage().getName(), 0);
        String strLanguage = settings.getString("Language", "");
        String strDebugging = settings.getString("IDDebuggingEnabled", "false");
        if (strLanguage.equals(""))
        {
        	SharedPreferences.Editor editor = settings.edit();
        	editor.putString("Language", "en");
            editor.commit();
        }
        
        m_lrMainView = (RelativeLayout)findViewById(R.id.IDMainView);
        httpClient = new CHTTPAsyncClient();
        if ("false".equals(strDebugging) || null == strDebugging)
        {
        	httpClient.setRequestParameters("admin", "g3tun1k", 10000, "http://admin.tourappmaker.com/foresttale/xml");
        }
        else
        {
        	httpClient.setRequestParameters("admin", "g3tun1k", 10000, "http://admin.tourappmaker.com/foresttale/preview/xml");
        }
        httpClient.addCallbackListener(new MainClassAsyncCallback());
        httpClient.execute("");
    }
    
    /**
     * 
     */
    public void displayNotification(String strTitle, String strDetails)
    {
    	Intent notificationIntent = new Intent(this, MainActivity.class);
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
		
		CNotifications.displayNotifications(m_imInterfaceMaker.getMainContext(), R.drawable.icon, "", strTitle, strDetails, notificationIntent);
    }
    
    IUserLocationCallback m_cCallback = new IUserLocationCallback()
    {
        public void callback()
        {
        	updateDistances();
        }
    };
    
    void updateDistances()
    {   
    	WUITableViewPOIList tvpPOIList = (WUITableViewPOIList) m_imInterfaceMaker.getWidget("IDTVNearbyList");
    	if (null != tvpPOIList)
    	{
    		tvpPOIList.updateDistances(m_ulUserLocation.getCurrentLocation());
    	}
    }
        
    /**
    *
    */
   private void initializeManager()
   {
       //< Create a new request settings object and set all the details that will be required for future tracking
       m_tsSettings = new CHTTPRequestSettings();
       m_tsSettings.setOption(CHTTPRequestSettings.HTTP_USER, "admin");
       m_tsSettings.setOption(CHTTPRequestSettings.HTTP_PASSWORD, "admin");
       m_tsSettings.setOption(CHTTPRequestSettings.REQUEST_TIMEOUT, 10000);
       m_tsSettings.setOption(CHTTPRequestSettings.STATISTIC_URL, "http://macc.java.getunik.net/macc/statistic/");
       m_tsSettings.setOption(CHTTPRequestSettings.CONTENT_URL, "http://macc.java.getunik.net/macc/content/");
       m_tsSettings.setOption(CHTTPRequestSettings.APP_ID, "com.wwf.austria.ratgeber.app");

       GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
       analytics.setLocalDispatchPeriod(1800);

       Tracker tracker = analytics.newTracker("UA-15164922-3"); // Replace with actual tracker/property Id
       tracker.enableExceptionReporting(true);
       tracker.enableAdvertisingIdCollection(true);
       tracker.enableAutoActivityTracking(true);
       m_ctsStatistics = new CTrackStatistics(getBaseContext(), m_tsSettings, CTrackStatistics.Google_Analytics, tracker);

       //< Send statistics details and get new updates if available
       m_cmContentManager = new CContentManager(this, this, "main.xml", m_tsSettings, this.getClass().getPackage().getName());
   }
   
   /**
    * Callback function that is listening to
    */
   private OnClickListener btnSaveOnClick = new OnClickListener()
   { 
       @Override
       public void onClick(View v)
       {
       }
   };

   /**
    *
    */
   private OnItemClickListener listViewItemOnClick = new OnItemClickListener()
   { 
       @Override
       public void onItemClick(AdapterView arg0, View arg1, int position, long arg3)
       {
       }
   };
   
   // manages key presses not handled in other Views from this Activity
   @Override
   public boolean onKeyDown(int keyCode, KeyEvent event)
   {
       if (event.getRepeatCount() == 0)
       {
       if (true == m_uivAddedView.sendKeyDown(keyCode, event))
       {
           m_bPressedBack = false;
           return true;
       }
       else
       {
           if (false == m_bPressedBack)
           {
        	   if (m_imInterfaceMaker.getResourceManager().getSelectedLanguage().equals("cy"))
        	   {
        		   Toast.makeText(this,"Pwyswch y botwm ‘yn ôl’ eto er mwyn cau’r ap.", Toast.LENGTH_LONG).show();
        	   }
        	   else
        	   {
        		   Toast.makeText(this,"Press ‘back’ button again to close the app.", Toast.LENGTH_LONG).show();
        	   }
               m_bPressedBack = true;
               return true;
           }
           else
           {
               return super.onKeyDown(keyCode, event);
           } 
       }
       }
       else
       {
           return true;
       }
       // use this instead if you want to preserve onKeyDown() behavior
       // return super.onKeyDown(keyCode, event);
   }
   
   //< handler for the background updating
   Handler progressHandler = new Handler()
   {
       public void handleMessage(Message msg)
       {
           if (msg.what == 1)
           {
           	displayUpdatesAvailable();
           }
           
           if (msg.what == 2)
           {
        	   WUITableViewCell tvcCell = (WUITableViewCell) m_imInterfaceMaker.getWidget("IDTVCDetailsUpdate");
        	   
               WUILabel uilText = (WUILabel)m_imInterfaceMaker.getWidget("IDLNoMoreUpdates");
           	   if (null != uilText)
           	   {
           		   tvcCell.getCellView().removeView(uilText.getLabel());
           		   uilText.getLabel().setVisibility(View.VISIBLE);
           		   tvcCell.getCellView().addView(uilText.getLabel());
           	   }
           }
           
           m_isUpdating = false;
       }
   };
   
   public void onCHTTPAsyncClienPostExecute()
   {
   }
   
   /**
    * Checking if the application data is up to date in a thread
    *
    * @return none
    */
   void threadIsApplcationDataUpdated()
   {
   	Thread mUpdateDatabase = new Thread()
       {
           @Override
           public void run() {
        	   if (false == m_isUpdating)
        	   {
        		   m_isUpdating = true;
        		   isApplicationDataUpdated();
        	   }
           }
       };
       
       mUpdateDatabase.start();
   }
   
   /**
    * Check if the application data is up to date and if not display a message box
    *
    * @return none
    */
   boolean isApplicationDataUpdated()
   {
	   boolean bIsUpToDate         = false;
       
       //< Read the last update data availabile on the device
       // Restore preferences
       SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
       String nsstrLastUpdateGet = settings.getString("CForestXplorerLastUpdate", "");

       //< Check if the date from our backend is updated
       CHTTPClient httpClientGetunik = new CHTTPClient();
       m_nsstrServerUpdateDate = String.format("%s000", httpClientGetunik.sendGetHttpResponse("fcw", "gimidi66", 10000, "http://admin.guideappmaker.com/fcw/lastmodification"));
       
       if (null == m_nsstrServerUpdateDate)
       {
    	   bIsUpToDate = true;
       }
       else
       {
       if (false == m_nsstrServerUpdateDate.equals(""))
       {
    	   if (nsstrLastUpdateGet.length() == 0)
    	   {
    	       SharedPreferences.Editor editor = settings.edit();
    	       editor.putString("CForestXplorerLastUpdate", m_nsstrServerUpdateDate);
    	          
    	       // Commit the edits!
    	       editor.commit();
    	          
    		   bIsUpToDate = true;
    	   }
    	   else
    	   {
    		   bIsUpToDate = m_nsstrServerUpdateDate.equals(nsstrLastUpdateGet);
    	   }
       }
       }
       
       //< Inform the user that there are updates availabile if any
       if (false == bIsUpToDate)
       {
    	   progressHandler.sendMessage(Message.obtain(progressHandler, 1));
       }
       else
       {    
    	   progressHandler.sendMessage(Message.obtain(progressHandler, 2));
       }
       
   	   return false;
    }
   
   /**
    * Display the updates availabile screen
    *
    * @return none
    */
   void displayUpdatesAvailable()
   {
	   final RelativeLayout uivView = new RelativeLayout(m_imInterfaceMaker.getMainContext());
   	   uivView.setBackgroundColor(Color.argb(150, 0, 0, 0));
   	   RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
       //< Set the layout
       uivView.setLayoutParams(lp);
       
       ImageView uivViewImage = new ImageView(m_imInterfaceMaker.getMainContext());
       uivViewImage.setImageResource(R.drawable.ua_background);
       uivViewImage.setAdjustViewBounds(true);
       lp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, (int)(200 * m_imInterfaceMaker.m_fDensityScale));
       lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
       lp.setMargins(0, 0, 0, -2);
       //< Set the layout
       uivViewImage.setLayoutParams(lp);
       uivView.addView(uivViewImage);
       
       TextView uilLabel = new TextView(m_imInterfaceMaker.getMainContext());
       if (m_imInterfaceMaker.getResourceManager().getSelectedLanguage().equals("cy"))
       {
    	   uilLabel.setText("Mae cynnwys newydd ar gael i�w lwytho i lawr. Ydych chi am ei lwytho i lawr yn awr?");
       }
       else
       {
    	   uilLabel.setText("New content is available for download. Do you want to download it now ?");
       }
       lp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
       lp.setMargins((int)(10 * m_imInterfaceMaker.m_fDensityScale), 0, (int)(10 * m_imInterfaceMaker.m_fDensityScale), (int)(110 * m_imInterfaceMaker.m_fDensityScale));
       lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
       uilLabel.setLayoutParams(lp);
       uivView.addView(uilLabel);
       
       Button uibButtonOk = new Button(m_imInterfaceMaker.getMainContext());
       if (m_imInterfaceMaker.getResourceManager().getSelectedLanguage().equals("cy"))
       {
    	   uibButtonOk.setText("Ydw");
       }
       else
       {
    	   uibButtonOk.setText("Yes");
       }
       uibButtonOk.setTextColor(Color.WHITE);
       uibButtonOk.setBackgroundResource(R.drawable.ua_button);
       lp = new RelativeLayout.LayoutParams((int)(128 * m_imInterfaceMaker.m_fDensityScale), (int)(44 * m_imInterfaceMaker.m_fDensityScale));
       lp.setMargins((int)(20 * m_imInterfaceMaker.m_fDensityScale), 0, 0, (int)(60 * m_imInterfaceMaker.m_fDensityScale));
       lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
       //< Set the layout
       uibButtonOk.setLayoutParams(lp);
       uibButtonOk.setOnClickListener(new OnClickListener()
       {
           public void onClick(View v)
           {
           	 //< Go to the 3d tab screen
        	 m_uivAddedView.setCurrentTab(1);
               
               //< Show the updating screen and start downloading
           	//< Get the main navigation controller
               WUINavigationController uincNavicationController = (WUINavigationController)m_imInterfaceMaker.getWidget("IDNCSettingsNavigation");
               
               //< Jump to root controller to handle any exceptions
               uincNavicationController.popToRoot();
               uincNavicationController.subControllerCallback("IDCPUpdates", 0);
               
               //< Start the download
               startDownloading();
               
               //< Remove the glass view
               m_lrMainView.removeView(uivView);

           }
         });
       
       uivView.addView(uibButtonOk);
       
       //< Create a new text label view
       Button uibButtonCancel = new Button(m_imInterfaceMaker.getMainContext());
       if (m_imInterfaceMaker.getResourceManager().getSelectedLanguage().equals("cy"))
       {
    	   uibButtonCancel.setText("Nac ydw");
       }
       else
       {
    	   uibButtonCancel.setText("No");
       }
       uibButtonCancel.setTextColor(Color.WHITE);
       uibButtonCancel.setBackgroundResource(R.drawable.ua_button);
       lp = new RelativeLayout.LayoutParams((int)(128 * m_imInterfaceMaker.m_fDensityScale), (int)(44 * m_imInterfaceMaker.m_fDensityScale));
       lp.setMargins(0, 0, (int)(20 * m_imInterfaceMaker.m_fDensityScale), (int)(60 * m_imInterfaceMaker.m_fDensityScale));
       lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
       lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
       //< Set the layout
       uibButtonCancel.setLayoutParams(lp);
       uibButtonCancel.setOnClickListener(new OnClickListener()
       {
           public void onClick(View v)
           {
           	m_lrMainView.removeView(uivView);
           }
         });
       
       uivView.addView(uibButtonCancel);
       
       uivView.setOnClickListener(new OnClickListener(){

           @Override
           public void onClick(View v)
           {            	
           }
       });
       
       m_lrMainView.addView(uivView);
   }
   
   /**
   *
   */
  void startDownloading()
  {
	  WUITableViewCell uitvcUpdateCell = (WUITableViewCell)m_imInterfaceMaker.getWidget("IDTVCGuideData");
	  uitvcUpdateCell.loadContent();
	  uitvcUpdateCell.getCellView().setVisibility(View.VISIBLE);
	  
	  WUITableView uitvUpdates = (WUITableView) m_imInterfaceMaker.getWidget("IDTVUpdates");
	  uitvUpdates.reloadData();
	  
	  WUIGuideDataDownloadingView uivGuideDataUpdating  = (WUIGuideDataDownloadingView)m_imInterfaceMaker.getWidget("IDVGuideData");
	  uivGuideDataUpdating.resetProgressBar();
	  
	  uivGuideDataUpdating.startUpdating(m_imInterfaceMaker);
  }
  
  /**
   * Complete the updating. Reload all data.
   *
   * @return none
   */
  void completeUpdate(boolean bCompleted)
  {
      if (true == bCompleted)
      {
   	      //< Reload content
	      m_lrMainView.removeAllViews();
	      m_uivAddedView = (WUITabController) m_imInterfaceMaker.getComponent("//*[@id='IDTCMainTabController']", null);
	      m_lrMainView = (RelativeLayout)findViewById(R.id.IDMainView);
	      m_lrMainView.addView(m_uivAddedView.getView());
          
          SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
          SharedPreferences.Editor editor = settings.edit();
          editor.putString("CForestXplorerLastUpdate", m_nsstrServerUpdateDate);
          
          // Commit the edits!
          editor.commit();
      }
      
      //< Update the last update displayed text
      WUILastUpdateLabel uilLabel = (WUILastUpdateLabel)m_imInterfaceMaker.getWidget("IDLLastUpdate");
      uilLabel.updateDisplayedText();
  }
  
  public void onMainActivityCHTTPAsyncClienPostExecute()
  {
  	String strContentXMLData = httpClient.getResponse();

  		//< Write the content to a local file
      try
      {   
          if (null != strContentXMLData)
          {
              FileOutputStream fOut  = openFileOutput(String.format("%s.rss", Integer.toString("http://admin.tourappmaker.com/foresttale/xml".hashCode())), Context.MODE_WORLD_READABLE); 
              OutputStreamWriter osw = new OutputStreamWriter(fOut, "UTF8");

              //< Write the string to the file
              osw.write(strContentXMLData); 
              //< Ensure that everything is really written out and close
              osw.flush(); 
              osw.close();
          }
      }
      catch (IOException ioe)
      { 
          ioe.printStackTrace(); 
      }
      
    //< Initialize our content manager
      initializeManager();

      //< Load the default content, or the cached one
      m_imInterfaceMaker = m_cmContentManager.loadContent(btnSaveOnClick, listViewItemOnClick);
      m_imInterfaceMaker.setTrackingObject(m_ctsStatistics);
      m_uivAddedView = (WUITabController) m_imInterfaceMaker.getComponent("//*[@id='IDTCMainTabController']", null);
      
      m_lrMainView.addView(m_uivAddedView.getView());

      m_ulUserLocation = new CUserLocation(m_imInterfaceMaker.getMainContext(), m_cCallback, 50000, 100.0f);
  }
  
  class MainClassAsyncCallback implements IPublicAsyncClientCallback
  {
		@Override
		public void onCHTTPAsyncClientPostExecute()
		{
			onMainActivityCHTTPAsyncClienPostExecute();
		}
  };
}