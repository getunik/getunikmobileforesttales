package com.getunik.audiotours.forest.tales;
import net.getunik.android.widgets.WUIWebView;

public class WUIWebViewPOIDetails extends WUIWebView
{ 
	/**
     * Load the property called text.
     *
     * @param cxmlAttributeNode XML node that contains the data for this attribute.
     * @return none
     * @note This function can be overwritten if we want to load a different data
     */
    public void loadAttributeText(String strAttribute)
    {
    	if (null != strAttribute)
    	{
    		String nsstrText = m_rmResourcesManager.getStrAttributeValue(strAttribute, "", m_iWidgetIndex);
    		String nsstrHTML = String.format("<html><head><style>@font-face {font-family: Vogue; src: url(file:///android_asset/fonts/voguen.ttf);} @font-face {font-family: Vogue; src: url(file:///android_asset/fonts/vogueb.ttf); font-weight: bold;}       body {}</style></head><body>%s</body></html>", nsstrText);
            m_uivView.loadDataWithBaseURL("file:///android_asset/", nsstrHTML, "text/html", "UTF-8", null);
    	}
    }
}