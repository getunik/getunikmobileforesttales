package com.getunik.audiotours.forest.tales;

import org.dom4j.Element;

import net.getunik.android.core.InterfaceMaker;
import net.getunik.android.resources.CResourceManager;
import net.getunik.android.widgets.WGoogleMapsPOI;

public class WGoogleMapsPOICustom extends WGoogleMapsPOI
{
	int[] m_nsmaActivities = new int[100];
	boolean m_bIsWindfarmConstruction = false;
	///
    ///
    ///
    public Object initWithXMLNode(Element cxmlNode, CResourceManager rmResourcesManager, int iIndex, InterfaceMaker ccCore)
    {
        super.initWithXMLNode(cxmlNode, rmResourcesManager, iIndex, ccCore);
        
        //< Read and store all the activities that this POI can have
        int iActivities = m_rmResourcesManager.getIntAttributeValue("[@DataXMLList]./activity", 0, iIndex);
        for (int iStep = 0; iStep < iActivities; iStep++)
        {
            String nsstrActivityID = m_rmResourcesManager.getStrAttributeValue(String.format("[@DataXMLList]./activity[%d]", iStep + 1), "", iIndex);
            m_nsmaActivities[iStep] = Integer.parseInt(nsstrActivityID);
        }
        
        //< Check if the POI is under windfarm construction
        m_bIsWindfarmConstruction = false;
        String nsstrWindfar = m_rmResourcesManager.getStrAttributeValue("[@DataXMLList]./windfarm_construction", "", iIndex);
        
        if (null != nsstrWindfar)
        {
        	if (nsstrWindfar.length() > 0)
        	{
        		if (Integer.parseInt(nsstrWindfar) == 1)
        		{
        			m_bIsWindfarmConstruction = true;
        		}
            }
        }
        
        return this;
    }
    
    /**
     * Check if a specific activity is listed by this POI
     *
     * @param iActivityID Id of the activity that the user has selected
     * @return int category value
     */
    boolean isActivityListed(int iActivityID)
    {
        boolean bReturnVal = false;
        
        for (int iStep = 0; iStep < 100; iStep++)
        {
            if (m_nsmaActivities[iStep] == iActivityID)
            {
                bReturnVal = true;
            }
        }
        
        //< The -1 value represents the all activities
        if (-1 == iActivityID)
        {
            bReturnVal = true;
        }
        
        return bReturnVal;
    }
    
    /**
     * Is the windfar under construction or not.
     *
     * @return true if windfarm is underconstruction
     */
    public boolean isWindfarmConstruction()
    {
        return m_bIsWindfarmConstruction;
    }
}