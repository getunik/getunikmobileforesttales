package com.getunik.audiotours.forest.tales;

import org.dom4j.Element;
import net.getunik.android.core.InterfaceMaker;
import net.getunik.android.resources.CResourceManager;
import net.getunik.android.resources.RNumber;
import net.getunik.android.widgets.WUITableView;

public class WUITableViewForestTales extends WUITableView
{ 
	/**
     * Send an callback event to our parent widgets informing about the new action.
     * 
     * @param nsstrSenderID Sender ID
     * @param nsnIndex Index number of the widget that is calling the callback
     * @return none
     */
    public void sendCallbackEvent(String nsstrSenderID, int nsnIndex)
    {
    	if (nsstrSenderID.equals("IDNCPTalesPlace"))
        {
            RNumber rnSelectedAccessPoint = (RNumber)m_rmResourcesManager.getResource("[@RNAccessPoint]");
            rnSelectedAccessPoint.setValue(nsnIndex + 1);
            
            //< Check if we have to send a tracking message
            String strTitle   = m_rmResourcesManager.getStrAttributeValue("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/title_of_the_poi", "", 0);
            String strTitleAP = m_rmResourcesManager.getStrAttributeValue("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/title", "", 0);
            
            String strName = String.format("/iPhone/%s/%s/%s", m_rmResourcesManager.getSelectedLanguage(), strTitle, strTitleAP);
            m_cCore.getTrackObject().sendTrackDetails(strName);
        }
        
    	super.sendCallbackEvent(nsstrSenderID, nsnIndex);
    }
}