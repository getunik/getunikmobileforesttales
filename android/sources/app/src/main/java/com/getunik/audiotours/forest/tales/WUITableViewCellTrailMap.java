package com.getunik.audiotours.forest.tales;

import org.dom4j.Element;

import android.view.View;
import net.getunik.android.core.InterfaceMaker;
import net.getunik.android.resources.CResourceManager;
import net.getunik.android.widgets.WUITableViewCell;

public class WUITableViewCellTrailMap extends WUITableViewCell
{ 
	/**
	 * Initialize our widget with the informations that are coming from an XML node. The Node must be a valid macc node item.
	 * 
	 * @param cxmlNode XML node that contains all the data.
	 * @param rmResourcesManager Pointer to an resource manager that contain real values. It can be null in some cases.
	 * @param iIndex If this item comes from an group with multiple items, this will specify the index of the item inside the list.
	 * @param ccCore Pointer to the widget and resource creator.
	 * @return id of the created item.
	 */
	public Object initWithXMLNode(Element cxmlNode, CResourceManager rmResourcesManager, int iIndex, InterfaceMaker ccCore)
    {
        super.initWithXMLNode(cxmlNode, rmResourcesManager, iIndex, ccCore);
	    
	    String nsstrTrailMapPath = m_rmResourcesManager.getStrAttributeValue(String.format("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/access_points/access_point[[@RNAccessPoint]]/trails/trail[%d]/map_of_trail", iIndex + 1), "", 0);
	    
	    if (null != nsstrTrailMapPath)
	    {
	    	if (nsstrTrailMapPath.length() == 0)
		    {
		    	getCellView().setVisibility(View.INVISIBLE);
		    }
	    }
	    else
	    {
	    	getCellView().setVisibility(View.INVISIBLE);
	    }
	    
	    return this;
	}
}