package com.getunik.audiotours.forest.tales;

import android.widget.Toast;
import net.getunik.android.widgets.WUIButton;
import net.getunik.android.widgets.WUILabel;

public class WUIButtonUpdating extends WUIButton
{
	boolean m_bVerifyState = true;
    
	/**
     * Send an callback event to our parent widgets informing about the new action.
     * 
     * @param nsstrSenderID Sender ID
     * @param nsnIndex Index number of the widget that is calling the callback
     * @return none
     */
    public void sendCallbackEvent(String nsstrSenderID, int nsnIndex)
    {
    	Toast.makeText(m_cCore.getMainContext(),"Checking for available updates. Please wait.", Toast.LENGTH_SHORT).show();
    	if (m_bVerifyState == true)
        {
    		
//    		((MainActivity)m_cCore.m_Activity).threadIsApplcationDataUpdated();
        }
        else
        {
        	//< Cancel the download	
//          m_appDelegate.m_bStopUpdating = true;
        }
    };

    /**
     * This button can have 2 states. Enable the verify of updates state and cancel downloading state
     */
    void toogleVerifyState(boolean bVerifyState)
    {
        m_bVerifyState = bVerifyState;
        
        if (false == m_bVerifyState)
        {
//            [self setTitle:NSLocalizedString(@"update_abort_button", @"update_abort_button")];
        }
        else
        {
//            [self setTitle:NSLocalizedString(@"update_test_now_button", @"update_test_now_button")];
        }
    }
}