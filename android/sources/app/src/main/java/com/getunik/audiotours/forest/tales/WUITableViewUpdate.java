package com.getunik.audiotours.forest.tales;

import org.dom4j.Element;

import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.widget.Toast;
import net.getunik.android.core.InterfaceMaker;
import net.getunik.android.resources.CResourceManager;
import net.getunik.android.widgets.WUILabel;
import net.getunik.android.widgets.WUITableView;

public class WUITableViewUpdate extends WUITableView
{
	/**
     * {@inheritDoc}
     */
    public Object initWithXMLNode(Element cxmlNode, CResourceManager rmResourcesManager, int iIndex, InterfaceMaker ccCore)
    {        
        super.initWithXMLNode(cxmlNode, rmResourcesManager, iIndex, ccCore);
        
        GradientDrawable gradientSelector;
        gradientSelector = new GradientDrawable (GradientDrawable.Orientation.TOP_BOTTOM, new int[] { 0x00000000, 0x00000000});
        gradientSelector.setShape(GradientDrawable.LINEAR_GRADIENT);
        gradientSelector.setAlpha(0);
        gradientSelector.setGradientRadius((float)(Math.sqrt(2) * 60));
    	
        m_uitvTableView.getTableView().setSelector(gradientSelector);
        
        return this;
    }
    
	/**
     * Send an callback event to our parent widgets informing about the new action.
     * 
     * @param nsstrSenderID Sender ID
     * @param nsnIndex Index number of the widget that is calling the callback
     * @return none
     */
    public void sendCallbackEvent(String nsstrSenderID, int nsnIndex)
    {
    	if (nsstrSenderID.equals("IDCheckForUpdates"))
    	{
    		WUILabel uilText = (WUILabel)m_cCore.getWidget("IDLNoMoreUpdates");
        	if (null != uilText)
        	{
        		uilText.getLabel().setVisibility(View.INVISIBLE);
        	}
        	   
    		Toast.makeText(m_cCore.getMainContext(),"Checking for available updates.", Toast.LENGTH_SHORT).show();
    		((MainActivity)m_cCore.m_Activity).threadIsApplcationDataUpdated();
    	}
		
    	return;
    }
}