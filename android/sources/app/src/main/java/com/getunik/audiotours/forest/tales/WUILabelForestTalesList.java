package com.getunik.audiotours.forest.tales;

import org.dom4j.Element;

import net.getunik.android.core.InterfaceMaker;
import net.getunik.android.resources.CResourceManager;
import net.getunik.android.widgets.WUILabel;

public class WUILabelForestTalesList extends WUILabel
{
	/**
	 * Load the property called text.
	 *
	 * @param cxmlAttributeNode XML node that contains the data for this attribute.
	 * @return none
	 * @note This function can be overwritten if we want to load a different data
	 */
	public String loadAttributeText(Element cxmlAttributeNode)
	{
		String nsstrFormat = null;
	    String nsstrText   = null;
	    
	    //< Set the displayed text
	    if (null != cxmlAttributeNode)
	    {
	        nsstrText = m_rmResourcesManager.getStrAttributeValue(String.format("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[%d]/title", m_iWidgetIndex + 1), "", 0);
	        nsstrFormat = String.format("%s", nsstrText);
	    }
	    
	    return nsstrFormat;
	}
}