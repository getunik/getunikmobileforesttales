package com.getunik.audiotours.forest.tales;
import android.graphics.Color;
import android.location.Location;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ListView;
import net.getunik.android.resources.RColor;
import net.getunik.android.widgets.WUITableView;
import net.getunik.android.widgets.WUITableViewSection;

public class WUITableViewPOIList extends WUITableView
{
	int iCurrentActivity = -1;
	Location m_lUserLocation;
	/**
	 * Fill the table view
	 *
	 * @return none
	 */
	public void fillTableView(int iSelectedActivity)
	{
		iCurrentActivity = iSelectedActivity;
		
		m_uitvTableView.getTableView().setDivider(null);
		m_uitvTableView.getTableView().setDividerHeight(0);
		
	    WUITableViewCellPOIList cell = null;
	    if (null != m_nsmaTableSections)
	    {
	    WUITableViewSection uitvsSection = m_nsmaTableSections.get(0);
	    
	    uitvsSection.sort();
	    
	    int iSize = uitvsSection.getCellCount();
	    int iBackgroundCount = 0;
	    for (int iStep = 0; iStep < iSize; iStep++)
	    {
	        cell = (WUITableViewCellPOIList)uitvsSection.getCellAtIndex(iStep);
	        if (false == cell.isActivityListed(iCurrentActivity))
	        {
	        	cell.getCellView().setVisibility(View.GONE);
	        }
	        else
	        {
	            if (iBackgroundCount % 2 == 0)
	            {
	            	cell.setBackgroundColor(hexRGBAColorToInt("0xffffffff"));
	            }
	            else
	            {
	            	cell.setBackgroundColor(hexRGBAColorToInt("0xeeeeeeff"));
	            }
	            iBackgroundCount++;
	            cell.getCellView().setVisibility(View.VISIBLE);
	        }
	    }
	    
		m_uitvTableView.reloadData();
	    }
	}
	
	public void updateMetrics()
	{
		updateDistances(m_lUserLocation);
	}
	public void updateDistances(Location lUserLocation)
	{
		m_lUserLocation = lUserLocation;
		if (null != m_nsmaTableSections)
		{
		WUITableViewSection uitvsSelectionList = m_nsmaTableSections.get(0);
		
		int iRows = uitvsSelectionList.getCellCount();
		for (int iStep = 0; iStep < iRows; iStep++)
		{
			WUITableViewCellPOIList tvcCell = (WUITableViewCellPOIList) uitvsSelectionList.getCellAtIndex(iStep);
			tvcCell.updateDistance(lUserLocation);
		}
		
		fillTableView(iCurrentActivity);
		}
	}
	
	public int hexRGBAColorToInt(String strColor)
    {
        int iColor = Color.argb(255, 0, 0, 0);
        if (null != strColor)
        {
            iColor = Color.argb(Integer.parseInt(strColor.substring(8, 10), 16), 
                                Integer.parseInt(strColor.substring(2, 4), 16), 
                                Integer.parseInt(strColor.substring(4, 6), 16), 
                                Integer.parseInt(strColor.substring(6, 8), 16));
        }
        
        return iColor;
    }
	
	/**
     * Send an callback event to our parent widgets informing about the new action.
     * 
     * @param nsstrSenderID Sender ID
     * @param nsnIndex Index number of the widget that is calling the callback
     * @return none
     */
    public void sendCallbackEvent(String nsstrSenderID, int nsnIndex)
    {
    	//< Check if we have to send a tracking message
        String strName = String.format("/android/%s/%s", m_rmResourcesManager.getSelectedLanguage(), m_rmResourcesManager.getStrAttributeValue("[@DataXMLList]./title_of_the_poi", "", nsnIndex));
        m_cCore.getTrackObject().sendTrackDetails(strName);
        
    	super.sendCallbackEvent(nsstrSenderID, nsnIndex);
    }
}