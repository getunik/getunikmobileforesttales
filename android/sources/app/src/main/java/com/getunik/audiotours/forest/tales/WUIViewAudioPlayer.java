package com.getunik.audiotours.forest.tales;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import net.getunik.android.utils.CContentDownloader;
import net.getunik.android.utils.CToolbox;
import net.getunik.android.utils.CContentDownloader.IDownloaderCallback;
import net.getunik.android.widgets.WUIButton;
import net.getunik.android.widgets.WUILabel;
import net.getunik.android.widgets.WUITableView;
import net.getunik.android.widgets.WUITableViewCell;
import net.getunik.android.widgets.WUIView;

public class WUIViewAudioPlayer extends WUIView implements IDownloaderCallback
{
	CContentDownloader m_cdDownloader = null;
	String m_strFilePath = null;
	MediaPlayer m_avpAudioPlayer = null;
	private SeekBar m_uipvAudioProgress = null;
	Timer m_timerUpdadeUI = null;
	
	WUITableViewCell m_wuitvcSpaceCell = null;

	/**
	 *
	 */
	void initializePlayerWithFile(String nsstrPath, WUITableViewCell spaceCell)
	{
		m_wuitvcSpaceCell = spaceCell;
		if (null == m_avpAudioPlayer)
		{
		if ((null != nsstrPath) && (nsstrPath.length() > 0))
	    {
			//< Make the view visible
			getView().setVisibility(View.VISIBLE);

			WUILabel uilLabel = (WUILabel)m_cCore.getWidget("IDLAudioIsDownloading");
			uilLabel.getLabel().setVisibility(View.VISIBLE);

			WUIButton uibControls = (WUIButton)m_cCore.getWidget("IDBAudioControl");
			uibControls.getButton().setVisibility(View.INVISIBLE);
	    
			WUIButton m_uibControls = (WUIButton)m_cCore.getWidget("IDBAudioControl");
			m_uibControls.setDisplayResource("[@IDIAudioPlayerPause]");

			if (null == m_uipvAudioProgress)
			{
				m_uipvAudioProgress = new SeekBar(m_cCore.getMainContext());
		        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, (int)(30 * m_cCore.m_fDensityScale));
		        lp.setMargins(60, (int)(44 * m_cCore.m_fDensityScale), 60, 0);
		        //< Set the layout
		        m_uipvAudioProgress.setLayoutParams(lp);
		        m_uipvAudioProgress.setVisibility(View.INVISIBLE);

		        getView().addView(m_uipvAudioProgress);
		        
		        m_uipvAudioProgress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
		        {
		        	boolean bTracking = false;
		        	
		            @Override
		            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
		            {
		            	if (true == bTracking)
		            	{
		            		m_avpAudioPlayer.seekTo(progress * 1000);
		            	}
		            }

		            @Override
		            public void onStartTrackingTouch(SeekBar seekBar)
		            {
		            	bTracking = true;
		            }

		            @Override
		            public void onStopTrackingTouch(SeekBar seekBar)
		            {
		            	bTracking = false;
		            }
		        });
			}	

			m_strFilePath = nsstrPath;
			if (false == CToolbox.fileExistsOnSDCard("/ForestTales", Integer.toString(m_strFilePath.hashCode()) + "." + "mp3"))
			{
				m_cdDownloader = new CContentDownloader(m_cCore.getMainContext(), (IDownloaderCallback) this);
				m_cdDownloader.addContentPackage("Information", "Audio File is downloading. Please wait...");
				m_cdDownloader.addContentFile(nsstrPath, "", "", "/ForestTales", Integer.toString(m_strFilePath.hashCode()) + "." + "mp3", false);
				m_cdDownloader.startDownloading();
			}
			else
			{
				m_uipvAudioProgress.setVisibility(View.VISIBLE);
				playAudioFile();
			}
	    }
		}
	}
	
	public void loadingCompleted()
    {
		//< There was an error while downloading the application data
    	if (CToolbox.ERROR_NONE != m_cdDownloader.getDownloadingStatus())
    	{
    		AlertDialog alertDialog;
    		alertDialog = new AlertDialog.Builder(m_cCore.getMainContext()).create();
    		alertDialog.setCancelable(false);
    		
    		switch (m_cdDownloader.getDownloadingStatus())
    		{
    			case CToolbox.ERROR_EXTRACTING_ZIP_FILE:
    			case CToolbox.ERROR_CREATING_FILE:
    			case CToolbox.ERROR_UNABLE_TO_WRITE_TO_SD:
    				alertDialog.setTitle("Externe Speicherkarte wurde nicht gefunden!");
    				alertDialog.setMessage("Diese Applikation benötigt ca. 240 MB an zusätzlichen Daten welche auf die SD Card herunter geladen werden. Bitte stellen Sie sicher dass eine Speicherkarte mit genügend freiem Platz zur Verfügung steht.");
    			break;
    			case CToolbox.ERROR_DOWNLOADING:
    				alertDialog.setTitle("Keine oder abgebrochene Internetverbindung!");
    				alertDialog.setMessage("Bitte stellen Sie sicher dass Ihr Gerät via Wi-Fi oder 3G mit dem Internet verbunden ist. Die Datenmenge beträgt ca. 240 MB.");
    			break;
    			default:
    			break;
    		}

    		alertDialog.setButton("Beenden", new DialogInterface.OnClickListener()
    		{
    		    @Override
    		    public void onClick(DialogInterface dialog, int which)
    		    {
    		        dialog.dismiss();
    		    }  
    		});
    		alertDialog.show();
    	}
    	else
    	{
    		m_uipvAudioProgress.setVisibility(View.VISIBLE);
    		playAudioFile();
    	}
    }

	private void TimerMethod()
	{
	    //This method is called directly by the timer
	    //and runs in the same thread as the timer.

	    //We call the method that will work with the UI
	    //through the runOnUiThread method.
	    m_cCore.m_Activity.runOnUiThread(Timer_Tick);
	}

	private Runnable Timer_Tick = new Runnable() {
	    public void run()
	    {
	    	if (null != m_avpAudioPlayer)
	    	{
	    		int iCurrentPosition = m_avpAudioPlayer.getCurrentPosition() / 1000;
	    		int iDurationLeft    = m_avpAudioPlayer.getDuration() /  1000;
	    	
	    		WUILabel uilLabel = (WUILabel)m_cCore.getWidget("IDLTimeSpent");
	    		uilLabel.setText(String.format("%02d:%02d", iCurrentPosition / 60, iCurrentPosition % 60));

	    		uilLabel = (WUILabel)m_cCore.getWidget("IDLTimeLeft");
	    		uilLabel.setText(String.format("%02d:%02d", iDurationLeft / 60, iDurationLeft % 60));
		    
	    		m_uipvAudioProgress.setMax(iDurationLeft);
	    		m_uipvAudioProgress.setProgress(iCurrentPosition);
	    	}
	    }
	};

	/**
	 * 
	 */
	void updatePlayerProgressControl()
	{
/*	    if (m_timerUpdadeUI) 
	    {
			[m_timerUpdadeUI invalidate];
	        m_timerUpdadeUI = nil;
	    }
	    
		if (m_avpAudioPlayer.playing)
		{
	        m_timerUpdadeUI = [NSTimer scheduledTimerWithTimeInterval:.01 target:self selector:@selector(updatePlayerProgressControl) userInfo:nil repeats:YES];
			m_uipvAudioProgress.value = m_avpAudioPlayer.currentTime;
	        
		}
		
	    WUILabel *uilLabel = (WUILabel*)[m_cCore getWidget:@"IDLTimeSpent"];
	    [uilLabel setText:[NSString stringWithFormat:@"%d:%02d", (int)m_avpAudioPlayer.currentTime / 60, (int) m_avpAudioPlayer.currentTime % 60]];
	    
	    uilLabel = (WUILabel*)[m_cCore getWidget:@"IDLTimeLeft"];
	    [uilLabel setText:[NSString stringWithFormat:@"%d:%02d", (int)m_avpAudioPlayer.duration / 60, (int)m_avpAudioPlayer.duration % 60]];*/
	}

	/**
	 *
	 */
	void playAudioFile()
	{
		//< Remove the download text and display a progressbar
	    WUILabel uilLabel = (WUILabel)m_cCore.getWidget("IDLAudioIsDownloading");
	    uilLabel.getLabel().setVisibility(View.INVISIBLE);

		File sdDir = new File(Environment.getExternalStorageDirectory().getPath());
		String strFilePath = sdDir.getAbsolutePath() + "/ForestTales" + "/" + Integer.toString(m_strFilePath.hashCode()) + "." + "mp3";

		//< No error found, start playing
		m_avpAudioPlayer = MediaPlayer.create(m_cCore.m_Activity, Uri.parse(strFilePath));
		m_avpAudioPlayer.start();

	    WUIButton uibControls = (WUIButton)m_cCore.getWidget("IDBAudioControl");
	    uibControls.getButton().setVisibility(View.VISIBLE);

	    if (null != m_timerUpdadeUI)
	    {
	    	m_timerUpdadeUI.cancel();
	    }

	    m_timerUpdadeUI = new Timer();
	    m_timerUpdadeUI.schedule(new TimerTask()
	    {
	        @Override
	        public void run()
	        {
	        	TimerMethod();
	        }
	    }, 0, 1000);
	}

	void pausePlayback()
	{
	    //< Update displayed icon
	    WUIButton m_uibControls = (WUIButton)m_cCore.getWidget("IDBAudioControl");
	    m_uibControls.setDisplayResource("[@IDIAudioPlayerPlay]");

	    if (null != m_avpAudioPlayer)
	    {
	    	m_avpAudioPlayer.pause();
	    }
	}
	
	void stopPlayback()
	{
	    //< Update displayed icon
	    WUIButton m_uibControls = (WUIButton)m_cCore.getWidget("IDBAudioControl");
	    m_uibControls.setDisplayResource("[@IDIAudioPlayerPlay]");

	    if (null != m_avpAudioPlayer)
	    {
	    	m_timerUpdadeUI.cancel();
	    	m_timerUpdadeUI = null;
	    	m_avpAudioPlayer.stop();
	    	m_avpAudioPlayer = null;
	    }
	}

	void startPlayback()
	{
	    //< Update displayed icon
	    WUIButton m_uibControls = (WUIButton)m_cCore.getWidget("IDBAudioControl");
	    m_uibControls.setDisplayResource("[@IDIAudioPlayerPause]");

	    if (null != m_avpAudioPlayer)
	    {
	    	m_avpAudioPlayer.start();
	    }
	}

	/**
	 * Send an callback event to our parent widgets informing about the new action.
	 *
	 * @param nsstrSenderID Sender ID
	 * @param nsnIndex Index number of the widget that is calling the callback
	 * @param nsnActionType Specific action that was triggered
	 * @return None
	 **/
	public void sendCallbackEvent(String nsstrSenderID, int nsnIndex)
	{
	    boolean bHandled = false;
	    
	    if ("IDBAudioControl".equals(nsstrSenderID))
	    {
	        if (m_avpAudioPlayer.isPlaying())
	        {
	        	pausePlayback();
	        }
	        else
	        {
	        	startPlayback();
	        }
	        
	        bHandled = true;
	    }
	    
	    if ("IDBAudioClose".equals(nsstrSenderID))
	    {
	    	stopPlayback();
	    	getView().setVisibility(View.INVISIBLE);
	    	if (null != m_wuitvcSpaceCell)
	    	{
	    		m_wuitvcSpaceCell.getCellView().setVisibility(View.INVISIBLE);
	    	}
	        
	        bHandled = true;
	    }
	    
	    if (false == bHandled)
	    {
//	        super.sendCallbackEvent(nsstrSenderID, nsnIndex);
	    }
	}
}