package com.getunik.audiotours.forest.tales;

import java.io.File;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.widget.Toast;
import net.getunik.android.resources.RString;
import net.getunik.android.utils.CToolbox;
import net.getunik.android.widgets.WUINavigationControllerPage;

public class WUINavigationControllerPageTrails extends WUINavigationControllerPage
{
	/**
     * Send an callback event to our parent widgets informing about the new action.
     * 
     * @param nsstrSenderID Sender ID
     * @param nsnIndex Index number of the widget that is calling the callback
     * @return none
     */
    public void sendCallbackEvent(String nsstrSenderID, int nsnIndex)
    {
	    boolean bHandled = false;
	    
	    if (nsstrSenderID.equals("IDNCPTrailMapPDF"))
	    {        
	        String  nsstrTrailMapPath = m_rmResourcesManager.getStrAttributeValue(String.format("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/access_points/access_point[[@RNAccessPoint]]/trails/trail[%d]/map_of_trail", nsnIndex + 1), "", 0);
	        RString rstrTrailPath     = (RString)m_rmResourcesManager.getResource("[@IDSTrailMapPath]");
	        rstrTrailPath.setValue(nsstrTrailMapPath);
	        
	        if (m_rmResourcesManager.getSelectedLanguage().equals("cy"))
	        {
	        	showPDFDocument("Yn llwytho map llwybrau i lawr. Disgwyliwch.", String.format("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/access_points/access_point[[@RNAccessPoint]]/trails/trail[%d]/map_of_trail", nsnIndex + 1));
	        }
	        else
	        {
	        	showPDFDocument("Downloading Trail Map. Please wait.", String.format("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/access_points/access_point[[@RNAccessPoint]]/trails/trail[%d]/map_of_trail", nsnIndex + 1));
	        }
	        //< Send the callback action
//	        super.sendCallbackEvent("IDNCPTrailMapPDF", nsnIndex);
	        
	        bHandled = true;
	    }
	    
	    if (nsstrSenderID.equals("IDNCPTrailAerial"))
	    {        
	        String nsstrTrailMapPath = m_rmResourcesManager.getStrAttributeValue(String.format("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/access_points/access_point[[@RNAccessPoint]]/trails/trail[%d]/ap_of_trail", nsnIndex + 1), "", 0);
	        RString rstrTrailPath    = (RString)m_rmResourcesManager.getResource("[@IDSTrailMapPath]");
	        rstrTrailPath.setValue(nsstrTrailMapPath);
	        
	        if (m_rmResourcesManager.getSelectedLanguage().equals("cy"))
	        {
	        	showPDFDocument("Yn llwytho awyrlun i lawr. Disgwyliwch.", String.format("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/access_points/access_point[[@RNAccessPoint]]/trails/trail[%d]/ap_of_trail", nsnIndex + 1));
	        }
	        else
	        {
	        	showPDFDocument("Downloading Aerial Photo. Please wait.", String.format("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/access_points/access_point[[@RNAccessPoint]]/trails/trail[%d]/ap_of_trail", nsnIndex + 1));
	        }
	        
	        //< Send the callback action
//	        super.sendCallbackEvent("IDNCPTrailAerial", nsnIndex);
	        
	        bHandled = true;
	    }

	    if (false == bHandled)
	    {
	        super.sendCallbackEvent(nsstrSenderID, nsnIndex);
	    }
	}
    
    /**
     * Download and display a pdf document in an external pdf viewer
     * @param strParsePath
     */
    public void showPDFDocument(String strDisplayText, String strParsePath)
    {
    	final String strPDF = m_rmResourcesManager.getStrAttributeValue(strParsePath, "", 0);
		
		final String strCachedImageName = Integer.toString(strPDF.hashCode()) + ".pdf";
		
		if (false == CToolbox.fileExistsOnSDCard("/forestXPlorer", strCachedImageName))
		{
			final AlertDialog alertDialog;
			alertDialog = new AlertDialog.Builder(m_cCore.getMainContext()).create();
			alertDialog.setTitle("Downloading");
			alertDialog.setMessage(strDisplayText);
			alertDialog.show();

			final Handler handler = new Handler();
			handler.postDelayed(new Runnable() {
			  @Override
			  public void run() {
				  CToolbox.downloadHTTPFileToSDCard(strPDF, "/forestXPlorer", strCachedImageName);
				  
				  alertDialog.cancel();
				  
				  File pdfFile = new File(String.format("/sdcard/forestXPlorer/%s", strCachedImageName)); 
    				if(pdfFile.exists()) 
    				{
    					Uri path = Uri.fromFile(pdfFile);
    					Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
    					pdfIntent.setDataAndType(path, "application/pdf");
    					pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

    					try
    					{
    						m_cCore.m_Activity.startActivity(pdfIntent);
    					}
    					catch(ActivityNotFoundException e)
    					{
    						Toast.makeText(m_cCore.getMainContext(), "No Application available to view pdf", Toast.LENGTH_LONG).show(); 
    					}
    				}
			  }
			}, 1000);
		}
		else
		{
			File pdfFile = new File(String.format("/sdcard/forestXPlorer/%s", strCachedImageName)); 
			if(pdfFile.exists()) 
			{
				Uri path = Uri.fromFile(pdfFile);
				Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
				pdfIntent.setDataAndType(path, "application/pdf");
				pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

				try
				{
					m_cCore.m_Activity.startActivity(pdfIntent);
				}
				catch(ActivityNotFoundException e)
				{
					Toast.makeText(m_cCore.getMainContext(), "No Application available to view pdf", Toast.LENGTH_LONG).show(); 
				}
			}
		}
    }
}