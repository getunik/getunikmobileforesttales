package com.getunik.audiotours.forest.tales;

import org.dom4j.Element;

import android.view.View;
import net.getunik.android.core.InterfaceMaker;
import net.getunik.android.resources.CResourceManager;
import net.getunik.android.resources.RNumber;
import net.getunik.android.resources.RString;
import net.getunik.android.resources.RStringLocalized;
import net.getunik.android.widgets.WUILabel;
import net.getunik.android.widgets.WUITableView;
import net.getunik.android.widgets.WUITableViewCell;

public class WUITVFolkTrailAudioCell extends WUITableViewCell
{
	String m_nsstrTitle = null;
	String m_nsstrAudioPath = null;
	
	/**
	 * Initialize our widget with the informations that are coming from an XML node. The Node must be a valid macc node item.
	 * 
	 * @param cxmlNode XML node that contains all the data.
	 * @param rmResourcesManager Pointer to an resource manager that contain real values. It can be null in some cases.
	 * @param iIndex If this item comes from an group with multiple items, this will specify the index of the item inside the list.
	 * @param ccCore Pointer to the widget and resource creator.
	 * @return id of the created item.
	 */
	public Object initWithXMLNode(Element cxmlNode, CResourceManager rmResourcesManager, int iIndex, InterfaceMaker ccCore)
    {
        super.initWithXMLNode(cxmlNode, rmResourcesManager, iIndex, ccCore);
	    
      //< Read Tale details
        if (getID().equals("IDTVCAudioTrails"))
        {
            m_nsstrTitle     = m_rmResourcesManager.getStrAttributeValue(String.format("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/audio_trails/audio_trail[%d]/title", iIndex + 1), "", 0);
            m_nsstrAudioPath = m_rmResourcesManager.getStrAttributeValue(String.format("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/audio_trails/audio_trail[%d]/audiofile", iIndex + 1), "", 0);
        }
        
        if (getID().equals("IDTVCFolkTrails"))
        {
            m_nsstrTitle     = m_rmResourcesManager.getStrAttributeValue(String.format("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/folk_tales/folk_tale[%d]/title", iIndex + 1), "", 0);
            m_nsstrAudioPath = m_rmResourcesManager.getStrAttributeValue(String.format("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/folk_tales/folk_tale[%d]/audiofile", iIndex + 1), "", 0);
        }
        
        if (getID().equals("IDTVCHeritageTrails"))
        {
            m_nsstrTitle     = m_rmResourcesManager.getStrAttributeValue(String.format("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/heritage_scrolls/heritage_scroll[%d]/title", iIndex + 1), "", 0);
            m_nsstrAudioPath = m_rmResourcesManager.getStrAttributeValue(String.format("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/heritage_scrolls/heritage_scroll[%d]/audiofile", iIndex + 1), "", 0);
        }
        
        if ((null == m_nsstrAudioPath) || (m_nsstrAudioPath.length() == 0))
        {
        	this.getCellView().setVisibility(View.INVISIBLE);
        }
        
	    return this;
	}
	
	/**
     * Load the content presented by this cell
     * 
     * @return none
     */
    public void loadContent()
    {
    	RStringLocalized rsText = null;
    	if (false == m_bContentLoaded)
        {
            super.loadContent();
            
            if ((null != m_nsstrAudioPath) && (m_nsstrAudioPath.length() > 0))
            {
                WUILabel uilTitle = (WUILabel)getChildWidget("IDLTitle");
                if (getID().equals("IDTVCAudioTrails"))
                {
                    rsText = (RStringLocalized)m_rmResourcesManager.getResource("[@IDRSLPlayAudioTrail]");
                }
                if (getID().equals("IDTVCFolkTrails"))
                {
                    rsText = (RStringLocalized)m_rmResourcesManager.getResource("[@IDRSLPlayFolkTale]");
                }
                if (getID().equals("IDTVCHeritageTrails"))
                {
                    rsText = (RStringLocalized)m_rmResourcesManager.getResource("[@IDRSLPlayAudioTale]");
                }
                uilTitle.setText(rsText.getValue());
            }
        }
    }
    
  ///
  ///
  ///
  public String getActionID()
  {
	  RString rsAudioPath = (RString)m_rmResourcesManager.getResource("[@IDRSAudioPath]");
	  rsAudioPath.setValue(m_nsstrAudioPath);
	  
      return super.getActionID();
  }
}