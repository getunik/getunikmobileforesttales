package com.getunik.audiotours.forest.tales;

import java.io.File;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.VideoView;
import net.getunik.android.utils.CToolbox;
import net.getunik.android.widgets.WUINavigationControllerPage;
import net.getunik.android.widgets.WUITabController;

public class WUINCPPlayMedia extends WUINavigationControllerPage
{
	VideoView m_vvVideoHolder = null;
	FrameLayout fr = null;
	RelativeLayout rr;
	
	///
    ///
    ///
    public void loadContent(int iIndex)
    {
        super.loadContent(iIndex);
        
     	//< Request fullscreen
        m_cCore.m_Activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        m_cCore.m_Activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        
        m_ncParentNavigation.setNavigationBarVisibility(false);

        WUITabController tcController = (WUITabController) m_cCore.getWidget("IDTCMainTabController");
        tcController.setTabbarVisibility(false);

        m_vvVideoHolder = new MyVideoView(m_cCore.getMainContext());
        
        RelativeLayout.LayoutParams lv = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        lv.addRule(RelativeLayout.CENTER_IN_PARENT);
        
        rr = new RelativeLayout(m_cCore.getMainContext());
        rr.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
                                   LayoutParams.FILL_PARENT));
        rr.setBackgroundColor(Color.BLACK);

        m_vvVideoHolder.setLayoutParams(lv);
        rr.addView(m_vvVideoHolder);
    }

    public class MyVideoView extends VideoView
    {
    	public MyVideoView(Context context)
    	{
			super(context);
			// TODO Auto-generated constructor stub
		}
    }
    
    ///
    ///
    ///
    public void unloadContent()
    {
    	m_vvVideoHolder.requestFocus();
        m_vvVideoHolder.stopPlayback();
        m_uivcController.removeAllViews();
        
    	m_ncParentNavigation.setNavigationBarVisibility(true);
        
        WUITabController tcController = (WUITabController) m_cCore.getWidget("IDTCMainTabController");
        tcController.setTabbarVisibility(true);
        
        //< Go back to normal screen
        m_cCore.m_Activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        m_cCore.m_Activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        
        super.unloadContent();
    }
    
    public void videoPlayer(String path, String fileName, boolean autoplay)
    {
        //MediaController is the ui control howering above the video (just like in the default youtube player).
    	MediaController mediaController = new MediaController(m_cCore.getMainContext());
    	mediaController.setMediaPlayer(m_vvVideoHolder);
        m_vvVideoHolder.setMediaController(mediaController);
        //mediaController.invalidate();
        //mediaController.setAnchorView(m_vvVideoHolder);
        //mediaController.show(5000);
        
        //< Check if the .mp3 file exist on the storagecard
        if (true == CToolbox.fileExistsOnSDCard("/ForestTales", Integer.toString(path.hashCode()) + "." + "mp3"))
		{
        	String strCachedImageName = Integer.toString(path.hashCode());
        	File sdDir = new File(Environment.getExternalStorageDirectory().getPath());
        	String strFile = sdDir.getAbsolutePath() + "/ForestTales/" + strCachedImageName + "." + "mp3";
        	m_vvVideoHolder.setVideoPath(strFile);
		}
        else
        {
        	m_vvVideoHolder.setVideoPath(path);
        }
        
        //get focus, before playing the video.e
        m_vvVideoHolder.requestFocus();
        
        if(autoplay)
        {
            m_vvVideoHolder.start();
        }
        
        m_uivcController.addView(rr);
        
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
          @Override
          public void run() {
            //Do something after 100ms
          
     // Obtain MotionEvent object
        long downTime = SystemClock.uptimeMillis();
        long eventTime = SystemClock.uptimeMillis() + 100;
        float x = 100.0f;
        float y = 100.0f;
        // List of meta states found here: developer.android.com/reference/android/view/KeyEvent.html#getMetaState()
        int metaState = 0;
        MotionEvent motionEvent = MotionEvent.obtain(
            downTime, 
            eventTime, 
            MotionEvent.ACTION_DOWN, 
            x, 
            y, 
            metaState
        );

        // Dispatch touch event to view
        getView().dispatchTouchEvent(motionEvent);
          }
        }, 1000);
     }
}