package com.getunik.audiotours.forest.tales;

import org.dom4j.Element;

import net.getunik.android.widgets.WUILabel;

public class WUILabelAccessPointList extends WUILabel
{
	/**
	 * Load the property called text.
	 *
	 * @param cxmlAttributeNode XML node that contains the data for this attribute.
	 * @return none
	 * @note This function can be overwritten if we want to load a different data
	 */
	public String loadAttributeText(Element cxmlAttributeNode)
	{
		String nsstrText = m_rmResourcesManager.getStrAttributeValue(String.format("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/access_points/access_point[%d]/title_of_the_ap", m_iWidgetIndex + 1), "", 0);
	    
	    return nsstrText;
	}
}