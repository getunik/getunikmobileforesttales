package com.getunik.audiotours.forest.tales;

import org.dom4j.Element;

import android.os.Handler;
import android.os.Message;
import android.view.ViewGroup.LayoutParams;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import net.getunik.android.core.InterfaceMaker;
import net.getunik.android.resources.CResourceManager;
import net.getunik.android.resources.RRSSData;
import net.getunik.android.widgets.WUIView;

public class WUIGuideDataDownloadingView extends WUIView 
{
	ProgressBar pbProgressBar = null;
	InterfaceMaker m_imInterfaceMaker = null;
	
	/**
	 * Initialize our widget with the informations that are coming from an XML node. The Node must be a valid macc node item.
	 * 
	 * @param cxmlNode XML node that contains all the data.
	 * @param rmResourcesManager Pointer to an resource manager that contain real values. It can be null in some cases.
	 * @param iIndex If this item comes from an group with multiple items, this will specify the index of the item inside the list.
	 * @param ccCore Pointer to the widget and resource creator.
	 * @return id of the created item.
	 */
	public Object initWithXMLNode(Element cxmlNode, CResourceManager rmResourcesManager, int iIndex, InterfaceMaker ccCore)
    {
        super.initWithXMLNode(cxmlNode, rmResourcesManager, iIndex, ccCore);
	    
        pbProgressBar = new ProgressBar(ccCore.m_Activity, null, android.R.attr.progressBarStyleHorizontal);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(LayoutParams.FILL_PARENT, 20);
        lp.setMargins(10, 0, 10, 0);
        //< Set the layout
        pbProgressBar.setLayoutParams(lp);
        
        m_uivView.addView(pbProgressBar);
        m_uivView.setBackgroundColor(android.R.color.black);
	    return this;
	}
	
	/**
	 * Reset the progress bar to 0%
	 *
	 * @return none
	 */
	void resetProgressBar()
	{
		pbProgressBar.setProgress(0);
	}
	
	/**
	 * Start the updating process
	 *
	 * @return none
	 */
	void startUpdating(InterfaceMaker imInterfaceMaker)
	{
		Thread mUpdateDatabase = new Thread(new Runnable()
        {			
            @Override
            public void run()
            {
            	downloadContent();
            	
            	 for (int iStep = 2; (iStep <= 10); iStep++)
                 {
                     //< Cancel the download if requested
//                     if (true == m_appDelegate.m_bStopUpdating){bDatabasesUpdated = false;}

            		try
            		{
            			 Thread.sleep(200);
					}
            		catch (InterruptedException e)
            		{
						// TODO Auto-generated catch block
            			e.printStackTrace();
					}
                    progressHandler.sendMessage(Message.obtain(progressHandler, iStep * 10));
                 }
            }
        });
        
        m_imInterfaceMaker = imInterfaceMaker;
        mUpdateDatabase.start();
		
		
	}
	
	/**
	 * 
	 */
	void downloadContent()
	{
		boolean bDatabasesUpdated = true;
		
		progressHandler.sendMessage(Message.obtain(progressHandler, 10));
        
		if (false == ((RRSSData) m_imInterfaceMaker.getResourceManager().getResource("[@DataXML]")).redownloadAndLoadContent())
        {
//            bDatabasesUpdated = false;
        }
	}
	
	//< handler for the background updating
    Handler progressHandler = new Handler()
    {
        public void handleMessage(Message msg)
        {
        	if (msg.what < 100)
            {        	 
        		m_uivView.removeView(pbProgressBar);
        		m_uivView.addView(pbProgressBar);
            	pbProgressBar.setProgress(msg.what);
            }
            
        	if (msg.what == 100)
            {
        		m_uivView.removeView(pbProgressBar);
        		m_uivView.addView(pbProgressBar);
            	pbProgressBar.setProgress(msg.what);
            	
            	((MainActivity)m_cCore.m_Activity).completeUpdate(true);
            }
        }
    };
}