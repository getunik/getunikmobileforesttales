package com.getunik.audiotours.forest.tales;

import net.getunik.android.resources.RNumber;
import net.getunik.android.widgets.WUITableView;

public class WUITableViewTrails extends WUITableView
{
	/**
     * Send an callback event to our parent widgets informing about the new action.
     * 
     * @param nsstrSenderID Sender ID
     * @param nsnIndex Index number of the widget that is calling the callback
     * @return none
     */
    public void sendCallbackEvent(String nsstrSenderID, int nsnIndex)
    {           
        //< Check if we have to send a tracking message
        String strTitle = m_rmResourcesManager.getStrAttributeValue("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/title_of_the_poi", "", 0);
        String strTitleAP = m_rmResourcesManager.getStrAttributeValue("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/access_points/access_point[[@RNAccessPoint]]/title_of_the_ap", "", 0);
        String strTrail   = m_rmResourcesManager.getStrAttributeValue("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/access_points/access_point[[@RNAccessPoint]]/trails/trail/@name", "", nsnIndex);

        String strName = String.format("/android/%s/%s/%s/%s", m_rmResourcesManager.getSelectedLanguage(), strTitle, strTitleAP, strTrail);
        m_cCore.getTrackObject().sendTrackDetails(strName);
        
        super.sendCallbackEvent(nsstrSenderID, nsnIndex);
    }
}