package com.getunik.audiotours.forest.tales;
import java.util.List;

import org.dom4j.Element;

import android.content.SharedPreferences;
import android.location.Location;
import android.view.ViewGroup.LayoutParams;
import android.widget.ListView;
import android.widget.RelativeLayout;
import net.getunik.android.core.InterfaceMaker;
import net.getunik.android.resources.CResourceManager;
import net.getunik.android.widgets.WUILabel;
import net.getunik.android.widgets.WUITableViewCell;

public class WUITableViewCellPOIList extends WUITableViewCell
{
	Location m_gpsLocation;
	float m_fDistance;
	float m_fLatitude;
	float m_fLongitude;
	
	int[] m_nsmaActivities = new int[100];
	
	///
    ///
    ///
    public Object initWithXMLNode(Element cxmlNode, CResourceManager rmResourcesManager, int iIndex, InterfaceMaker ccCore)
    {
        super.initWithXMLNode(cxmlNode, rmResourcesManager, iIndex, ccCore);

        m_gpsLocation = new Location("gps");
        
        String nsstrData = rmResourcesManager.getStrAttributeValue("[@DataXMLList]./geolocation_lat", "0", iIndex);
        if ((null != nsstrData) && (nsstrData.length() > 0))
        {
        	m_gpsLocation.setLatitude(Float.parseFloat(nsstrData));
        }

        nsstrData = rmResourcesManager.getStrAttributeValue("[@DataXMLList]./geolocation_long", "0", iIndex);
        if ((null != nsstrData) && (nsstrData.length() > 0))
        {
        	m_gpsLocation.setLongitude(Float.parseFloat(nsstrData));
        }
        
        //< Read and store all the activities that this POI can have
        int iActivities = m_rmResourcesManager.getIntAttributeValue("[@DataXMLList]./activity", 0, iIndex);
        for (int iStep = 0; iStep < iActivities; iStep++)
        {
            String nsstrActivityID = m_rmResourcesManager.getStrAttributeValue(String.format("[@DataXMLList]./activity[%d]", iStep + 1), "", iIndex);
            m_nsmaActivities[iStep] = Integer.parseInt(nsstrActivityID);
        }
        
        return this;
    }
    
    /**
     * Check if a specific activity is listed by this POI
     *
     * @param iActivityID Id of the activity that the user has selected
     * @return int category value
     */
    boolean isActivityListed(int iActivityID)
    {
        boolean bReturnVal = false;
        
        for (int iStep = 0; iStep < 100; iStep++)
        {
            if (m_nsmaActivities[iStep] == iActivityID)
            {
                bReturnVal = true;
            }
        }
        
        //< The -1 value represents the all activities
        if (-1 == iActivityID)
        {
            bReturnVal = true;
        }
        
        return bReturnVal;
    }
		
	public void updateDistance(Location lUserLocation)
	{
		loadContent();

		if (null != lUserLocation)
		{
			m_fDistance = lUserLocation.distanceTo(m_gpsLocation);
		
			WUILabel uilDistance = (WUILabel) getChildWidget("IDLDistance");
		
			SharedPreferences settings = m_cCore.getMainContext().getSharedPreferences(this.getClass().getPackage().getName(), 0);
			String nsstrMetrics = settings.getString("Units", "");
        
	    	if (nsstrMetrics.equals("mi"))
	        {
	    		uilDistance.setText(String.format("%.2f m", (float)(m_fDistance / 1000 / 1.61)));
	        }
	    	else
	    	{
	    		uilDistance.setText(String.format("%.2f km", (float)(m_fDistance / 1000)));
	    	}
		}
	}
	
	protected int getCompareNumber()
    {
        return (int) m_fDistance;
    }
}