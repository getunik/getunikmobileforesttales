package com.getunik.audiotours.forest.tales;
import android.view.View;
import net.getunik.android.resources.RString;
import net.getunik.android.widgets.WUINavigationControllerPage;
import net.getunik.android.widgets.WUITableViewCell;

public class WUINCPageMediaPage extends WUINavigationControllerPage
{
	public void unloadContent()
	{
		WUITableViewCell uitvcSpaceCell = (WUITableViewCell)getChildWidget("IDTVCSpace");
  	  	uitvcSpaceCell.getCellView().setVisibility(View.INVISIBLE);
  	  	
  	  	WUIViewAudioPlayer wuiPlayer = (WUIViewAudioPlayer)getChildWidget("IDVAudioPlayer");
  	  	if (null != wuiPlayer)
  	  	{
  	  		wuiPlayer.sendCallbackEvent("IDBAudioClose", 0);
  	  	}
  	  	
  	  	super.unloadContent();
	}
	
	/**
     * Send an callback event to our parent widgets informing about the new action.
     * 
     * @param nsstrSenderID Sender ID
     * @param nsnIndex Index number of the widget that is calling the callback
     * @return none
     */
    public void sendCallbackEvent(String nsstrSenderID, int nsnIndex)
    {
	    boolean bHandled = false;
	    
	    if (nsstrSenderID.equals("IDTVCPlayAudio"))
	    {
	    	WUITableViewCell uitvcSpaceCell = (WUITableViewCell)getChildWidget("IDTVCSpace");
	  	  	uitvcSpaceCell.getCellView().setVisibility(View.VISIBLE);
	  	  	
	  	    RString rsAudioPath = (RString)m_rmResourcesManager.getResource("[@IDRSAudioPath]");
	  	  
	  	  	WUIViewAudioPlayer wuiPlayer = (WUIViewAudioPlayer)getChildWidget("IDVAudioPlayer");
	  	  	wuiPlayer.stopPlayback();
	  	  	wuiPlayer.initializePlayerWithFile(rsAudioPath.getValue(), uitvcSpaceCell);
	  	  
	        bHandled = true;
	    }
	    else
	    {
	    	WUITableViewCell uitvcSpaceCell = (WUITableViewCell)getChildWidget("IDTVCSpace");
	  	  	uitvcSpaceCell.getCellView().setVisibility(View.INVISIBLE);
	  	  	
	  	  	WUIViewAudioPlayer wuiPlayer = (WUIViewAudioPlayer)getChildWidget("IDVAudioPlayer");
	  	  	wuiPlayer.sendCallbackEvent("IDBAudioClose", 0);
	    }

	    if (false == bHandled)
	    {
	        super.sendCallbackEvent(nsstrSenderID, nsnIndex);
	    }
	}
}