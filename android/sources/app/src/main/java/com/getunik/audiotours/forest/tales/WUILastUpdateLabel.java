package com.getunik.audiotours.forest.tales;

import java.util.Date;

import org.dom4j.Element;

import android.content.SharedPreferences;
import net.getunik.android.core.InterfaceMaker;
import net.getunik.android.resources.CResourceManager;
import net.getunik.android.widgets.WUILabel;

public class WUILastUpdateLabel extends WUILabel
{
public static final String PREFS_NAME = "ForestXplorerPrefs";
	
	/**
	 * Initialize our widget with the informations that are coming from an XML node. The Node must be a valid macc node item.
	 * 
	 * @param cxmlNode XML node that contains all the data.
	 * @param rmResourcesManager Pointer to an resource manager that contain real values. It can be null in some cases.
	 * @param iIndex If this item comes from an group with multiple items, this will specify the index of the item inside the list.
	 * @param ccCore Pointer to the widget and resource creator.
	 * @return id of the created item.
	 */
	public Object initWithXMLNode(Element cxmlNode, CResourceManager rmResourcesManager, int iIndex, InterfaceMaker ccCore)
    {
        super.initWithXMLNode(cxmlNode, rmResourcesManager, iIndex, ccCore);
	    
        updateDisplayedText();
	    return this;
	}
	
	/**
	 *
	 */
	void updateDisplayedText()
	{
	    //< The date of the last update will be stored in our user defaults
		SharedPreferences settings = m_cCore.m_Activity.getSharedPreferences(PREFS_NAME, 0);
        String nsstrLastUpdate = settings.getString("CForestXplorerLastUpdate", "");
	    
	    if ((null == nsstrLastUpdate) || (nsstrLastUpdate.length() == 0)) 
	    {
	    	if (m_rmResourcesManager.getSelectedLanguage().equals("cy"))
	    	{
	    		m_uilLabel.setText("");
	    	}
	    	else
	    	{
	    		m_uilLabel.setText("");
	    	}
	    }
	    else
	    {
	    	if ((null != nsstrLastUpdate) && (false == "".equals(nsstrLastUpdate)))
	    	{
	    		nsstrLastUpdate = nsstrLastUpdate.replace("\n", "");
	    	
	    		long lDate = 0;
	    		
	    		try{ lDate = Long.parseLong( nsstrLastUpdate); }catch(Exception e){}
	    		
	    		if (0 == lDate) return;
	    		
	    		Date time = new Date(lDate);

	    		if (m_rmResourcesManager.getSelectedLanguage().equals("cy"))
	    		{
	    			m_uilLabel.setText(String.format("Diweddariad diwethaf: %02d.%02d.%d %02d:%02d",
    					time.getDate(),
    					time.getMonth() + 1,
                       	time.getYear() + 1900,
                        time.getHours(),
                        time.getMinutes()));
	    		}
	    		else
	    		{
	    			m_uilLabel.setText(String.format("Last update: %02d.%02d.%d %02d:%02d",
	    					time.getDate(),
	    					time.getMonth() + 1,
	                       	time.getYear() + 1900,
	                        time.getHours(),
	                        time.getMinutes()));
	    		}
	    	}
	    }
	}
}