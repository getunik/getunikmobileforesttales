package com.getunik.audiotours.forest.tales;

import net.getunik.android.widgets.WGoogleMaps;

public class WGoogleMapsCustom extends WGoogleMaps
{   
	public void fillMapView(int iSelectedActivity)
	{		    	
	    int iSize = m_nsmaPOIList.size();
	    for (int iStep = 0; iStep < iSize; iStep++) 
	    {
	    	//< Hide or show POI based on selected activity
	    	WGoogleMapsPOICustom poi = (WGoogleMapsPOICustom) m_nsmaPOIList.get(iStep);
	    	poi.setHidden(!poi.isActivityListed(iSelectedActivity));
	    }
	    
	    refreshClusters();
		
		return;
	}
	
    public void sendCallbackEvent(String nsstrSenderID, int nsnIndex)
    {
    	//< Check if we have to send a tracking message
        String strName = String.format("/android/%s/%s", m_rmResourcesManager.getSelectedLanguage(), m_rmResourcesManager.getStrAttributeValue("[@DataXMLList]./title_of_the_poi", "", nsnIndex));
        m_cCore.getTrackObject().sendTrackDetails(strName);
        
    	super.sendCallbackEvent(nsstrSenderID, nsnIndex);
    }
}