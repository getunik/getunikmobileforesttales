package com.getunik.audiotours.forest.tales;

import org.dom4j.Element;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

import net.getunik.android.core.InterfaceMaker;
import net.getunik.android.resources.CResourceManager;
import net.getunik.android.widgets.WUINCPLeftButton;
import net.getunik.android.widgets.WUIView;

public class WUINCPLeftButtonCustom extends WUINCPLeftButton
{ 
	public static final String PREFS_NAME = "ForestXplorer";
	
    public String m_nsstrTitle = "";
    public int    m_iRating    = 0;
    Spinner s = null;
    int[] m_nsmaActivities = new int[100];
    
    public Object initWithXMLNode(Element cxmlNode, CResourceManager rmResourcesManager, int iIndex, InterfaceMaker ccCore)
    {
    	super.initWithXMLNode(cxmlNode, rmResourcesManager, iIndex, ccCore);
        
    	MyArrayAdapter adapter = new MyArrayAdapter(m_cCore.getMainContext(), android.R.layout.simple_spinner_item); 
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        if (m_rmResourcesManager.getSelectedLanguage().equals("cy"))
		{
        	adapter.add("Holl weithgareddau");
		}
        else
        {
        	adapter.add("All Activities");
        }
        
        //< Get a list with all values
        int iActivities = m_rmResourcesManager.getIntAttributeValue("[@ActivitiesXMLList]", 0, 0);
        
        //< Loop all elements and store the values
        for (int iStep = 0; iStep < iActivities; iStep++)
        {
            String nsaStringList = m_rmResourcesManager.getStrAttributeValue("[@ActivitiesXMLList]./title_of_the_activity", "", iStep);
            String nsaIntList    = m_rmResourcesManager.getStrAttributeValue("[@ActivitiesXMLList]./@id", "", iStep);

            adapter.add(nsaStringList);
            m_nsmaActivities[iStep] = Integer.parseInt(nsaIntList);
//            [m_nsmaIntValues addObject:nsaIntList];
        }
        
        s = new Spinner(m_cCore.getMainContext());
        s.setOnItemSelectedListener(selectListener);
        s.setAdapter(adapter);
        
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(0,0);
        lp.setMargins(0, 0, 0, 0);
        //< Set the layout
        s.setLayoutParams(lp);
        
        WUINCPMapWithFilter uiv = (WUINCPMapWithFilter) m_cCore.getWidget("IDNCPMapView");
        uiv.getView().addView(s);
        
        
    	return this;
    }
    
    /**
     * Send an callback event to our parent widgets informing about the new action.
     * 
     * @param nsstrSenderID Sender ID
     * @param nsnIndex Index number of the widget that is calling the callback
     * @return none
     */
    public void sendCallbackEvent(String nsstrSenderID, int nsnIndex)
    {
    	s.performClick();
    }
    
    void applyFilter(int iFilterID)
    {   
    	WUITableViewPOIList tvpPOIList = (WUITableViewPOIList) m_cCore.getWidget("IDTVNearbyList");
    	if (null != tvpPOIList)
    	{
    		tvpPOIList.fillTableView(iFilterID);
    	}
    	
    	WGoogleMapsCustom wgmcMapView = (WGoogleMapsCustom) m_cCore.getWidget("IDMGoogleMainMap");
    	if (null != wgmcMapView)
    	{
    		wgmcMapView.fillMapView(iFilterID);
    	}
    }
    
    private OnItemSelectedListener selectListener = new OnItemSelectedListener() {
        public void onItemSelected(AdapterView parent, View v, int position, long id)
        {
        	if (position == 0)
        	{
        		applyFilter(-1);
        	}
        	else
        	{
        		applyFilter(m_nsmaActivities[position - 1]);
        	}
            return;
        }
        public void onNothingSelected(AdapterView arg0) {}
    };

    private class MyArrayAdapter extends ArrayAdapter {

        public MyArrayAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        }

        public TextView getView(int position, View convertView, ViewGroup parent) {
        TextView v = (TextView) super.getView(position, convertView, parent);
        v.setTextColor(Color.BLACK);
        v.setTextSize(12);
        v.setGravity(Gravity.CENTER);
        return v;
        }
    }
}