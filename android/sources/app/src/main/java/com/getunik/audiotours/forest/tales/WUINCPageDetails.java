package com.getunik.audiotours.forest.tales;

import java.io.File;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import org.dom4j.Element;

import net.getunik.android.resources.RNumber;
import net.getunik.android.resources.RStringLocalized;
import net.getunik.android.resources.RXPathNodes;
import net.getunik.android.utils.CContentDownloader.IDownloaderCallback;
import net.getunik.android.utils.CNSDataXMLNodeContentDownload;
import net.getunik.android.utils.CToolbox;
import net.getunik.android.widgets.WUIButton;
import net.getunik.android.widgets.WUINavigationControllerPage;
import net.getunik.android.widgets.WUITableViewCell;
import net.getunik.android.widgets.WUIView;

public class WUINCPageDetails extends WUINavigationControllerPage implements IDownloaderCallback
{ 
	CNSDataXMLNodeContentDownload m_cdContentDownload = null;
			
	/**
	 * Called when the content is loading
	 *
	 * @param iIndex Widget index
	 * @return none
	 */
	public void loadContent(int iIndex)
	{
	    //< Retreive the number object associated with the selected POI and set the selected number
	    RNumber rnSelectedPOI = (RNumber)m_rmResourcesManager.getResource("[@RNSelectedPOI]");
	    rnSelectedPOI.setValue(iIndex);
	    
	    RNumber rnSelectedIndex = (RNumber)m_rmResourcesManager.getResource("[@RNPOIIndex]");
	    rnSelectedIndex.setValue(iIndex + 1);
	    
	    //< Superclass call
	    super.loadContent(iIndex);
	    
	    //< Hide the download button if content files are available
	    RXPathNodes rsContentData = (RXPathNodes)m_rmResourcesManager.getResource("[@DataXMLList]");
	    Element cxmlElement = rsContentData.getCXMLElementAtPath("[@DataXMLList]", rnSelectedIndex.getValue() - 1);
	    
	    m_cdContentDownload = new CNSDataXMLNodeContentDownload(m_cCore.getMainContext(), this, "/forestTales");
	    m_cdContentDownload.addDownloadFileExtension("jpg", false);
	    m_cdContentDownload.addDownloadFileExtension("pdf", true);
	    m_cdContentDownload.addDownloadFileExtension("mp3", true);
	    
	    if (false == m_cdContentDownload.isNewContentAvailableWithXMLNode(cxmlElement))
	    {
	        WUITableViewCell wuitvcDownloadButton = (WUITableViewCell)getChildWidget("IDTVCDownloadTales");
	        wuitvcDownloadButton.getCellView().setVisibility(View.INVISIBLE);
	    }
	    else
	    {
	    	long lNewContentSize = m_cdContentDownload.getSizeOfNewContentWithXMLNode(cxmlElement);
	        WUIButton uibDownloadButton = (WUIButton)m_cCore.getWidget("IDBDownloadButton");
	        RStringLocalized rsText = (RStringLocalized)m_rmResourcesManager.getResource("[@IDSDownloadSiteTales]");
	        
	        //< Add 1 MB always since the size can be very close to 0
	        uibDownloadButton.setTitle(String.format("%s (%dMB)", rsText.getValue(), lNewContentSize / 1048576 + 1));
	    }
	}
	
	/**
  	 * Function called by the callback function.
  	 * Note: This function should be overwritten in the derived classes if they want to handle the events recived
  	 * 
     * @param nsstrSenderID Sender ID
     * @param nsnIndex Index number of the widget that is calling the callback
     * @return none
     */
    public void subControllerCallback(String nsstrSenderID, int nsnIndex)
    {
    	//< Hide add if present
    	boolean bHandled = false;

    	if (true == m_cCore.isUserInteractionEnabled())
    	{
    		if (nsstrSenderID.equals("IDNCPOverviewMap"))
    		{
    			showPDFDocument("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/map_of_poi");
                
    			bHandled = true;
    		}
    		
    		if (nsstrSenderID.equals("IDBDownloadContent"))
    	    {
    	        startDownload();

    	        bHandled = true;
    	    }

    		if (false == bHandled)
    		{ 
    			super.subControllerCallback(nsstrSenderID, nsnIndex);
    		}
    	}
	}
    
    void startDownload()
    {
        RNumber rnSelectedIndex = (RNumber)m_rmResourcesManager.getResource("[@RNPOIIndex]");
        RXPathNodes rsContentData = (RXPathNodes)m_rmResourcesManager.getResource("[@DataXMLList]");
        Element cxmlElement = rsContentData.getCXMLElementAtPath("[@DataXMLList]", rnSelectedIndex.getValue() - 1);
        
        m_cdContentDownload.downloadAllContentFromXMLNode(cxmlElement, "admin", "g3tun1k");
        
        WUIButton uibDownloadButton = (WUIButton)getChildWidget("IDBDownloadButton");
        uibDownloadButton.getButton().setVisibility(View.INVISIBLE);
    }
    
    /**
     * Download and display a pdf document in an external pdf viewer
     * @param strParsePath
     */
    public void showPDFDocument(String strParsePath)
    {
    	final String strPDF = m_rmResourcesManager.getStrAttributeValue(strParsePath, "", 0);
		
		final String strCachedImageName = Integer.toString(strPDF.hashCode()) + ".pdf";
		
		if (false == CToolbox.fileExistsOnSDCard("/forestTales", strCachedImageName))
		{
			final AlertDialog alertDialog;
			alertDialog = new AlertDialog.Builder(m_cCore.getMainContext()).create();
			alertDialog.setTitle("Downloading");
			if (m_rmResourcesManager.getSelectedLanguage().equals("cy"))
			{
				alertDialog.setMessage("Lawrlwytho Map Lleoliad. Disgwyliwch.");
			}
			else
			{
				alertDialog.setMessage("Downloading Location Map. Please wait.");
			}
			alertDialog.show();

			final Handler handler = new Handler();
			handler.postDelayed(new Runnable() {
			  @Override
			  public void run() {
				  CToolbox.downloadHTTPFileToSDCard(strPDF, "/forestTales", strCachedImageName);
				  
				  alertDialog.cancel();
				  
				  File pdfFile = new File(String.format("/sdcard/forestTales/%s", strCachedImageName)); 
    				if(pdfFile.exists()) 
    				{
    					Uri path = Uri.fromFile(pdfFile);
    					Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
    					pdfIntent.setDataAndType(path, "application/pdf");
    					pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

    					try
    					{
    						m_cCore.m_Activity.startActivity(pdfIntent);
    					}
    					catch(ActivityNotFoundException e)
    					{
    						Toast.makeText(m_cCore.getMainContext(), "No Application available to view pdf", Toast.LENGTH_LONG).show(); 
    					}
    				}
			  }
			}, 1000);
		}
		else
		{
			File pdfFile = new File(String.format("/sdcard/forestTales/%s", strCachedImageName)); 
			if(pdfFile.exists()) 
			{
				Uri path = Uri.fromFile(pdfFile);
				Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
				pdfIntent.setDataAndType(path, "application/pdf");
				pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

				try
				{
					m_cCore.m_Activity.startActivity(pdfIntent);
				}
				catch(ActivityNotFoundException e)
				{
					Toast.makeText(m_cCore.getMainContext(), "No Application available to view pdf", Toast.LENGTH_LONG).show(); 
				}
			}
		}
    }

	@Override
	public void loadingCompleted() {
		// TODO Auto-generated method stub
		
	}
}