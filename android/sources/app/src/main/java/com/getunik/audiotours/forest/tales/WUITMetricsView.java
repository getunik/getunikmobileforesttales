package com.getunik.audiotours.forest.tales;

import org.dom4j.Element;

import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.graphics.drawable.GradientDrawable;
import net.getunik.android.core.InterfaceMaker;
import net.getunik.android.resources.CResourceManager;
import net.getunik.android.widgets.WUITableView;
import net.getunik.android.widgets.WUITableViewCell;

public class WUITMetricsView extends WUITableView
{
	/**
     * {@inheritDoc}
     */
    public Object initWithXMLNode(Element cxmlNode, CResourceManager rmResourcesManager, int iIndex, InterfaceMaker ccCore)
    {        
        super.initWithXMLNode(cxmlNode, rmResourcesManager, iIndex, ccCore);
        
        updateDisplay();
        
        return this;
    }
    
	/**
     * Send an callback event to our parent widgets informing about the new action.
     * 
     * @param nsstrSenderID Sender ID
     * @param nsnIndex Index number of the widget that is calling the callback
     * @return none
     */
    public void sendCallbackEvent(String nsstrSenderID, int nsnIndex)
    {
    	if (nsstrSenderID.equals("IDCMetricsKM"))
    	{
    		writeMetrics("km");
    		
    		updateDisplay();
    	}
    	if (nsstrSenderID.equals("IDCMetricsMI"))
    	{
    		writeMetrics("mi");
    		
    		updateDisplay();
    	}

    	return;
    }
    
    protected void writeMetrics(String nsstrMetrics)
    {
    	// We need an Editor object to make preference changes.
        // All objects are from android.context.Context
        SharedPreferences settings = m_cCore.getMainContext().getSharedPreferences(this.getClass().getPackage().getName(), 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("Units", nsstrMetrics);
        
        // Commit the edits!
        editor.commit();
    }
    
    protected void updateDisplay()
    {
    	SharedPreferences settings = m_cCore.getMainContext().getSharedPreferences(this.getClass().getPackage().getName(), 0);
        String nsstrMetrics = settings.getString("Units", "");
        
    	if (nsstrMetrics.equals("mi"))
        {
        	WUITableViewCell uivCellEnglish = (WUITableViewCell) m_cCore.getWidget("IDCMetricsKM");
    		uivCellEnglish.setAccessory(0);
    		WUITableViewCell uivCellWelsh = (WUITableViewCell) m_cCore.getWidget("IDCMetricsMI");
    		uivCellWelsh.setAccessory(1);
        }
        else
        {
        	WUITableViewCell uivCellEnglish = (WUITableViewCell) m_cCore.getWidget("IDCMetricsKM");
    		uivCellEnglish.setAccessory(1);
    		WUITableViewCell uivCellWelsh = (WUITableViewCell) m_cCore.getWidget("IDCMetricsMI");
    		uivCellWelsh.setAccessory(0);
        }
    	
    	WUITableViewPOIList uitvPOITable = (WUITableViewPOIList)m_cCore.getWidget("IDTVNearbyList");
    	uitvPOITable.updateMetrics();
    }
}