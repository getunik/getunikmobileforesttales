package com.getunik.audiotours.forest.tales;

import org.dom4j.Element;
import net.getunik.android.core.InterfaceMaker;
import net.getunik.android.resources.CResourceManager;
import net.getunik.android.widgets.WUIImage;
import net.getunik.android.widgets.WUILabel;
import net.getunik.android.widgets.WUITableViewCell;

public class WUITVFolkTrailCell extends WUITableViewCell
{
	String m_nsstrTitle = null;
	String m_nsstrImagePath = null;
	
	/**
	 * Initialize our widget with the informations that are coming from an XML node. The Node must be a valid macc node item.
	 * 
	 * @param cxmlNode XML node that contains all the data.
	 * @param rmResourcesManager Pointer to an resource manager that contain real values. It can be null in some cases.
	 * @param iIndex If this item comes from an group with multiple items, this will specify the index of the item inside the list.
	 * @param ccCore Pointer to the widget and resource creator.
	 * @return id of the created item.
	 */
	public Object initWithXMLNode(Element cxmlNode, CResourceManager rmResourcesManager, int iIndex, InterfaceMaker ccCore)
    {
        super.initWithXMLNode(cxmlNode, rmResourcesManager, iIndex, ccCore);
	    
        //< Read Tale details
        if (getID().equals("IDTVCAudioTrails"))
        {
            m_nsstrTitle     = m_rmResourcesManager.getStrAttributeValue(String.format("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/audio_trails/audio_trail[%d]/title", iIndex + 1), "", 0);
            m_nsstrImagePath = m_rmResourcesManager.getStrAttributeValue(String.format("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/audio_trails/audio_trail[%d]/picture[@param='560x']", iIndex + 1), "", 0);
        }
        
        if (getID().equals("IDTVCFolkTrails"))
        {
            m_nsstrTitle     = m_rmResourcesManager.getStrAttributeValue(String.format("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/folk_tales/folk_tale[%d]/title", iIndex + 1), "", 0);
            m_nsstrImagePath = m_rmResourcesManager.getStrAttributeValue(String.format("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/folk_tales/folk_tale[%d]/picture[@param='560x']", iIndex + 1), "", 0);
        }
        
        if (getID().equals("IDTVCHeritageTrails"))
        {
            m_nsstrTitle     = m_rmResourcesManager.getStrAttributeValue(String.format("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/heritage_scrolls/heritage_scroll[%d]/title", iIndex + 1), "", 0);
            m_nsstrImagePath = m_rmResourcesManager.getStrAttributeValue(String.format("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/heritage_scrolls/heritage_scroll[%d]/picture[@param='560x']", iIndex + 1), "", 0);
        }
        
	    return this;
	}
	
	/**
     * Load the content prezented by this cell
     * 
     * @return none
     */
    public void loadContent()
    {
    	if (false == m_bContentLoaded)
        {
            super.loadContent();
            
            WUILabel uilTitle = (WUILabel)getChildWidget("IDLTaleTitle");
            uilTitle.setText(m_nsstrTitle);
            
            WUIImage uiiImage = (WUIImage)getChildWidget("IDITalePicture");
            uiiImage.loadImage(m_nsstrImagePath, 0);
        }
    }
}