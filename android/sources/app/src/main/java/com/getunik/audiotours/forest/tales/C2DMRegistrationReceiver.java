package com.getunik.audiotours.forest.tales;

import java.util.ArrayList;
import java.util.List;
import net.getunik.android.httphandling.CHTTPClient;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.util.Log;
import android.widget.Toast;

public class C2DMRegistrationReceiver extends BroadcastReceiver
{
	@Override
	public void onReceive(Context context, Intent intent)
	{
		String action = intent.getAction();
		if ("com.google.android.c2dm.intent.REGISTRATION".equals(action))
		{
			final String registrationId = intent.getStringExtra("registration_id");
			String error = intent.getStringExtra("error");

			Log.d("C2DM", "dmControl: registrationId = " + registrationId + ", error = " + error);
			String deviceId = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
			createNotification(context, registrationId);
			sendRegistrationIdToServer(deviceId, registrationId);
			// Also save it in the preference to be able to show it later
			saveRegistrationId(context, registrationId);
		}
	}

	private void saveRegistrationId(Context context, String registrationId)
	{
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		Editor edit = prefs.edit();
		edit.putString("C2DMRegistrationID", registrationId);
		edit.commit();
	}

	public void createNotification(Context context, String registrationId)
	{
//		Toast.makeText(context, "C2DM Registration sucsessfull", Toast.LENGTH_SHORT).show();
	}

	// Incorrect usage as the receiver may be canceled at any time
	// do this in an service and in an own thread
	public void sendRegistrationIdToServer(String deviceId, String registrationId)
	{
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
		// Get the deviceID
		nameValuePairs.add(new BasicNameValuePair("name", "android"));
		nameValuePairs.add(new BasicNameValuePair("token", registrationId));
		
		CHTTPClient httpClient = new CHTTPClient();
		httpClient.sendPOSTRequest(nameValuePairs, "http://admin.guideappmaker.com/fcw/token", "fcw", "gimidi66", 1000);
	}
}