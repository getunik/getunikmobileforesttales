package com.getunik.audiotours.forest.tales;

import org.dom4j.Element;

import android.view.View;
import net.getunik.android.core.InterfaceMaker;
import net.getunik.android.resources.CResourceManager;
import net.getunik.android.widgets.WUITableView;
import net.getunik.android.widgets.WUITableViewCell;

public class WUITVTalesPlaceDetails extends WUITableView
{ 
	/**
	 * Initialize our widget with the informations that are coming from an XML node. The Node must be a valid macc node item.
	 * 
	 * @param cxmlNode XML node that contains all the data.
	 * @param rmResourcesManager Pointer to an resource manager that contain real values. It can be null in some cases.
	 * @param iIndex If this item comes from an group with multiple items, this will specify the index of the item inside the list.
	 * @param ccCore Pointer to the widget and resource creator.
	 * @return id of the created item.
	 */
	public Object initWithXMLNode(Element cxmlNode, CResourceManager rmResourcesManager, int iIndex, InterfaceMaker ccCore)
    {
        super.initWithXMLNode(cxmlNode, rmResourcesManager, iIndex, ccCore);
        
        //< Check if we have any audioguides
        int iAudioTrails = m_rmResourcesManager.getIntAttributeValue("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/audio_trails/audio_trail", 0, 0);
        if (0 == iAudioTrails)
        {
            WUITableViewCell uitvcCell = (WUITableViewCell)getChildWidget("IDTVCDetailsAudioTrails");
            uitvcCell.getCellView().setVisibility(View.INVISIBLE);
        }
        
        //< Check if we have any folk tales
        int iFolkTales = m_rmResourcesManager.getIntAttributeValue("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/folk_tales/folk_tale", 0, 0);
        if (0 == iFolkTales)
        {
            WUITableViewCell uitvcCell = (WUITableViewCell)getChildWidget("IDTVCDetailsFolkTales");
            uitvcCell.getCellView().setVisibility(View.INVISIBLE);
        }
        
        //< Check if we have any audioguides
        int iHeritageScrolls = m_rmResourcesManager.getIntAttributeValue("[@DataXMLAllPOI]./point_of_interest[[@RNPOIIndex]]/forest_tales_places/forest_tales_place[[@RNAccessPoint]]/heritage_scrolls/heritage_scroll", 0, 0);
        if (0 == iHeritageScrolls)
        {
            WUITableViewCell uitvcCell = (WUITableViewCell)getChildWidget("IDTVCDetailsHeritageScrolls");
            uitvcCell.getCellView().setVisibility(View.INVISIBLE);
        }
        
        reloadData();
	    
	    return this;
	}
}